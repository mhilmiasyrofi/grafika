#include "global.h"
#include "point.h"

#ifndef GRAFIKA_FONTBITMAP_H
#define GRAFIKA_FONTBITMAP_H

#define NUMBER_OF_CHARS 26
#define FONT_FILE "examplefont"

void loadFont (const char * filename, int (*char_ptr)[FONT_HEIGHT][FONT_WIDTH]);
int from_char_to_index(char input);
void printChar(char c, frame_buffer fb,  int x_start, int y_start, int scale, int color, int (*char_table)[FONT_HEIGHT][FONT_WIDTH]);

#endif
