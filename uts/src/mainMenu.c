#define  _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include "fbuffer.h"
#include "global.h"
#include "cursor.h"
#include "shape.h"

static volatile sig_atomic_t  done = 0;


static void my_handler(int *s) {
    printf("Caught signal %d\n", s);
    done = 1;
}

void on_left_button_press(cursor * c, int *x) {
    // int x = 0;
    if (c->left && !c->wasleft) {
        // onpress left button
        *x = 1;
        printf(" LeftDown");
    } else if (c->left && c->wasleft) {
        // x = 1;
        // onhold left button
        printf(" Left");
    } else if (!c->left && c->wasleft) {
        // x = 1;
        // onrelease left button
        printf(" LeftUp");
    }

    fflush(stdout);

    c->wasleft = c->left;
}

// void printLetter(frame_buffer fb, int minX, int maxX, int minY, int maxY, char string[10]){
//     int batasAtas = minY;
//      int batasKiri = minX;
//      int batasBawah = maxY;
//      int batasKanan = maxX;
//      int currentX = batasKiri;
//      int currentY = batasAtas;
//      // int n = 0;
//      FILE *file;
//      // *string
//      // char input[100];
//      // scanf("%s", input);
//      printf("%d\n",strlen(string));
//      int tempCurrentX = currentX;
//      int tempCurrentY = currentY;
//      int length = 5;
//      int width = 5;
//      for (int n = 0; n < strlen(string);n++){
//          char extensionName[10] = ".txt";
//          char fileName[110];
//          fileName[0] = string[n];
//          if (n == 0){
//              strcat(fileName,extensionName); 
//          }
//          file = fopen(fileName, "r");

         
//          if ((currentX + 5*(length)) > batasKanan){
//             currentY += 5*(width+4);
//             currentX = batasKiri;
//          }
//          if ((currentY + 5*(width)) > batasBawah){
//             exit(0);
//          }

//          tempCurrentX = currentX;
//          tempCurrentY = currentY;

//          for (int j = 0; j<width; j++){
//            for (int i = 0; i<length; i++){
//                 if (getc(file) == '*') {
//                     for (int y = currentY; y < currentY + 5*1; y++)
//                         setPixel(fb,i,j,LINE_COLOR);
//                      }
//                 }
//                 currentX += 5;
//            }
//            currentY += 5;
//            currentX = tempCurrentX;
//         }
//         currentX = tempCurrentX + 5*(length+4);
//         currentY = tempCurrentY;
// }

int main()
{

        /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    frame_buffer fb;

    if (initializeFrameBuffer(&fb) == 1)
        return 1;

    initializeScreen(fb, SCREEN_COLOR);

    point pointer;
    pointer.x = X_START;
    pointer.y = Y_START;

     /* Judul */
        point building1Points[4];
        int batasKiri1 = -200 + (fb.width/2);
        int batasKanan1 = 200 + (fb.width/2);
        int batasAtas1 = -350 + (fb.height/2);
        int batasBawah1 = -150 + (fb.height/2);
        building1Points[0] = create_point(batasKiri1, batasAtas1);
        building1Points[1] = create_point(batasKanan1, batasAtas1);
        building1Points[2] = create_point(batasKanan1, batasBawah1);
        building1Points[3] = create_point(batasKiri1, batasBawah1);

        line building1Lines[4];
        building1Lines[0] = create_line(building1Points[0], building1Points[1]);
        building1Lines[1] = create_line(building1Points[1], building1Points[2]);
        building1Lines[2] = create_line(building1Points[2], building1Points[3]);
        building1Lines[3] = create_line(building1Points[3], building1Points[0]);

        plane building1 = create_plane(building1Lines, sizeof(building1Lines)/sizeof(building1Lines[0]));


    /* Option 1 */
        point building2Points[4];
        int batasKiri2 = -200 + (fb.width/2);
        int batasKanan2 = 200 + (fb.width/2);
        int batasAtas2 = -120 + (fb.height/2);
        int batasBawah2 = -40 + (fb.height/2);
        building2Points[0] = create_point(batasKiri2, batasAtas2);
        building2Points[1] = create_point(batasKanan2, batasAtas2);
        building2Points[2] = create_point(batasKanan2, batasBawah2);
        building2Points[3] = create_point(batasKiri2, batasBawah2);

        line building2Lines[4];
        building2Lines[0] = create_line(building2Points[0], building2Points[1]);
        building2Lines[1] = create_line(building2Points[1], building2Points[2]);
        building2Lines[2] = create_line(building2Points[2], building2Points[3]);
        building2Lines[3] = create_line(building2Points[3], building2Points[0]);

        plane building2 = create_plane(building2Lines, sizeof(building2Lines)/sizeof(building2Lines[0]));


    /* Option 2 */
        point building3Points[4];
        int batasKiri3 = -200 + (fb.width/2);
        int batasKanan3 = 200 + (fb.width/2);
        int batasAtas3 = 10 + (fb.height/2);
        int batasBawah3 = 90 + (fb.height/2);
        building3Points[0] = create_point(batasKiri3, batasAtas3);
        building3Points[1] = create_point(batasKanan3, batasAtas3);
        building3Points[2] = create_point(batasKanan3, batasBawah3);
        building3Points[3] = create_point(batasKiri3, batasBawah3);

        line building3Lines[4];
        building3Lines[0] = create_line(building3Points[0], building3Points[1]);
        building3Lines[1] = create_line(building3Points[1], building3Points[2]);
        building3Lines[2] = create_line(building3Points[2], building3Points[3]);
        building3Lines[3] = create_line(building3Points[3], building3Points[0]);

        plane building3 = create_plane(building3Lines, sizeof(building3Lines)/sizeof(building3Lines[0]));


    /* Option 3 */
        point building4Points[4];
        int batasKiri4 = -200 + (fb.width/2);
        int batasKanan4 = 200 + (fb.width/2);
        int batasAtas4 = 140 + (fb.height/2);
        int batasBawah4 = 220 + (fb.height/2);

        building4Points[0] = create_point(batasKiri4, batasAtas4);
        building4Points[1] = create_point(batasKanan4, batasAtas4);
        building4Points[2] = create_point(batasKanan4, batasBawah4);
        building4Points[3] = create_point(batasKiri4, batasBawah4);
        
        line building4Lines[4];
        building4Lines[0] = create_line(building4Points[0], building4Points[1]);
        building4Lines[1] = create_line(building4Points[1], building4Points[2]);
        building4Lines[2] = create_line(building4Points[2], building4Points[3]);
        building4Lines[3] = create_line(building4Points[3], building4Points[0]);

        plane building4 = create_plane(building4Lines, sizeof(building4Lines)/sizeof(building4Lines[0]));

    /* QUIT */
        point building5Points[4];
        int batasKiri5 = -200 + (fb.width/2);
        int batasKanan5 = 200 + (fb.width/2);
        int batasAtas5 = 270 + (fb.height/2);
        int batasBawah5 = 350 + (fb.height/2);

        building5Points[0] = create_point(batasKiri5, batasAtas5);
        building5Points[1] = create_point(batasKanan5, batasAtas5);
        building5Points[2] = create_point(batasKanan5, batasBawah5);
        building5Points[3] = create_point(batasKiri5, batasBawah5);
        
        line building5Lines[4];
        building5Lines[0] = create_line(building5Points[0], building5Points[1]);
        building5Lines[1] = create_line(building5Points[1], building5Points[2]);
        building5Lines[2] = create_line(building5Points[2], building5Points[3]);
        building5Lines[3] = create_line(building5Points[3], building5Points[0]);

        plane building5 = create_plane(building5Lines, sizeof(building5Lines)/sizeof(building5Lines[0]));


    cursor crosshair;
    init_cursor(&crosshair);

    int x = 0;
    while (!done) {
        initializeScreen(fb, SCREEN_COLOR);
        draw_empty_plane(fb, building1, pointer, LINE_COLOR);
        char string1[10] = "GRAFIKA";
        // printLetter(fb,batasKiri2,batasKanan2,batasAtas2,batasBawah2,string1);
        draw_empty_plane(fb, building2, pointer, LINE_COLOR);
        draw_empty_plane(fb, building3, pointer, LINE_COLOR);
        draw_empty_plane(fb, building4, pointer, LINE_COLOR);
        draw_empty_plane(fb, building5, pointer, LINE_COLOR);
        // menggambar cursor
        draw_cursor(fb, &crosshair);
        x = 0;
        on_left_button_press(&crosshair,&x);
        if (x == 1){
            if (batasKiri2 <= (crosshair.center.x) && batasKanan2 >= (crosshair.center.x) && batasAtas2 <= (crosshair.center.y) && batasBawah2 >= (crosshair.center.y)) {
                printf("\nmenu 1\n");
                break;
            }
            if (batasKiri3 <= (crosshair.center.x) && batasKanan3 >= (crosshair.center.x) && batasAtas3 <= (crosshair.center.y) && batasBawah3 >= (crosshair.center.y)) {
                printf("\nmenu 2\n");
                break;
            }
            if (batasKiri4 <= (crosshair.center.x) && batasKanan4 >= (crosshair.center.x) && batasAtas4 <= (crosshair.center.y) && batasBawah4 >= (crosshair.center.y)) {
                printf("\nmenu 3\n");
                break;
            }
            if (batasKiri5 <= (crosshair.center.x) && batasKanan5 >= (crosshair.center.x) && batasAtas5 <= (crosshair.center.y) && batasBawah5 >= (crosshair.center.y)) {
                printf("\nmenu 4\n");
                break;
            }
        }
    }

    /* Done. */
    // printf("%d <=> %d\n",crosshair.center.x,crosshair.center.y);
    close(crosshair.devfd);
    return EXIT_SUCCESS;
};