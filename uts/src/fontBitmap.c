#include "fontBitmap.h"

void loadFont (const char * filename, int (*char_ptr)[FONT_HEIGHT][FONT_WIDTH]) {
    FILE *fontFile;
    fontFile = fopen(filename, "r");

    for (int i = 0; i < NUMBER_OF_CHARS; i++) {
        for (int j = 0; j < FONT_HEIGHT; j++) {
            for (int k = 0; k < FONT_WIDTH; k++) {
                int buff = fgetc(fontFile);
                if (buff == '0' || buff == '1') {
                    char_ptr[i][j][k] = buff;
                } else if (!feof(fontFile)){
                    k--;
                }
            }
        }
    }

    fclose(fontFile);
}

int from_char_to_index(char input) {
    switch (toupper(input)) {
        case 'A' :
            return 0;
        case 'B' :
            return 1;
        case 'C' :
            return 2;
        case 'D' :
            return 3;
        case 'E' :
            return 4;
        case 'F' :
            return 5;
        case 'G' :
            return 6;
        case 'H' :
            return 7;
        case 'I' :
            return 8;
        case 'J' :
            return 9;
        case 'K' :
            return 10;
        case 'L' :
            return 11;
        case 'M' :
            return 12;
        case 'N' :
            return 13;
        case 'O' :
            return 14;
        case 'P' :
            return 15;
        case 'Q' :
            return 16;
        case 'R' :
            return 17;
        case 'S' :
            return 18;
        case 'T' :
            return 19;
        case 'U' :
            return 20;
        case 'V' :
            return 21;
        case 'W' :
            return 22;
        case 'X' :
            return 23;
        case 'Y' :
            return 24;
        case 'Z' :
            return 25;
        default:
            return 26;
    }
}

void printChar(char c, frame_buffer fb,  int x_start, int y_start, int scale, int color, int (*char_table)[FONT_HEIGHT][FONT_WIDTH]) {
    int index = from_char_to_index(c);

    for (int j = 0; j < FONT_HEIGHT; j++ ) {
        for (int k = 0; k < FONT_WIDTH; k++ ) {
            if (char_table[index][j][k] - '0' == true) {
                for (int sx = 0; sx < scale; sx++) {
                    for (int sy = 0; sy < scale; sy++) {
                        setPixel(fb, (x_start + k)*scale + sx, (y_start + j)*scale +sy, color);
                    }
                }
            }
        }
    }
}