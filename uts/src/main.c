#define  _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include "fbuffer.h"
#include "global.h"
#include "cursor.h"
#include "plane.h"
#include "shape.h"
#include "letter.h"

#define STATE_MAINMENU          0
#define STATE_FONT_VECTOR       1
#define STATE_FONT_BITMAP       2
#define STATE_FILM_VECTOR       3
#define STATE_FILM_BITMAP       4
#define STATE_MAP               5
#define BUTTON_1_COLOR  0xff0000
#define BUTTON_2_COLOR  0x00ff00
#define BUTTON_3_COLOR  0x0000ff
#define BUTTON_4_COLOR  0xffff00
#define BUTTON_5_COLOR  0xff00ff
#define jr 80
#define button_width 600
#define button_height 70

static int state;
static frame_buffer fb;
static volatile sig_atomic_t  done = 0;

static void my_handler(int s) {
    printf("Caught signal %d\n", s);
    if (state == STATE_MAINMENU)
    {
        done = 1;
    } else {
        state = STATE_MAINMENU;
    }
}

int inArea (int posX, int posY) {
    if (posX >= 200 && posX <= 200 + button_width) {
        if (posY >= 150+jr && posY<= 150+jr +  button_height) {
            return STATE_FONT_BITMAP;
        }
        if (posY >= 330+jr && posY<= 330+jr +  button_height) {
            return STATE_FILM_BITMAP;
        }
        if (posY >= 520+jr && posY<= 520+jr +  button_height) {
            return STATE_MAP;
        }
    } 
    if (posX >= 700 && posX <= 700 + button_width){
        if (posY >= 240+jr && posY <= (240 + jr +  button_height)) {
            return STATE_FONT_VECTOR;
        }
        if (posY >= 430+jr && posY <= (430 + jr +  button_height)) {
            return STATE_FILM_VECTOR;
        }
    }
    return STATE_MAINMENU;
}

void on_left_button_press(cursor * c) {
    int click_x = c->center.x;
    int click_y = c->center.y;
    // int click_color = BUTTON_5_COLOR;
    int click_color = getPixel(fb, click_x+1, click_y+1);

    if (c->left && !c->wasleft) {
        // onpress left button
        // switch (click_color) {
        //     case BUTTON_1_COLOR :
        //         state = STATE_FONT_BITMAP;
        //         break;
        //     case BUTTON_2_COLOR :
        //         state = STATE_FONT_VECTOR;
        //         break;
        //     case BUTTON_3_COLOR :
        //         state = STATE_FILM_BITMAP;
        //         break;
        //     case BUTTON_4_COLOR :
        //         state = STATE_FILM_VECTOR;
        //         break;
        //     case BUTTON_5_COLOR :
        //         state = STATE_MAP;
        //         break;
        //     default :
        //         break;
        // }
        switch (inArea(click_x,click_y)) {
            case STATE_FONT_BITMAP :
                state = STATE_FONT_BITMAP;
                break;
            case STATE_FONT_VECTOR :
                state = STATE_FONT_VECTOR;
                break;
            case STATE_FILM_BITMAP :
                state = STATE_FILM_BITMAP;
                break;
            case STATE_FILM_VECTOR :
                state = STATE_FILM_VECTOR;
                break;
            case STATE_MAP :
                state = STATE_MAP;
                break;
            default :
                break;
        }
        printf ("%d dan %d dan %d",click_x, click_y, state);
    } else if (c->left && c->wasleft) {
        // onhold left button
    } else if (!c->left && c->wasleft) {
        // onrelease left button
    }

    printf("\n");
    fflush(stdout);

    c->wasleft = c->left;
}

void draw_words(char * buffer, point pointer, int size, int color, int scale) {
    size_t length = size;

    int i = 0;
    int space = 2;

    for (i = 0; i < length; ++i) {
        if ((pointer.x + (10 + FONT_SPACE) * FONT_SCALE) > fb.width) {
            pointer.x = 0; 
            pointer.y += (10 + FONT_SPACE) * FONT_SCALE;
        }
        if ('A' <= buffer[i] <= 'Z' || 'a' <= buffer[i] <= 'z')
            draw_letter(fb, buffer[i], pointer, color);
        pointer.x += (10 + FONT_SPACE) * FONT_SCALE;
    }
}

int main()
{

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    //inisialisasi frame buffer
    if (initializeFrameBuffer(&fb) == 1)
        return 1;

    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = 0;
    pointer.y = 0;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    initializeScreen(fb, SCREEN_COLOR);

    // inisialisasi cursor
    cursor crosshair;
    init_cursor(&crosshair);

    point welcome = create_point(300, 50);
    point button1 = create_point(200, 150+jr);
    point button2 = create_point(700, 240+jr);
    point button3 = create_point(200, 330+jr);
    point button4 = create_point(700, 430+jr);
    point button5 = create_point(200, 520+jr);


    while (!done) {
        // menggambar cursor
        
        if (state == STATE_MAINMENU) {
            // TODO:
            // Gambar interface main menu.
            // Ketika tombol ditekan, state berubah sesuai tombol yang ditekan.
            
            draw_words("CLICK YOUR PICK", welcome, 15, 0xffffff, 11);
            draw_words("FONT BITMAP", button1, 11, BUTTON_1_COLOR, 11);
            draw_words("FONT VECTOR", button2, 11, BUTTON_2_COLOR, 11);
            draw_words("FILM BITMAP", button3, 11, BUTTON_3_COLOR, 11);
            draw_words("FILM VECTOR", button4, 11, BUTTON_4_COLOR, 11);
            draw_words("MAP", button5, 3, BUTTON_5_COLOR, 11);
            // draw_letter(fb, 'FONT BITMAP', button1, BUTTON_1_COLOR);
            // draw_filled_rectangle(fb, button1, button_width, button_height, BUTTON_1_COLOR);
            // draw_filled_rectangle(fb, button2, button_width, button_height, BUTTON_2_COLOR);
            // draw_filled_rectangle(fb, button3, button_width, button_height, BUTTON_3_COLOR);
            // draw_filled_rectangle(fb, button4, button_width, button_height, BUTTON_4_COLOR);
            // draw_filled_rectangle(fb, button5, button_width, button_height, BUTTON_5_COLOR);

        } else if (state == STATE_FONT_BITMAP) {
            pid_t pid=fork();
            if (pid==0) { /* child process */
                execl("./p_fontb", NULL);
                exit(127); /* only if execv fails */
            }
            else { /* pid!=0; parent process */
                waitpid(pid,0,0); /* wait for child to exit */
            }
        } else if (state == STATE_FONT_VECTOR) {
            // TODO:
            // Gambar interface untuk menampilkan font.
            // User dapat memasukkan rangkaian huruf untuk kemudian ditampilkan.
            // Pastikan tidak terjadi segfault.
            // opsional: disediakan tombol back untuk mengubah state ke STATE_MAINMENU
            // alternatif: masukan kode untuk menjalankan main() font
            char *buffer;
            size_t bufsize = 32;
            size_t length;

            buffer = (char *)malloc(bufsize * sizeof(char));
            if (buffer == NULL) {
                perror("Unable to allocate buffer");
                exit(1);
            }

            printf("Type something: ");
            length = getline(&buffer, &bufsize, stdin);

            int i = 0;

            for (i = 0; i < length - 1; ++i) {
                if ((pointer.x + (10 + FONT_SPACE) * FONT_SCALE) > fb.width) {
                    pointer.x = 0; 
                    pointer.y += (10 + FONT_SPACE) * FONT_SCALE;
                }
                if ('A' <= buffer[i] <= 'Z' || 'a' <= buffer[i] <= 'z')
                    draw_letter(fb, buffer[i], pointer, FONT_COLOR);

                pointer.x += (10 + FONT_SPACE) * FONT_SCALE;
            }
        } else if (state == STATE_FILM_VECTOR) {
            /*Spawn a child to run the program.*/
            pid_t pid=fork();
            if (pid==0) { /* child process */
                execl("./p_film", NULL);
                exit(127); /* only if execv fails */
            }
            else { /* pid!=0; parent process */
                waitpid(pid,0,0); /* wait for child to exit */
            }            
        } else if (state == STATE_FILM_BITMAP) {
            // TODO:
            // Gambar interface film.
            // Film terus loop hingga SIGINT diberikan. Pastikan tidak terjadi segfault.
            // opsional: disediakan tombol back untuk mengubah state ke STATE_MAINMENU sehingga
            // tidak perlu mengirim SIGINT.

            // alternatif: masukan kode untuk menjalankan main() film
            
            /*Spawn a child to run the program.*/
            pid_t pid=fork();
            if (pid==0) { /* child process */
                execl("./p_line", NULL);
                exit(127); /* only if execv fails */
            }
            else { /* pid!=0; parent process */
                waitpid(pid,0,0); /* wait for child to exit */
            }
        } else if (state == STATE_MAP) {
            // TODO:
            // Gambar interface dan interaction untuk peta ITB.
            // User dapat menggerakan viewport dengan keyboard.
            // Terdapat checkbox untuk menampilkan/menyembunyikan jalan.
            // Terdapat checkbox untuk menampilkan/menyembunyikan gedung.
            // Pastikan tidak terjadi segfault.
            // opsional: disediakan tombol back untuk mengubah state ke STATE_MAINMENU

            // alternatif: masukan kode untuk menjalankan main() peta ITB yang telah dimodifikasi sehingga
            // memiliki checkbox.
            
            /*Spawn a child to run the program.*/
            pid_t pid=fork();
            if (pid==0) { /* child process */
                execl("./p_map", NULL);
                exit(127); /* only if execv fails */
            }
            else { /* pid!=0; parent process */
                waitpid(pid,0,0); /* wait for child to exit */
            }
        }

        draw_cursor(fb, &crosshair);
        on_left_button_press(&crosshair);
        initializeScreen(fb, SCREEN_COLOR);
    }

    /* Done. */
    close(crosshair.devfd);
    printf("Program exited successfully.\n");
    return EXIT_SUCCESS;
}
