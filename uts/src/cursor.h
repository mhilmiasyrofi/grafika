//
// Created by iqbal on 03/03/18.
//

#define  _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include "fbuffer.h"
#include "global.h"

#ifndef CURSOR_FBUFFER_H
#define CURSOR_FBUFFER_H

typedef struct {
    point           center;
    unsigned char   buffer[4];
    int             devfd;
    int             left;
    int             wasleft;
} cursor;

int init_cursor(cursor * c);

int draw_cursor(frame_buffer fb, cursor * c);



#endif //GRAFIKA_FBUFFER_H
