#include "global.h"
#include "fbuffer.h"
#include "point.h"
#include "line.h"
#include "letter.h"

void my_handler(int s) {
    printf("Caught signal %d", s);
    exit(1);
}

int main() {

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    frame_buffer fb;

    //inisialisasi frame buffer
    if (initializeFrameBuffer(&fb) == 1)
        return 1;

    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = X_START;
    pointer.y = Y_START;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    initializeScreen(fb, SCREEN_COLOR);

    char *buffer;
    size_t bufsize = 32;
    size_t length;

    buffer = (char *)malloc(bufsize * sizeof(char));
    if (buffer == NULL) {
        perror("Unable to allocate buffer");
        exit(1);
    }

    printf("Type something: ");
    length = getline(&buffer, &bufsize, stdin);

    int i = 0;

    for (i = 0; i < length - 1; ++i) {
        if ((pointer.x + (10 + FONT_SPACE) * FONT_SCALE) > fb.width) {
            pointer.x = 0; 
            pointer.y += (10 + FONT_SPACE) * FONT_SCALE;
        }
        if ('A' <= buffer[i] <= 'Z' || 'a' <= buffer[i] <= 'z') {
            draw_letter(fb, buffer[i], pointer, FONT_COLOR);
        }
        pointer.x += (10 + FONT_SPACE) * FONT_SCALE;
    }

    while (1);

    return 0;
}
