//
// Created by iqbal on 05/02/18.
//

#include "point.h"
#include "global.h"
#include "fbuffer.h"
#include <math.h>

#ifndef GRAFIKA_LINE_H
#define GRAFIKA_LINE_H

typedef struct {
    point start;
    point end;
    float gradient;
    int deltaX;
    int deltaY;
} line;

line create_line(point start, point end);

void draw_line(frame_buffer fb, line a_line, int scale, int color);

line rotate_line(line l, point pivot, float angle);

line translate_line(line l, int dx, int dy);

#endif //GRAFIKA_LINE_H
