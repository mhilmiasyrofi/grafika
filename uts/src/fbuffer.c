//
// Created by iqbal on 05/02/18.
//
#include "fbuffer.h"

const char *FB_NAME = "/dev/fb0";
void*   m_FrameBuffer;
struct  fb_fix_screeninfo m_FixInfo;
struct  fb_var_screeninfo m_VarInfo;
int 	m_FBFD;

int initializeFrameBuffer(frame_buffer *fb) {

    int iFrameBufferSize;
    /* Open the framebuffer device in read write */
    m_FBFD = open(FB_NAME, O_RDWR);
    if (m_FBFD < 0) {
        printf("Unable to open %s.\n", FB_NAME);
        return 1;
    }
    /* Do Ioctl. Retrieve fixed screen info. */
    if (ioctl(m_FBFD, FBIOGET_FSCREENINFO, &m_FixInfo) < 0) {
        printf("get fixed screen info failed: %s\n",
               strerror(errno));
        close(m_FBFD);
        return 1;
    }
    /* Do Ioctl. Get the variable screen info. */
    if (ioctl(m_FBFD, FBIOGET_VSCREENINFO, &m_VarInfo) < 0) {
        printf("Unable to retrieve variable screen info: %s\n",
               strerror(errno));
        close(m_FBFD);
        return 1;
    }

    /* Calculate the size to mmap */
    iFrameBufferSize = m_FixInfo.line_length * m_VarInfo.yres;
    printf("Line length %d\n", m_FixInfo.line_length);
    /* Now mmap the framebuffer. */
    m_FrameBuffer = mmap(NULL, iFrameBufferSize, PROT_READ | PROT_WRITE,
                         MAP_SHARED, m_FBFD,0);
    if (m_FrameBuffer == NULL) {
        printf("mmap failed:\n");
        close(m_FBFD);
        return 1;
    }

    fb->pointer = (char *) m_FrameBuffer;
    fb->width = m_VarInfo.xres;
    fb->height = m_VarInfo.yres;

    return 0;
}

void validatePoint(frame_buffer fb, point p) {

    if (p.x > fb.width || p.x < 0){
        printf("Index X out of range\n");
        exit(1);
    }

    if (p.y > fb.height || p.y < 0){
        printf("Index Y out of range\n");
        exit(1);
    }
}

int isPointValid(frame_buffer fb, point p) {

    if (p.x > fb.width || p.x < 0){
        //printf("Index X out of range\n");
        return 0;
    }

    if (p.y > fb.height || p.y < 0){
        //printf("Index Y out of range\n");
        return 0;
    }

    return 1;
}

int setPixel(frame_buffer f, int x, int y, int color) {
    point p = create_point(x, y);
    //validatePoint(f, p);
    if (isPointValid(f, p)) {
        const long int loc_x = (x + m_VarInfo.xoffset) * m_VarInfo.bits_per_pixel / 8;
        const long int loc_y = (y + m_VarInfo.yoffset) * m_FixInfo.line_length;
        *(int*) (f.pointer + loc_x + loc_y) = color;
    }
}

int getPixel(frame_buffer f, int x, int y) {
    point p;
    p.x = x;
    p.y = y;
    //validatePoint(f, p);
    if (isPointValid) {
        const long int loc_x = (x + m_VarInfo.xoffset) * m_VarInfo.bits_per_pixel / 8;
        const long int loc_y = (y + m_VarInfo.yoffset) * m_FixInfo.line_length;
        return *(int *)(f.pointer + loc_x + loc_y);
    }
    return 0;
}


void initializeScreen(frame_buffer fb, int color) {
    for (int y = 0; y < fb.height; y++) {
        for (int x = 0; x < fb.width; x++) {
            setPixel(fb, x, y, color);
        }
    }
}