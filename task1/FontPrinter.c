#include <linux/fb.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <signal.h>

typedef int bool;
#define true 1
#define false 0

#define FONT_WIDTH 3
#define FONT_HEIGHT 5
#define FONT_COLOR 0x999990
#define FONT_SCALE 15
#define SCREEN_COLOR 0x000000

#define X_START 0
#define Y_START 0

#define NUMBER_OF_CHARS 26
#define FONT_FILE "examplefont"

// tipe data untuk merepresentasikan suatu frame buffer
typedef struct {
    char* pointer;
    int width;
    int height;
} frame_buffer;

// tipe data untuk merepresantikan titik
typedef struct {
    int x;
    int y;
} point;

const char *FB_NAME = "/dev/fb0";
void*   m_FrameBuffer;
struct  fb_fix_screeninfo m_FixInfo;
struct  fb_var_screeninfo m_VarInfo;
int 	m_FBFD;

// Mengambil font dari file eksternal
void loadFont (const char * filename, int (*char_ptr)[FONT_HEIGHT][FONT_WIDTH]) {
    FILE *fontFile;
    fontFile = fopen(filename, "r");

    for (int i = 0; i < NUMBER_OF_CHARS; i++) {
        for (int j = 0; j < FONT_HEIGHT; j++) {
            for (int k = 0; k < FONT_WIDTH; k++) {
                int buff = fgetc(fontFile);
                if (buff == '0' || buff == '1') {
                    char_ptr[i][j][k] = buff;
                } else if (!feof(fontFile)){
                    k--;
                }
            }
        }
    }

    fclose(fontFile);
}

// Mengubah karakter menjadi indeks pada tabel karakter
int from_char_to_index(char input) {
    switch (toupper(input)) {
        case 'A' :
            return 0;
        case 'B' :
            return 1;
        case 'C' :
            return 2;
        case 'D' :
            return 3;
        case 'E' :
            return 4;
        case 'F' :
            return 5;
        case 'G' :
            return 6;
        case 'H' :
            return 7;
        case 'I' :
            return 8;
        case 'J' :
            return 9;
        case 'K' :
            return 10;
        case 'L' :
            return 11;
        case 'M' :
            return 12;
        case 'N' :
            return 13;
        case 'O' :
            return 14;
        case 'P' :
            return 15;
        case 'Q' :
            return 16;
        case 'R' :
            return 17;
        case 'S' :
            return 18;
        case 'T' :
            return 19;
        case 'U' :
            return 20;
        case 'V' :
            return 21;
        case 'W' :
            return 22;
        case 'X' :
            return 23;
        case 'Y' :
            return 24;
        case 'Z' :
            return 25;
        default:
            return 26;
    }
}

// menginisialisasi frame_buffer
int initialFrameBuffer(frame_buffer *fb) {

    int iFrameBufferSize;
    /* Open the framebuffer device in read write */
    m_FBFD = open(FB_NAME, O_RDWR);
    if (m_FBFD < 0) {
    	printf("Unable to open %s.\n", FB_NAME);
    	return 1;
    }
    /* Do Ioctl. Retrieve fixed screen info. */
    if (ioctl(m_FBFD, FBIOGET_FSCREENINFO, &m_FixInfo) < 0) {
    	printf("get fixed screen info failed: %s\n",
    		  strerror(errno));
    	close(m_FBFD);
    	return 1;
    }
    /* Do Ioctl. Get the variable screen info. */
    if (ioctl(m_FBFD, FBIOGET_VSCREENINFO, &m_VarInfo) < 0) {
    	printf("Unable to retrieve variable screen info: %s\n",
    		  strerror(errno));
    	close(m_FBFD);
    	return 1;
    }

    /* Calculate the size to mmap */
    iFrameBufferSize = m_FixInfo.line_length * m_VarInfo.yres;
    printf("Line length %d\n", m_FixInfo.line_length);
    /* Now mmap the framebuffer. */
    m_FrameBuffer = mmap(NULL, iFrameBufferSize, PROT_READ | PROT_WRITE,
    				 MAP_SHARED, m_FBFD,0);
    if (m_FrameBuffer == NULL) {
    	printf("mmap failed:\n");
    	close(m_FBFD);
    	return 1;
    }

    fb->pointer = (char *) m_FrameBuffer;
    fb->width = m_VarInfo.xres;
    fb->height = m_VarInfo.yres;

    return 0;
}

// menyetting warna pixel pada suatu frame_buffer
void setPixel(frame_buffer f, int x, int y, int color) {
    const long int loc_x = (x + m_VarInfo.xoffset) * m_VarInfo.bits_per_pixel / 8;
    const long int loc_y = (y + m_VarInfo.yoffset) * m_FixInfo.line_length;
    *(int*) (f.pointer + loc_x + loc_y) = color;
}

// menginsialisi screen frame_buffer dengan suatu warna
void initialScreen(frame_buffer fb, int color) {
    for (int y = 0; y < fb.height; y++) {
        for (int x = 0; x < fb.width; x++) {
            setPixel(fb, x, y, color);
        }
    }
}

// mencetak nilai karakter 'c' pada frame buffer
// yang memiliki posisi x_start dna y_start
// scale digunakan untuk memperbesar atau memperkecil ukuran font
// color digunakan untuk menyetting warna font
void printChar(char c, frame_buffer fb,  int x_start, int y_start, int scale, int color, int (*char_table)[FONT_HEIGHT][FONT_WIDTH]) {
    int index = from_char_to_index(c);

    for (int j = 0; j < FONT_HEIGHT; j++ ) {
        for (int k = 0; k < FONT_WIDTH; k++ ) {
            if (char_table[index][j][k] - '0' == true) {
                for (int sx = 0; sx < scale; sx++) {
                    for (int sy = 0; sy < scale; sy++) {
                        setPixel(fb, (x_start + k)*scale + sx, (y_start + j)*scale +sy, color);
                    }
                }
            }
        }
    }
}

void my_handler(int s) {
    printf("Caught signal %d", s);
    exit(1);
}

int main() {

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    // load file eksternal
    int font[NUMBER_OF_CHARS][FONT_HEIGHT][FONT_WIDTH];
    int (*char_table)[FONT_HEIGHT][FONT_WIDTH] = font;
    loadFont(FONT_FILE, char_table);

    char *buffer;
    size_t bufsize = 32;
    size_t length;

    buffer = (char *)malloc(bufsize * sizeof(char));
    if (buffer == NULL) {
        perror("Unable to allocate buffer");
        exit(1);
    }

    printf("Type something: ");
    length = getline(&buffer, &bufsize, stdin);
    // printf("%zu characters were read.\n", length);
    // printf("You typed: '%s'\n", buffer);
    
    
    frame_buffer fb;

    //inisialisasi frame buffer
    if (initialFrameBuffer(&fb) == 1)
        return 1;
    
    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = X_START;
    pointer.y = Y_START;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    // initialScreen(fb, SCREEN_COLOR);

    int i = 0; 
    
    for (i = 0; i < length; ++i) {
        
        printChar(buffer[i], fb, pointer.x, pointer.y, FONT_SCALE, FONT_COLOR, char_table);
        pointer.x = pointer.x + FONT_WIDTH + 1; 
        // printf("%c", buffer[i]);

        if (pointer.x*FONT_SCALE + FONT_WIDTH * FONT_SCALE > fb.width) {
            
            // menurunkan posisi y satu line
            pointer.y = pointer.y + FONT_HEIGHT + 1;
            
            // mengembalikan posisi x ke sebelah kiri
            pointer.x = X_START;
        }
    }

    return 0;
}
