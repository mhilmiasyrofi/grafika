#include <bits/stdc++.h>

using namespace std;

int main() {

    int input;
    int i = 0;

    vector<int> vx;
    vector<int> vy;

    ifstream myfile;
	myfile.open ("char.txt");
	if (myfile.is_open())
	{
		//Enter input
		while(myfile.eof() == false)
		{
            myfile >> input;
            if (i%2 == 0) {
                vx.push_back(input);
            } else {
                vy.push_back(input);
            }
            i++;
		}//end while
		myfile.close();
	}//end if

    for (int i = 0; i < vx.size(); i++) {
        for (int j = i+1; j < vx.size(); j++) {
            if (vy[i]  > vy[j]) {
                int tempX = vx[i];
                int tempY = vy[i];
                vx[i] = vx[j];
                vy[i] = vy[j];
                vx[j] = tempX;
                vy[j] = tempY;
            } else if (vy[i] == vy[j] && vx[i] > vx[j]) {
                int tempX = vx[i];
                int tempY = vy[i];
                vx[i] = vx[j];
                vy[i] = vy[j];
                vx[j] = tempX;
                vy[j] = tempY;
            } else if (vy[i] == vy[j] && vx[i] == vx[j]) {
                vx.erase(vx.begin() + j);
                vy.erase(vy.begin() + j);
            }
        }
    }


    for (int i = 0; i < vx.size(); i++) {
        // cout << vx[i] << " " << vy[i] << endl;
    }

    i = 0;

    for (int y = 0; y < 60; y++) {
        for (int x = 0; x < 60; x++) {
            if (vx[i] == x && vy[i] == y){
                cout << "#";
                i++;
            } else {
                cout << " ";
            }
        }
        cout << endl;
    }


    return 0;
}