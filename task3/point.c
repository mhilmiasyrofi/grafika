//
// Created by iqbal on 05/02/18.
//

#include <math.h>
#include <stdio.h>
#include "point.h"

point create_point(int x, int y) {
    point new_point;
    new_point.x = x;
    new_point.y = y;
    return new_point;
}

void print_point(point p) {
    printf("(%d, %d)", p.x, p.y);
}


point translate_point(point p, int dx, int dy) {
    p.x += dx;
    p.y += dy;

    return p;
}

point rotate_point(point p, point pivot, float angle) {

    float radian_angle = angle * M_PI / 180;

    float s = sinf(radian_angle);
    float c = cosf(radian_angle);

    p.x -= pivot.x;
    p.y -= pivot.y;

    int newx = (int) roundf(p.x * c - p.y * s);
    int newy = (int) roundf(p.x * s + p.y * c);

    p.x = newx + pivot.x;
    p.y = newy + pivot.y;
    return p;
}

point dilate_point(point p, point center, float sx, float sy) {
    p.x -= center.x;
    p.y -= center.y;

    int newx = (int) roundf(p.x * sx);
    int newy = (int) roundf(p.y * sy);

    p.x = newx + center.x;
    p.y = newy + center.y;
    return p;
}

point scale_point(point p, float sx, float sy) {
	p.x *= sx;
	p.y *= sy;

	return p;
}
