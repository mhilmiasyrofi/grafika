#include "letter.h"


letter create_letter(char c, line * lines, int count) {
    letter result;
    result.c = c;
    result.lines = lines;
    result.count = count;

    for (int i = 0; i < count; i++){
        lines[i].start.x *= FONT_SCALE;
        lines[i].start.y *= FONT_SCALE;
        lines[i].end.x *= FONT_SCALE;
        lines[i].end.y *= FONT_SCALE;
    }

    int max_width = lines[0].start.x;
    int min_width = lines[0].start.x;
    for (int i = 0; i < count; i++){
        if (lines[i].start.x < min_width)
            min_width = lines[i].start.x;
        if (lines[i].start.x > max_width)
            max_width = lines[i].start.x;
    }
    // result.width = max_width - min_width;
    result.width = max_width + FONT_SCALE;
    
    
    int max_length = lines[0].start.y;
    int min_length = lines[0].start.y;
    for (int i = 0; i < count; i++){
        if (lines[i].start.y < min_length)
            min_length = lines[i].start.y;
        if (lines[i].start.y > max_length)
            max_length = lines[i].start.y;
    }
    // result.length = max_length - min_length;
    result.length = max_length + FONT_SCALE;

    // printf("%d\n", result.width);
    return result;
}


// menggambar huruf pada titik acuan tertentu
void draw_letter(frame_buffer fb, char c, point origin, int color) {

    /*********** Hardcode huruf A **********/
    point points[11];
    points[0] = create_point(0, 10);
    points[1] = create_point(2, 10);
    points[2] = create_point(3, 7);
    points[3] = create_point(7, 7);
    points[4] = create_point(8, 10);
    points[5] = create_point(10, 10);
    points[6] = create_point(6, 0);
    points[7] = create_point(4, 0);
    points[8] = create_point(6, 5);
    points[9] = create_point(4, 5);
    points[10] = create_point(5, 2);

    line lines[11];
    lines[0] = create_line(points[0], points[1]);
    lines[1] = create_line(points[1], points[2]);
    lines[2] = create_line(points[2], points[3]);
    lines[3] = create_line(points[3], points[4]);
    lines[4] = create_line(points[4], points[5]);
    lines[5] = create_line(points[5], points[6]);
    lines[6] = create_line(points[6], points[7]);
    lines[7] = create_line(points[7], points[0]);
    lines[8] = create_line(points[8], points[9]);
    lines[9] = create_line(points[9], points[10]);
    lines[10] = create_line(points[10], points[8]);

    letter letterA = create_letter('A', lines, sizeof(lines) / sizeof(lines[0]));
    /***************************************/

    /*********** Hardcode huruf B **********/
    point pointsB[17];
    pointsB[0] = create_point(3, 0);
    pointsB[1] = create_point(8, 0);
    pointsB[2] = create_point(9, 1);
    pointsB[3] = create_point(9, 3);
    pointsB[4] = create_point(8, 5);
    pointsB[5] = create_point(9, 6);
    pointsB[6] = create_point(9, 8);
    pointsB[7] = create_point(8, 10);
    pointsB[8] = create_point(3, 10);
    pointsB[9] = create_point(5, 2);
    pointsB[10] = create_point(6, 2);
    pointsB[11] = create_point(6, 3);
    pointsB[12] = create_point(5, 3);
    pointsB[13] = create_point(5, 6);
    pointsB[14] = create_point(6, 6);
    pointsB[15] = create_point(6, 7);
    pointsB[16] = create_point(5, 7);

    line linesB[17];
    linesB[0] = create_line(pointsB[0], pointsB[1]);
    linesB[1] = create_line(pointsB[1], pointsB[2]);
    linesB[2] = create_line(pointsB[2], pointsB[3]);
    linesB[3] = create_line(pointsB[3], pointsB[4]);
    linesB[4] = create_line(pointsB[4], pointsB[5]);
    linesB[5] = create_line(pointsB[5], pointsB[6]);
    linesB[6] = create_line(pointsB[6], pointsB[7]);
    linesB[7] = create_line(pointsB[7], pointsB[8]);
    linesB[8] = create_line(pointsB[8], pointsB[0]);
    linesB[9] = create_line(pointsB[9], pointsB[10]);
    linesB[10] = create_line(pointsB[10], pointsB[11]);
    linesB[11] = create_line(pointsB[11], pointsB[12]);
    linesB[12] = create_line(pointsB[12], pointsB[9]);
    linesB[13] = create_line(pointsB[13], pointsB[14]);
    linesB[14] = create_line(pointsB[14], pointsB[15]);
    linesB[15] = create_line(pointsB[15], pointsB[16]);
    linesB[16] = create_line(pointsB[16], pointsB[13]);

    letter letterB = create_letter('B', linesB, sizeof(linesB) / sizeof(linesB[0]));
    /***************************************/

    /*********** Hardcode huruf C **********/
    point pointsC[12];
    pointsC[0] = create_point(5, 0);
    pointsC[1] = create_point(9, 0);
    pointsC[2] = create_point(9, 2);
    pointsC[3] = create_point(6, 2);
    pointsC[4] = create_point(4, 4);
    pointsC[5] = create_point(4, 6);
    pointsC[6] = create_point(6, 8);
    pointsC[7] = create_point(9, 8);
    pointsC[8] = create_point(9, 10);
    pointsC[9] = create_point(5, 10);
    pointsC[10] = create_point(2, 8);
    pointsC[11] = create_point(2, 2);

    line linesC[12];
    linesC[0] = create_line(pointsC[0], pointsC[1]);
    linesC[1] = create_line(pointsC[1], pointsC[2]);
    linesC[2] = create_line(pointsC[2], pointsC[3]);
    linesC[3] = create_line(pointsC[3], pointsC[4]);
    linesC[4] = create_line(pointsC[4], pointsC[5]);
    linesC[5] = create_line(pointsC[5], pointsC[6]);
    linesC[6] = create_line(pointsC[6], pointsC[7]);
    linesC[7] = create_line(pointsC[7], pointsC[8]);
    linesC[8] = create_line(pointsC[8], pointsC[9]);
    linesC[9] = create_line(pointsC[9], pointsC[10]);
    linesC[10] = create_line(pointsC[10], pointsC[11]);
    linesC[11] = create_line(pointsC[11], pointsC[0]);

    letter letterC = create_letter('C', linesC, sizeof(linesC) / sizeof(linesC[0]));
    /***************************************/

    /*********** Hardcode huruf D **********/
    point pointsD[10];
    pointsD[0] = create_point(1, 0);
    pointsD[1] = create_point(4, 0);
    pointsD[2] = create_point(6, 2);
    pointsD[3] = create_point(6, 8);
    pointsD[4] = create_point(4, 10);
    pointsD[5] = create_point(1, 10);
    pointsD[6] = create_point(3, 3);
    pointsD[7] = create_point(4, 4);
    pointsD[8] = create_point(4, 6);
    pointsD[9] = create_point(3, 7);

    line linesD[10];
    linesD[0] = create_line(pointsD[0], pointsD[1]);
    linesD[1] = create_line(pointsD[1], pointsD[2]);
    linesD[2] = create_line(pointsD[2], pointsD[3]);
    linesD[3] = create_line(pointsD[3], pointsD[4]);
    linesD[4] = create_line(pointsD[4], pointsD[5]);
    linesD[5] = create_line(pointsD[5], pointsD[0]);
    linesD[6] = create_line(pointsD[6], pointsD[7]);
    linesD[7] = create_line(pointsD[7], pointsD[8]);
    linesD[8] = create_line(pointsD[8], pointsD[9]);
    linesD[9] = create_line(pointsD[9], pointsD[6]);

    letter letterD = create_letter('D', linesD, sizeof(linesD) / sizeof(linesD[0]));
    /***************************************/

    /*********** Hardcode huruf E **********/
    point pointsE[12];
    pointsE[0] = create_point(1, 0);
    pointsE[1] = create_point(6, 0);
    pointsE[2] = create_point(6, 2);
    pointsE[3] = create_point(3, 2);
    pointsE[4] = create_point(3, 4);
    pointsE[5] = create_point(6, 4);
    pointsE[6] = create_point(6, 6);
    pointsE[7] = create_point(3, 6);
    pointsE[8] = create_point(3, 8);
    pointsE[9] = create_point(6, 8);
    pointsE[10] = create_point(6, 10);
    pointsE[11] = create_point(1, 10);

    line linesE[12];
    linesE[0] = create_line(pointsE[0], pointsE[1]);
    linesE[1] = create_line(pointsE[1], pointsE[2]);
    linesE[2] = create_line(pointsE[2], pointsE[3]);
    linesE[3] = create_line(pointsE[3], pointsE[4]);
    linesE[4] = create_line(pointsE[4], pointsE[5]);
    linesE[5] = create_line(pointsE[5], pointsE[6]);
    linesE[6] = create_line(pointsE[6], pointsE[7]);
    linesE[7] = create_line(pointsE[7], pointsE[8]);
    linesE[8] = create_line(pointsE[8], pointsE[9]);
    linesE[9] = create_line(pointsE[9], pointsE[10]);
    linesE[10] = create_line(pointsE[10], pointsE[11]);
    linesE[11] = create_line(pointsE[11], pointsE[0]);

    letter letterE = create_letter('E', linesE, sizeof(linesE) / sizeof(linesE[0]));
    /***************************************/

    /*********** Hardcode huruf F **********/
    point pointsF[10];
    pointsF[0] = create_point(1, 0);
    pointsF[1] = create_point(6, 0);
    pointsF[2] = create_point(6, 2);
    pointsF[3] = create_point(3, 2);
    pointsF[4] = create_point(3, 4);
    pointsF[5] = create_point(6, 4);
    pointsF[6] = create_point(6, 6);
    pointsF[7] = create_point(3, 6);
    pointsF[8] = create_point(3, 10);
    pointsF[9] = create_point(1, 10);

    line linesF[10];
    linesF[0] = create_line(pointsF[0], pointsF[1]);
    linesF[1] = create_line(pointsF[1], pointsF[2]);
    linesF[2] = create_line(pointsF[2], pointsF[3]);
    linesF[3] = create_line(pointsF[3], pointsF[4]);
    linesF[4] = create_line(pointsF[4], pointsF[5]);
    linesF[5] = create_line(pointsF[5], pointsF[6]);
    linesF[6] = create_line(pointsF[6], pointsF[7]);
    linesF[7] = create_line(pointsF[7], pointsF[8]);
    linesF[8] = create_line(pointsF[8], pointsF[9]);
    linesF[9] = create_line(pointsF[9], pointsF[0]);

    letter letterF = create_letter('F', linesF, sizeof(linesF) / sizeof(linesF[0]));
    /***************************************/

    /*********** Hardcode huruf G **********/
    point pointsG[17];
    pointsG[0] = create_point(2, 0);
    pointsG[1] = create_point(6, 0);
    pointsG[2] = create_point(7, 1);
    pointsG[3] = create_point(7, 3);
    pointsG[4] = create_point(6, 3);
    pointsG[5] = create_point(6, 1);
    pointsG[6] = create_point(2, 1);
    pointsG[7] = create_point(2, 9);
    pointsG[8] = create_point(6, 9);
    pointsG[9] = create_point(6, 7);
    pointsG[10] = create_point(4, 7);
    pointsG[11] = create_point(4, 6);
    pointsG[12] = create_point(7, 6);
    pointsG[13] = create_point(7, 10);
    pointsG[14] = create_point(2, 10);
    pointsG[15] = create_point(1, 9);
    pointsG[16] = create_point(1, 1);

    line linesG[17];
    linesG[0] = create_line(pointsG[0], pointsG[1]);
    linesG[1] = create_line(pointsG[1], pointsG[2]);
    linesG[2] = create_line(pointsG[2], pointsG[3]);
    linesG[3] = create_line(pointsG[3], pointsG[4]);
    linesG[4] = create_line(pointsG[4], pointsG[5]);
    linesG[5] = create_line(pointsG[5], pointsG[6]);
    linesG[6] = create_line(pointsG[6], pointsG[7]);
    linesG[7] = create_line(pointsG[7], pointsG[8]);
    linesG[8] = create_line(pointsG[8], pointsG[9]);
    linesG[9] = create_line(pointsG[9], pointsG[10]);
    linesG[10] = create_line(pointsG[10], pointsG[11]);
    linesG[11] = create_line(pointsG[11], pointsG[12]);
    linesG[12] = create_line(pointsG[12], pointsG[13]);
    linesG[13] = create_line(pointsG[13], pointsG[14]);
    linesG[14] = create_line(pointsG[14], pointsG[15]);
    linesG[15] = create_line(pointsG[15], pointsG[16]);
    linesG[16] = create_line(pointsG[16], pointsG[0]);

    letter letterG = create_letter('G', linesG, sizeof(linesG) / sizeof(linesG[0]));
    /***************************************/

    /*********** Hardcode huruf H **********/
    point pointsH[12];
    pointsH[0] = create_point(1, 0);
    pointsH[1] = create_point(3, 0);
    pointsH[2] = create_point(3, 4);
    pointsH[3] = create_point(7, 4);
    pointsH[4] = create_point(7, 0);
    pointsH[5] = create_point(9, 0);
    pointsH[6] = create_point(9, 10);
    pointsH[7] = create_point(7, 10);
    pointsH[8] = create_point(7, 6);
    pointsH[9] = create_point(3, 6);
    pointsH[10] = create_point(3, 10);
    pointsH[11] = create_point(1, 10);
    
    line linesH[12];
    linesH[0] = create_line(pointsH[0], pointsH[1]);
    linesH[1] = create_line(pointsH[1], pointsH[2]);
    linesH[2] = create_line(pointsH[2], pointsH[3]);
    linesH[3] = create_line(pointsH[3], pointsH[4]);
    linesH[4] = create_line(pointsH[4], pointsH[5]);
    linesH[5] = create_line(pointsH[5], pointsH[6]);
    linesH[6] = create_line(pointsH[6], pointsH[7]);
    linesH[7] = create_line(pointsH[7], pointsH[8]);
    linesH[8] = create_line(pointsH[8], pointsH[9]);
    linesH[9] = create_line(pointsH[9], pointsH[10]);
    linesH[10] = create_line(pointsH[10], pointsH[11]);
    linesH[11] = create_line(pointsH[11], pointsH[0]);
    
    letter letterH = create_letter('H', linesH, sizeof(linesH) / sizeof(linesH[0]));
    /***************************************/

    /*********** Hardcode huruf I **********/
    point pointsI[4];
    pointsI[0] = create_point(2, 0);
    pointsI[1] = create_point(4, 0);
    pointsI[2] = create_point(4, 10);
    pointsI[3] = create_point(2, 10);

    line linesI[4];
    linesI[0] = create_line(pointsI[0], pointsI[1]);
    linesI[1] = create_line(pointsI[1], pointsI[2]);
    linesI[2] = create_line(pointsI[2], pointsI[3]);
    linesI[3] = create_line(pointsI[3], pointsI[0]);

    letter letterI = create_letter('I', linesI, sizeof(linesI) / sizeof(linesI[0]));
    /***************************************/

    /*********** Hardcode huruf J **********/
    point pointsJ[9];
    pointsJ[0] = create_point(8, 1);
    pointsJ[1] = create_point(9, 1);
    pointsJ[2] = create_point(9, 6);
    pointsJ[3] = create_point(7, 10);
    pointsJ[4] = create_point(5, 10);
    pointsJ[5] = create_point(3, 7);
    pointsJ[6] = create_point(4, 7);
    pointsJ[7] = create_point(6, 9);
    pointsJ[8] = create_point(8, 7);

    line linesJ[9];
    linesJ[0] = create_line(pointsJ[0], pointsJ[1]);
    linesJ[1] = create_line(pointsJ[1], pointsJ[2]);
    linesJ[2] = create_line(pointsJ[2], pointsJ[3]);
    linesJ[3] = create_line(pointsJ[3], pointsJ[4]);
    linesJ[4] = create_line(pointsJ[4], pointsJ[5]);
    linesJ[5] = create_line(pointsJ[5], pointsJ[6]);
    linesJ[6] = create_line(pointsJ[6], pointsJ[7]);
    linesJ[7] = create_line(pointsJ[7], pointsJ[8]);
    linesJ[8] = create_line(pointsJ[8], pointsJ[0]);

    letter letterJ = create_letter('J', linesJ, sizeof(linesJ) / sizeof(linesJ[0]));
    /***************************************/

    /*********** Hardcode huruf K **********/
    point pointsK[11];
    pointsK[0] = create_point(2, 0);
    pointsK[1] = create_point(4, 0);
    pointsK[2] = create_point(4, 3);
    pointsK[3] = create_point(7, 0);
    pointsK[4] = create_point(7, 2);
    pointsK[5] = create_point(4, 5);
    pointsK[6] = create_point(7, 8);
    pointsK[7] = create_point(7, 10);
    pointsK[8] = create_point(4, 7);
    pointsK[9] = create_point(4, 10);
    pointsK[10] = create_point(2, 10);

    line linesK[11];
    linesK[0] = create_line(pointsK[0], pointsK[1]);
    linesK[1] = create_line(pointsK[1], pointsK[2]);
    linesK[2] = create_line(pointsK[2], pointsK[3]);
    linesK[3] = create_line(pointsK[3], pointsK[4]);
    linesK[4] = create_line(pointsK[4], pointsK[5]);
    linesK[5] = create_line(pointsK[5], pointsK[6]);
    linesK[6] = create_line(pointsK[6], pointsK[7]);
    linesK[7] = create_line(pointsK[7], pointsK[8]);
    linesK[8] = create_line(pointsK[8], pointsK[9]);
    linesK[9] = create_line(pointsK[9], pointsK[10]);
    linesK[10] = create_line(pointsK[10], pointsK[0]);

    letter letterK = create_letter('K', linesK, sizeof(linesK) / sizeof(linesK[0]));
    /***************************************/

    /*********** Hardcode huruf L **********/
    point pointsL[6];
    pointsL[0] = create_point(1, 0);
    pointsL[1] = create_point(3, 0);
    pointsL[2] = create_point(3, 8);
    pointsL[3] = create_point(8, 8);
    pointsL[4] = create_point(8, 10);
    pointsL[5] = create_point(1, 10);
    
    line linesL[6];
    linesL[0] = create_line(pointsL[0], pointsL[1]);
    linesL[1] = create_line(pointsL[1], pointsL[2]);
    linesL[2] = create_line(pointsL[2], pointsL[3]);
    linesL[3] = create_line(pointsL[3], pointsL[4]);
    linesL[4] = create_line(pointsL[4], pointsL[5]);
    linesL[5] = create_line(pointsL[5], pointsL[0]);
    
    letter letterL = create_letter('L', linesL, sizeof(linesL) / sizeof(linesL[0]));
    /***************************************/

    /*********** Hardcode huruf M **********/
    point pointsM[13];
    pointsM[0] = create_point(2, 0);
    pointsM[1] = create_point(3, 0);
    pointsM[2] = create_point(6, 4);
    pointsM[3] = create_point(9, 0);
    pointsM[4] = create_point(10, 0);
    pointsM[5] = create_point(10, 10);
    pointsM[6] = create_point(9, 10);
    pointsM[7] = create_point(9, 2);
    pointsM[8] = create_point(7, 5);
    pointsM[9] = create_point(5, 5);
    pointsM[10] = create_point(3, 2);
    pointsM[11] = create_point(3, 10);
    pointsM[12] = create_point(2, 10);

    line linesM[13];
    linesM[0] = create_line(pointsM[0], pointsM[1]);
    linesM[1] = create_line(pointsM[1], pointsM[2]);
    linesM[2] = create_line(pointsM[2], pointsM[3]);
    linesM[3] = create_line(pointsM[3], pointsM[4]);
    linesM[4] = create_line(pointsM[4], pointsM[5]);
    linesM[5] = create_line(pointsM[5], pointsM[6]);
    linesM[6] = create_line(pointsM[6], pointsM[7]);
    linesM[7] = create_line(pointsM[7], pointsM[8]);
    linesM[8] = create_line(pointsM[8], pointsM[9]);
    linesM[9] = create_line(pointsM[9], pointsM[10]);
    linesM[10] = create_line(pointsM[10], pointsM[11]);
    linesM[11] = create_line(pointsM[11], pointsM[12]);
    linesM[12] = create_line(pointsM[12], pointsM[0]);

    letter letterM = create_letter('M', linesM, sizeof(linesM) / sizeof(linesM[0]));
    /***************************************/
  
    /*********** Hardcode huruf N **********/
    point pointsN[10];
    pointsN[0] = create_point(1, 0);
    pointsN[1] = create_point(3, 0);
    pointsN[2] = create_point(7, 7);
    pointsN[3] = create_point(7, 0);
    pointsN[4] = create_point(9, 0);
    pointsN[5] = create_point(9, 10);
    pointsN[6] = create_point(7, 10);
    pointsN[7] = create_point(3, 3);
    pointsN[8] = create_point(3, 10);
    pointsN[9] = create_point(1, 10);

    line linesN[10];
    linesN[0] = create_line(pointsN[0], pointsN[1]);
    linesN[1] = create_line(pointsN[1], pointsN[2]);
    linesN[2] = create_line(pointsN[2], pointsN[3]);
    linesN[3] = create_line(pointsN[3], pointsN[4]);
    linesN[4] = create_line(pointsN[4], pointsN[5]);
    linesN[5] = create_line(pointsN[5], pointsN[6]);
    linesN[6] = create_line(pointsN[6], pointsN[7]);
    linesN[7] = create_line(pointsN[7], pointsN[8]);
    linesN[8] = create_line(pointsN[8], pointsN[9]);
    linesN[9] = create_line(pointsN[9], pointsN[0]);

    letter letterN = create_letter('N', linesN, sizeof(linesN) / sizeof(linesK[0]));
    /***************************************/

    /*********** Hardcode huruf O **********/
    point pointsO[16];
    pointsO[0] = create_point(1, 2);
    pointsO[1] = create_point(3, 0);
    pointsO[2] = create_point(7, 0);
    pointsO[3] = create_point(9, 2);
    pointsO[4] = create_point(9, 8);
    pointsO[5] = create_point(7, 10);
    pointsO[6] = create_point(3, 10);
    pointsO[7] = create_point(1, 8);
    pointsO[8] = create_point(3, 4);
    pointsO[9] = create_point(4, 3);
    pointsO[10] = create_point(6, 3);
    pointsO[11] = create_point(7, 4);
    pointsO[12] = create_point(7, 7);
    pointsO[13] = create_point(6, 8);
    pointsO[14] = create_point(4, 8);
    pointsO[15] = create_point(3, 7);
    
    line linesO[16];
    linesO[0] = create_line(pointsO[0], pointsO[1]);
    linesO[1] = create_line(pointsO[1], pointsO[2]);
    linesO[2] = create_line(pointsO[2], pointsO[3]);
    linesO[3] = create_line(pointsO[3], pointsO[4]);
    linesO[4] = create_line(pointsO[4], pointsO[5]);
    linesO[5] = create_line(pointsO[5], pointsO[6]);
    linesO[6] = create_line(pointsO[6], pointsO[7]);
    linesO[7] = create_line(pointsO[7], pointsO[0]);
    linesO[8] = create_line(pointsO[8], pointsO[9]);
    linesO[9] = create_line(pointsO[9], pointsO[10]);
    linesO[10] = create_line(pointsO[10], pointsO[11]);
    linesO[11] = create_line(pointsO[11], pointsO[12]);
    linesO[12] = create_line(pointsO[12], pointsO[13]);
    linesO[13] = create_line(pointsO[13], pointsO[14]);
    linesO[14] = create_line(pointsO[14], pointsO[15]);
    linesO[15] = create_line(pointsO[15], pointsO[8]);
    
    letter letterO = create_letter('O', linesO, sizeof(linesO) / sizeof(linesO[0]));
    /***************************************/

    /*********** Hardcode huruf P **********/
    point pointsP[10];
    pointsP[0] = create_point(1, 0);
    pointsP[1] = create_point(7, 0);
    pointsP[2] = create_point(7, 6);
    pointsP[3] = create_point(3, 6);
    pointsP[4] = create_point(3, 10);
    pointsP[5] = create_point(1, 10);
    pointsP[6] = create_point(3, 4);
    pointsP[7] = create_point(3, 2);
    pointsP[8] = create_point(5, 2);
    pointsP[9] = create_point(5, 4);
    
    line linesP[10];
    linesP[0] = create_line(pointsP[0], pointsP[1]);
    linesP[1] = create_line(pointsP[1], pointsP[2]);
    linesP[2] = create_line(pointsP[2], pointsP[3]);
    linesP[3] = create_line(pointsP[3], pointsP[4]);
    linesP[4] = create_line(pointsP[4], pointsP[5]);
    linesP[5] = create_line(pointsP[5], pointsP[0]);
    linesP[6] = create_line(pointsP[6], pointsP[7]);
    linesP[7] = create_line(pointsP[7], pointsP[8]);
    linesP[8] = create_line(pointsP[8], pointsP[9]);
    linesP[9] = create_line(pointsP[9], pointsP[6]);
    
    letter letterP = create_letter('P', linesP, sizeof(linesP) / sizeof(linesP[0]));
    /***************************************/

    /*********** Hardcode huruf Q **********/
    point pointsQ[19];
    pointsQ[0] = create_point(1, 2);
    pointsQ[1] = create_point(3, 0);
    pointsQ[2] = create_point(7, 0);
    pointsQ[3] = create_point(9, 2);
    pointsQ[4] = create_point(9, 8);
    pointsQ[5] = create_point(10, 10);
    pointsQ[6] = create_point(9, 10);
    pointsQ[7] = create_point(8, 9);
    pointsQ[8] = create_point(7, 10);
    pointsQ[9] = create_point(3, 10);
    pointsQ[10] = create_point(1, 8);
    pointsQ[11] = create_point(3, 4);
    pointsQ[12] = create_point(4, 3);
    pointsQ[13] = create_point(6, 3);
    pointsQ[14] = create_point(7, 4);
    pointsQ[15] = create_point(7, 7);
    pointsQ[16] = create_point(6, 8);
    pointsQ[17] = create_point(4, 8);
    pointsQ[18] = create_point(3, 7);

    line linesQ[19];
    linesQ[0] = create_line(pointsQ[0], pointsQ[1]);
    linesQ[1] = create_line(pointsQ[1], pointsQ[2]);
    linesQ[2] = create_line(pointsQ[2], pointsQ[3]);
    linesQ[3] = create_line(pointsQ[3], pointsQ[4]);
    linesQ[4] = create_line(pointsQ[4], pointsQ[5]);
    linesQ[5] = create_line(pointsQ[5], pointsQ[6]);
    linesQ[6] = create_line(pointsQ[6], pointsQ[7]);
    linesQ[7] = create_line(pointsQ[7], pointsQ[8]);
    linesQ[8] = create_line(pointsQ[8], pointsQ[9]);
    linesQ[9] = create_line(pointsQ[9], pointsQ[10]);
    linesQ[10] = create_line(pointsQ[10], pointsQ[0]);
    linesQ[11] = create_line(pointsQ[11], pointsQ[12]);
    linesQ[12] = create_line(pointsQ[12], pointsQ[13]);
    linesQ[13] = create_line(pointsQ[13], pointsQ[14]);
    linesQ[14] = create_line(pointsQ[14], pointsQ[15]);
    linesQ[15] = create_line(pointsQ[15], pointsQ[16]);
    linesQ[16] = create_line(pointsQ[16], pointsQ[17]);
    linesQ[17] = create_line(pointsQ[17], pointsQ[18]);
    linesQ[18] = create_line(pointsQ[18], pointsQ[11]);

    letter letterQ = create_letter('Q', linesQ, sizeof(linesQ) / sizeof(linesQ[0]));
    /***************************************/

    /*********** Hardcode huruf R **********/
    point pointsR[18];
    pointsR[0] = create_point(2, 0);
    pointsR[1] = create_point(6, 0);
    pointsR[2] = create_point(7, 1);
    pointsR[3] = create_point(7, 4);
    pointsR[4] = create_point(6, 5);
    pointsR[5] = create_point(6, 6);
    pointsR[6] = create_point(7, 7);
    pointsR[7] = create_point(7, 10);
    pointsR[8] = create_point(6, 10);
    pointsR[9] = create_point(6, 7);
    pointsR[10] = create_point(5, 6);
    pointsR[11] = create_point(3, 6);
    pointsR[12] = create_point(3, 10);
    pointsR[13] = create_point(2, 10);
    pointsR[14] = create_point(3, 1);
    pointsR[15] = create_point(5, 1);
    pointsR[16] = create_point(5, 4);
    pointsR[17] = create_point(3, 4);

    line linesR[18];
    linesR[0] = create_line(pointsR[0], pointsR[1]);
    linesR[1] = create_line(pointsR[1], pointsR[2]);
    linesR[2] = create_line(pointsR[2], pointsR[3]);
    linesR[3] = create_line(pointsR[3], pointsR[4]);
    linesR[4] = create_line(pointsR[4], pointsR[5]);
    linesR[5] = create_line(pointsR[5], pointsR[6]);
    linesR[6] = create_line(pointsR[6], pointsR[7]);
    linesR[7] = create_line(pointsR[7], pointsR[8]);
    linesR[8] = create_line(pointsR[8], pointsR[9]);
    linesR[9] = create_line(pointsR[9], pointsR[10]);
    linesR[10] = create_line(pointsR[10], pointsR[11]);
    linesR[11] = create_line(pointsR[11], pointsR[12]);
    linesR[12] = create_line(pointsR[12], pointsR[13]);
    linesR[13] = create_line(pointsR[13], pointsR[0]);
    linesR[14] = create_line(pointsR[14], pointsR[15]);
    linesR[15] = create_line(pointsR[15], pointsR[16]);
    linesR[16] = create_line(pointsR[16], pointsR[17]);
    linesR[17] = create_line(pointsR[17], pointsR[14]);

    letter letterR = create_letter('R', linesR, sizeof(linesR) / sizeof(linesR[0]));
    /***************************************/
    
    /*********** Hardcode huruf S **********/
    point pointsS[12];
    pointsS[0] = create_point(1, 0);
    pointsS[1] = create_point(9, 0);
    pointsS[2] = create_point(9, 2);
    pointsS[3] = create_point(3, 2);
    pointsS[4] = create_point(3, 4);
    pointsS[5] = create_point(9, 4);
    pointsS[6] = create_point(9, 10);
    pointsS[7] = create_point(1, 10);
    pointsS[8] = create_point(1, 8);
    pointsS[9] = create_point(7, 8);
    pointsS[10] = create_point(7, 6);
    pointsS[11] = create_point(1, 6);

    line linesS[12];
    linesS[0] = create_line(pointsS[0], pointsS[1]);
    linesS[1] = create_line(pointsS[1], pointsS[2]);
    linesS[2] = create_line(pointsS[2], pointsS[3]);
    linesS[3] = create_line(pointsS[3], pointsS[4]);
    linesS[4] = create_line(pointsS[4], pointsS[5]);
    linesS[5] = create_line(pointsS[5], pointsS[6]);
    linesS[6] = create_line(pointsS[6], pointsS[7]);
    linesS[7] = create_line(pointsS[7], pointsS[8]);
    linesS[8] = create_line(pointsS[8], pointsS[9]);
    linesS[9] = create_line(pointsS[9], pointsS[10]);
    linesS[10] = create_line(pointsS[10], pointsS[11]);
    linesS[11] = create_line(pointsS[11], pointsS[0]);

    letter letterS = create_letter('S', linesS, sizeof(linesS) / sizeof(linesS[0]));
    /***************************************/

    /*********** Hardcode huruf T **********/
    point pointsT[8];
    pointsT[0] = create_point(1, 0);
    pointsT[1] = create_point(9, 0);
    pointsT[2] = create_point(9, 2);
    pointsT[3] = create_point(6, 2);
    pointsT[4] = create_point(6, 10);
    pointsT[5] = create_point(4, 10);
    pointsT[6] = create_point(4, 2);
    pointsT[7] = create_point(1, 2);
    
    line linesT[8];
    linesT[0] = create_line(pointsT[0], pointsT[1]);
    linesT[1] = create_line(pointsT[1], pointsT[2]);
    linesT[2] = create_line(pointsT[2], pointsT[3]);
    linesT[3] = create_line(pointsT[3], pointsT[4]);
    linesT[4] = create_line(pointsT[4], pointsT[5]);
    linesT[5] = create_line(pointsT[5], pointsT[6]);
    linesT[6] = create_line(pointsT[6], pointsT[7]);
    linesT[7] = create_line(pointsT[7], pointsT[0]);
    
    letter letterT = create_letter('T', linesT, sizeof(linesT) / sizeof(linesT[0]));
    /***************************************/

    /*********** Hardcode huruf U **********/
    point pointsU[8];
    pointsU[0] = create_point(1, 0);
    pointsU[1] = create_point(3, 0);
    pointsU[2] = create_point(3, 8);
    pointsU[3] = create_point(7, 8);
    pointsU[4] = create_point(7, 0);
    pointsU[5] = create_point(9, 0);
    pointsU[6] = create_point(9, 10);
    pointsU[7] = create_point(1, 10);
    
    line linesU[8];
    linesU[0] = create_line(pointsU[0], pointsU[1]);
    linesU[1] = create_line(pointsU[1], pointsU[2]);
    linesU[2] = create_line(pointsU[2], pointsU[3]);
    linesU[3] = create_line(pointsU[3], pointsU[4]);
    linesU[4] = create_line(pointsU[4], pointsU[5]);
    linesU[5] = create_line(pointsU[5], pointsU[6]);
    linesU[6] = create_line(pointsU[6], pointsU[7]);
    linesU[7] = create_line(pointsU[7], pointsU[0]);
    
    letter letterU = create_letter('U', linesU, sizeof(linesU) / sizeof(linesU[0]));
    /***************************************/

    /*********** Hardcode huruf V **********/
    point pointsV[9];
    pointsV[0] = create_point(1, 0);
    pointsV[1] = create_point(3, 0);
    pointsV[2] = create_point(5, 7);
    pointsV[3] = create_point(7, 0);
    pointsV[4] = create_point(9, 0);
    pointsV[5] = create_point(9, 2);
    pointsV[6] = create_point(6, 10);
    pointsV[7] = create_point(4, 10);
    pointsV[8] = create_point(1, 2);
    
    line linesV[9];
    linesV[0] = create_line(pointsV[0], pointsV[1]);
    linesV[1] = create_line(pointsV[1], pointsV[2]);
    linesV[2] = create_line(pointsV[2], pointsV[3]);
    linesV[3] = create_line(pointsV[3], pointsV[4]);
    linesV[4] = create_line(pointsV[4], pointsV[5]);
    linesV[5] = create_line(pointsV[5], pointsV[6]);
    linesV[6] = create_line(pointsV[6], pointsV[7]);
    linesV[7] = create_line(pointsV[7], pointsV[8]);
    linesV[8] = create_line(pointsV[8], pointsV[0]);
    
    letter letterV = create_letter('V', linesV, sizeof(linesV) / sizeof(linesV[0]));
    /***************************************/

    /*********** Hardcode huruf W **********/
    point pointsW[15];
    pointsW[0] = create_point(0, 0);
    pointsW[1] = create_point(2, 0);
    pointsW[2] = create_point(3, 5);
    pointsW[3] = create_point(4, 3);
    pointsW[4] = create_point(6, 3);
    pointsW[5] = create_point(7, 5);
    pointsW[6] = create_point(8, 0);
    pointsW[7] = create_point(10, 0);
    pointsW[8] = create_point(10, 2);
    pointsW[9] = create_point(8, 10);
    pointsW[10] = create_point(6, 10);
    pointsW[11] = create_point(5, 7);
    pointsW[12] = create_point(4, 10);
    pointsW[13] = create_point(2, 10);
    pointsW[14] = create_point(0, 2);

    line linesW[15];
    linesW[0] = create_line(pointsW[0], pointsW[1]);
    linesW[1] = create_line(pointsW[1], pointsW[2]);
    linesW[2] = create_line(pointsW[2], pointsW[3]);
    linesW[3] = create_line(pointsW[3], pointsW[4]);
    linesW[4] = create_line(pointsW[4], pointsW[5]);
    linesW[5] = create_line(pointsW[5], pointsW[6]);
    linesW[6] = create_line(pointsW[6], pointsW[7]);
    linesW[7] = create_line(pointsW[7], pointsW[8]);
    linesW[8] = create_line(pointsW[8], pointsW[9]);
    linesW[9] = create_line(pointsW[9], pointsW[10]);
    linesW[10] = create_line(pointsW[10], pointsW[11]);
    linesW[11] = create_line(pointsW[11], pointsW[12]);
    linesW[12] = create_line(pointsW[12], pointsW[13]);
    linesW[13] = create_line(pointsW[13], pointsW[14]);
    linesW[14] = create_line(pointsW[14], pointsW[0]);

    letter letterW = create_letter('W', linesW, sizeof(linesW) / sizeof(linesW[0]));
    /***************************************/

    /*********** Hardcode huruf X **********/
    point pointsX[12];
    pointsX[0] = create_point(0, 0);
    pointsX[1] = create_point(2, 0);
    pointsX[2] = create_point(5, 4);
    pointsX[3] = create_point(8, 0);
    pointsX[4] = create_point(10, 0);
    pointsX[5] = create_point(7, 5);
    pointsX[6] = create_point(10, 10);
    pointsX[7] = create_point(8, 10);
    pointsX[8] = create_point(5, 6);
    pointsX[9] = create_point(2, 10);
    pointsX[10] = create_point(0, 10);
    pointsX[11] = create_point(3, 5);

    line linesX[12];
    linesX[0] = create_line(pointsX[0], pointsX[1]);
    linesX[1] = create_line(pointsX[1], pointsX[2]);
    linesX[2] = create_line(pointsX[2], pointsX[3]);
    linesX[3] = create_line(pointsX[3], pointsX[4]);
    linesX[4] = create_line(pointsX[4], pointsX[5]);
    linesX[5] = create_line(pointsX[5], pointsX[6]);
    linesX[6] = create_line(pointsX[6], pointsX[7]);
    linesX[7] = create_line(pointsX[7], pointsX[8]);
    linesX[8] = create_line(pointsX[8], pointsX[9]);
    linesX[9] = create_line(pointsX[9], pointsX[10]);
    linesX[10] = create_line(pointsX[10], pointsX[11]);
    linesX[11] = create_line(pointsX[11], pointsX[0]);
    
    letter letterX = create_letter('X', linesX, sizeof(linesX) / sizeof(linesX[0]));
    /***************************************/

    /*********** Hardcode huruf Y **********/
    point pointsY[11];
    pointsY[0] = create_point(1, 0);
    pointsY[1] = create_point(3, 0);
    pointsY[2] = create_point(5, 3);
    pointsY[3] = create_point(7, 0);
    pointsY[4] = create_point(9, 0);
    pointsY[5] = create_point(9, 2);
    pointsY[6] = create_point(6, 5);
    pointsY[7] = create_point(6, 10);
    pointsY[8] = create_point(4, 10);
    pointsY[9] = create_point(4, 5);
    pointsY[10] = create_point(1, 2);

    line linesY[11];
    linesY[0] = create_line(pointsY[0], pointsY[1]);
    linesY[1] = create_line(pointsY[1], pointsY[2]);
    linesY[2] = create_line(pointsY[2], pointsY[3]);
    linesY[3] = create_line(pointsY[3], pointsY[4]);
    linesY[4] = create_line(pointsY[4], pointsY[5]);
    linesY[5] = create_line(pointsY[5], pointsY[6]);
    linesY[6] = create_line(pointsY[6], pointsY[7]);
    linesY[7] = create_line(pointsY[7], pointsY[8]);
    linesY[8] = create_line(pointsY[8], pointsY[9]);
    linesY[9] = create_line(pointsY[9], pointsY[10]);
    linesY[10] = create_line(pointsY[10], pointsY[0]);

    letter letterY = create_letter('Y', linesY, sizeof(linesY) / sizeof(linesY[0]));
    /***************************************/

    /*********** Hardcode huruf Z **********/
    point pointsZ[11];
    pointsZ[0] = create_point(1, 0);
    pointsZ[1] = create_point(9, 0);
    pointsZ[2] = create_point(9, 2);
    pointsZ[3] = create_point(4, 8);
    pointsZ[4] = create_point(9, 8);
    pointsZ[5] = create_point(9, 10);
    pointsZ[6] = create_point(1, 10);
    pointsZ[7] = create_point(1, 8);
    pointsZ[8] = create_point(6, 2);
    pointsZ[9] = create_point(1, 2);

    line linesZ[10];
    linesZ[0] = create_line(pointsZ[0], pointsZ[1]);
    linesZ[1] = create_line(pointsZ[1], pointsZ[2]);
    linesZ[2] = create_line(pointsZ[2], pointsZ[3]);
    linesZ[3] = create_line(pointsZ[3], pointsZ[4]);
    linesZ[4] = create_line(pointsZ[4], pointsZ[5]);
    linesZ[5] = create_line(pointsZ[5], pointsZ[6]);
    linesZ[6] = create_line(pointsZ[6], pointsZ[7]);
    linesZ[7] = create_line(pointsZ[7], pointsZ[8]);
    linesZ[8] = create_line(pointsZ[8], pointsZ[9]);
    linesZ[9] = create_line(pointsZ[9], pointsZ[0]);
    
    letter letterZ = create_letter('Z', linesZ, sizeof(linesZ) / sizeof(linesZ[0]));
    /***************************************/

    /*********** Hardcode huruf Space **********/
    point pointsSpace[2];
    pointsSpace[0] = create_point(1, 0);
    pointsSpace[1] = create_point(10, 0);

    line linesSpace[2];
    linesSpace[0] = create_line(pointsSpace[0], pointsSpace[1]);
    linesSpace[1] = create_line(pointsSpace[1], pointsSpace[0]);

    letter letterSpace = create_letter(' ', linesSpace, sizeof(linesSpace) / sizeof(linesSpace[0]));
    /***************************************/

    letter ch;

    switch (c) {
        case 'A':
        case 'a':
            ch = letterA;
            break;
        case 'B':
        case 'b':
            ch = letterB;
            break;
        case 'C':
        case 'c':
            ch = letterC;
            break;
        case 'D':
        case 'd':
            ch = letterD;
            break;
        case 'E':
        case 'e':
            ch = letterE;
            break;
        case 'F':
        case 'f':
            ch = letterF;
            break;
        case 'G':
        case 'g':
            ch = letterG;
            break;
        case 'H':
        case 'h':
            ch = letterH;
            break;
        case 'I':
        case 'i':
            ch = letterI;
            break;
        case 'J':
        case 'j':
            ch = letterJ;
            break;
        case 'K':
        case 'k':
            ch = letterK;
            break;
        case 'L':
        case 'l':
            ch = letterL;
            break;
        case 'M':
        case 'm':
            ch = letterM;
            break;
        case 'N':
        case 'n':
            ch = letterN;
            break;
        case 'O':
        case 'o':
            ch = letterO;
            break;
        case 'P':
        case 'p':
            ch = letterP;
            break;
        case 'Q':
        case 'q':
            ch = letterQ;
            break;
        case 'R':
        case 'r':
            ch = letterR;
            break;
        case 'S':
        case 's':
            ch = letterS;
            break;
        case 'T':
        case 't':
            ch = letterT;
            break;
        case 'U':
        case 'u':
            ch = letterU;
            break;
        case 'V':
        case 'v':
            ch = letterV;
            break;
        case 'W':
        case 'w':
            ch = letterW;
            break;
        case 'X':
        case 'x':
            ch = letterX;
            break;
        case 'Y':
        case 'y':
            ch = letterY;
            break;
        case 'Z':
        case 'z':
            ch = letterZ;
            break;
        default:
            ch = letterSpace;
    }

    int idx_last = ch.count - 1;
    point start = create_point(ch.lines[idx_last].start.x + origin.x, ch.lines[idx_last].start.y + origin.y);
    point end = create_point(ch.lines[idx_last].end.x + origin.x, ch.lines[idx_last].end.y + origin.y);
    line drawn_line = create_line(start, end);

    float prev_grad, curr_grad;
    prev_grad = drawn_line.gradient;
    int prev_deltaX, curr_deltaX;
    int prev_deltaY, curr_deltaY;
    prev_deltaX = drawn_line.deltaX;
    prev_deltaY = drawn_line.deltaY;

    point exception[10000];
    for (int i = 0; i < 10000; i++) {
        exception[i].x = -1;
        exception[i].y = -1;
    }
    int count = 0;

    for (int i = 0; i < ch.count; i++) {
        start = create_point(ch.lines[i].start.x + origin.x, ch.lines[i].start.y + origin.y);
        end = create_point(ch.lines[i].end.x + origin.x, ch.lines[i].end.y + origin.y);
        drawn_line = create_line(start, end);

        // get exception point
        curr_grad = drawn_line.gradient;
        curr_deltaY = drawn_line.deltaY;
        curr_deltaX = drawn_line.deltaX;
        
        if (curr_deltaY != 0)
            draw_line(fb, drawn_line, 1, color);

        
        if (prev_deltaY < 0 && curr_deltaY > 0) { 
            exception[count].x = start.x;
            exception[count].y = start.y;
            count++;
            // printf("V\n");
            exception[count].x = start.x-1;
            exception[count].y = start.y + 1;
            count++;
            exception[count].x = start.x;
            exception[count].y = start.y + 1;
            count++;
            exception[count].x = start.x+1;
            exception[count].y = start.y + 1;
            count++;
            if (prev_deltaX == 0 || curr_deltaX == 0) {
                exception[count].x = start.x - 2;
                exception[count].y = start.y + 2;
                count++;
                exception[count].x = start.x - 1;
                exception[count].y = start.y + 2;
                count++;
                exception[count].x = start.x;
                exception[count].y = start.y + 2;
                count++;
                exception[count].x = start.x + 1;
                exception[count].y = start.y + 2;
                count++;
                exception[count].x = start.x + 2;
                exception[count].y = start.y + 2;
                count++;
            }
            // printf("%d, %d\n", start.x, start.y);
        } else if (prev_deltaY > 0 && curr_deltaY < 0) {
            exception[count].x = start.x;
            exception[count].y = start.y;
            count++;
            // printf("ATAS\n");
            exception[count].x = start.x-1;
            exception[count].y = start.y - 1;
            count++;
            exception[count].x = start.x;
            exception[count].y = start.y - 1;
            count++;
            exception[count].x = start.x + 1;
            exception[count].y = start.y - 1;
            count++;

            if (prev_deltaX == 0 || curr_deltaX == 0) {
                exception[count].x = start.x - 2;
                exception[count].y = start.y - 2;
                count++;
                exception[count].x = start.x - 1;
                exception[count].y = start.y - 2;
                count++;
                exception[count].x = start.x;
                exception[count].y = start.y - 2;
                count++;
                exception[count].x = start.x + 1;
                exception[count].y = start.y - 2;
                count++;
                exception[count].x = start.x + 2;
                exception[count].y = start.y - 2;
                count++;
            }
            // printf("%d, %d\n", start.x, start.y);
        } else if (curr_deltaY == 0) {
            // if (start.x >= end.x){
            //     for (int j = end.x; j <= start.x; j++) {
            //         exception[count].x = j;
            //         exception[count].y = start.y;
            //         count++;
            //     }
            // } else {
            //     for (int j = start.x; j <= end.x; j++) {
            //         exception[count].x = j;
            //         exception[count].y = start.y;
            //         count++;
            //     }
            // }
        }

        prev_grad = curr_grad;
        prev_deltaX = curr_deltaX;
        prev_deltaY = curr_deltaY;
    }

    // sorting exception
    for (int i = 0; i < count; i++) {
        for (int j = i+1; j < count; j++) {
            if (exception[i].y > exception[j].y) {
                int tempX = exception[i].x;
                int tempY = exception[i].y;
                exception[i].x = exception[j].x;
                exception[i].y = exception[j].y;
                exception[j].x = tempX;
                exception[j].y = tempY;
            } else if (exception[i].y == exception[j].y && exception[i].x > exception[j].x) {
                int tempX = exception[i].x;
                int tempY = exception[i].y;
                exception[i].x = exception[j].x;
                exception[i].y = exception[j].y;
                exception[j].x = tempX;
                exception[j].y = tempY;
            } else if (exception[i].y == exception[j].y && exception[i].x == exception[j].x) {
                exception[j].x = -1;
                exception[j].y = -1;
            }
        }
    }
    
    // for (int i = 0; i < 1000; i++) {
    //     printf("%d %d\n", exception[i].x, exception[i].y);
    // }

    // memberi warna dalam huruf
    int idx = 0;
    boolean isColor = false;
    int color_before = SCREEN_COLOR;
    int color_now;
    int found = 0;

    int copyMat[ch.length][ch.width];
    for (int j = 0; j < ch.length; j++) {
        for (int i = 0; i < ch.width; i++) {
            copyMat[j][i] = getPixel(fb, i + origin.x, j + origin.y);
            // setPixel(fb, i + origin.x + 300, j + origin.y + 300, copyMat[j][i]); // buat debugging doang
        }
    }

    for (int j = 0; j < ch.length; j++) {
        for (int i = 0; i < ch.width; i++) {
            color_now = copyMat[j][i];
            if ((exception[idx].x == i + origin.x) && (exception[idx].y == j + origin.y)) {
                idx++;
                // printf("%d, %d\n", exception[idx].x, exception[idx].y);
            } else {
                if (color_now == color && color_before == SCREEN_COLOR) {
                    found++;
                    if (found % 2 == 0)
                        isColor = false;
                    else
                        isColor = true;
                }
                if (isColor == true) {
                    setPixel(fb, i + origin.x, j + origin.y, color);
                    // printf("%d %d\n", i + origin.x, j + origin.y);
                }
            }
            color_before = color_now;
        }
        color_before = SCREEN_COLOR;
        found = 0;
        isColor = false;
    }
}
