#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef GLOBAL_H
#define GLOBAL_H

#define FONT_COLOR 0x999990
#define FONT_SCALE 3
#define FONT_SPACE 2
#define SCREEN_COLOR 0x000000
#define LINE_COLOR 0x999999
#define WINDOW_COLOR 0x0000ff
#define CURSOR_COLOR 0x00ff00
#define CROSSHAIR_SIZE 20
#define X_START 0
#define Y_START 0
#define INF -999

typedef int boolean;
#define true 1
#define false 0

void delay(int number_of_seconds);

#endif //GLOBAL_H