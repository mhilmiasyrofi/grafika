#include "point.h"
#include "line.h"
#include "fbuffer.h"
#include <math.h>

#ifndef LETTER_H
#define LETTER_H

// tipe data untuk merepresentasikan huruf
typedef struct {
    char c;
    int count;
    line *lines;
    int width;
    int length;
} letter;

letter create_letter(char c, line *lines, int count);

void draw_letter(frame_buffer fb, char c, point origin, int color);

#endif