//
// Created by iqbal on 30/01/18.
//

#include <linux/fb.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <signal.h>
#include <time.h>

typedef int bool;
#define true 1
#define false 0

#define FONT_WIDTH 11
#define FONT_HEIGHT 11
#define FONT_COLOR 0x999990
#define FONT_SCALE 20
#define FONT_SPACE 2
#define SCREEN_COLOR 0x000000

#define X_START 0
#define Y_START 0

// tipe data untuk merepresentasikan suatu frame buffer
typedef struct {
    char* pointer;
    int width;
    int height;
} frame_buffer;

// tipe data untuk merepresentikan titik
typedef struct {
    int x;
    int y;
} point;

point create_point(int x, int y) {
    point result;
    result.x = x;
    result.y = y;
    return result;
}

// tipe data untuk merepresentasikan garis
typedef struct {
    point start;
    point end;
    float gradient;
    int deltaX;
    int deltaY;
} line;

line create_line(point start, point end) {
    line result;
    result.start = start;
    result.end = end;
    result.deltaX = end.x - start.x;
    result.deltaY = end.y - start.y;
    if (result.deltaX != 0)
        result.gradient = (float) ((float) (result.deltaY) / (float) (result.deltaX));
    else
        result.gradient = -9999;

    return result;
}

// tipe data untuk merepresentasikan huruf
typedef struct {
    char c;
    int count;
    line * lines;
    int width;
    int length;
} letter;

letter create_letter(char c, line * lines, int count) {
    letter result;
    result.c = c;
    result.lines = lines;
    result.count = count;

    for (int i = 0; i < count; i++){
        lines[i].start.x *= FONT_SCALE;
        lines[i].start.y *= FONT_SCALE;
        lines[i].end.x *= FONT_SCALE;
        lines[i].end.y *= FONT_SCALE;
    }

    int max_width = lines[0].start.x;
    int min_width = lines[0].start.x;
    for (int i = 0; i < count; i++){
        if (lines[i].start.x < min_width)
            min_width = lines[i].start.x;
        if (lines[i].start.x > max_width)
            max_width = lines[i].start.x;
    }
    // result.width = max_width - min_width;
    result.width = max_width + FONT_SCALE;
    
    
    int max_length = lines[0].start.y;
    int min_length = lines[0].start.y;
    for (int i = 0; i < count; i++){
        if (lines[i].start.y < min_length)
            min_length = lines[i].start.y;
        if (lines[i].start.y > max_length)
            max_length = lines[i].start.y;
    }
    // result.length = max_length - min_length;
    result.length = max_length + FONT_SCALE;

    // printf("%d\n", result.width);
    return result;
}

const char *FB_NAME = "/dev/fb0";
void*   m_FrameBuffer;
struct  fb_fix_screeninfo m_FixInfo;
struct  fb_var_screeninfo m_VarInfo;
int 	m_FBFD;

// menginisialisasi frame_buffer
int initialFrameBuffer(frame_buffer *fb) {

    int iFrameBufferSize;
    /* Open the framebuffer device in read write */
    m_FBFD = open(FB_NAME, O_RDWR);
    if (m_FBFD < 0) {
        printf("Unable to open %s.\n", FB_NAME);
        return 1;
    }
    /* Do Ioctl. Retrieve fixed screen info. */
    if (ioctl(m_FBFD, FBIOGET_FSCREENINFO, &m_FixInfo) < 0) {
        printf("get fixed screen info failed: %s\n",
               strerror(errno));
        close(m_FBFD);
        return 1;
    }
    /* Do Ioctl. Get the variable screen info. */
    if (ioctl(m_FBFD, FBIOGET_VSCREENINFO, &m_VarInfo) < 0) {
        printf("Unable to retrieve variable screen info: %s\n",
               strerror(errno));
        close(m_FBFD);
        return 1;
    }

    /* Calculate the size to mmap */
    iFrameBufferSize = m_FixInfo.line_length * m_VarInfo.yres;
    printf("Line length %d\n", m_FixInfo.line_length);
    /* Now mmap the framebuffer. */
    m_FrameBuffer = mmap(NULL, iFrameBufferSize, PROT_READ | PROT_WRITE,
                         MAP_SHARED, m_FBFD,0);
    if (m_FrameBuffer == NULL) {
        printf("mmap failed:\n");
        close(m_FBFD);
        return 1;
    }

    fb->pointer = (char *) m_FrameBuffer;
    fb->width = m_VarInfo.xres;
    fb->height = m_VarInfo.yres;

    return 0;
}

// memastikan suatu titik berada dalam range frame buffer yang tepat
void validatePoint(frame_buffer fb, point p) {

    if (p.x > fb.width || p.x < 0){
        printf("Index X out of range\n");
        exit(1);
    }

    if (p.y > fb.height || p.y < 0){
        printf("Index Y out of range\n");
        exit(1);
    }
}

// menyetting warna pixel pada suatu frame_buffer
void setPixel(frame_buffer f, int x, int y, int color) {
    point p;
    p.x = x;
    p.y = y;
    validatePoint(f, p);
    const long int loc_x = (x + m_VarInfo.xoffset) * m_VarInfo.bits_per_pixel / 8;
    const long int loc_y = (y + m_VarInfo.yoffset) * m_FixInfo.line_length;
    *(int*) (f.pointer + loc_x + loc_y) = color;
}

int getPixel(frame_buffer f, int x, int y) {
    point p;
    p.x = x;
    p.y = y;
    validatePoint(f, p);
    const long int loc_x = (x + m_VarInfo.xoffset) * m_VarInfo.bits_per_pixel / 8;
    const long int loc_y = (y + m_VarInfo.yoffset) * m_FixInfo.line_length;
    return *(int *)(f.pointer + loc_x + loc_y);
}

// menginsialisi screen frame_buffer dengan suatu warna
void initialScreen(frame_buffer fb, int color) {
    for (int y = 0; y < fb.height; y++) {
        for (int x = 0; x < fb.width; x++) {
            setPixel(fb, x, y, color);
        }
    }
}

// menggambar garis dengan 0 < gradien < 1
void draw_line_low(frame_buffer fb, point start, point end, int scale, int color) {
    int dx, dy, yi, p, x, y;

    dx = end.x - start.x;
    dy = end.y - start.y;
    if (dy >= 0) {
        yi = 1;
    } else {
        yi = -1;
        dy = -dy;
    }

    p = 2*dy - dx;
    y = start.y;
    for (x = start.x; x <= end.x; x++) {
        setPixel(fb, x, y, color);
        // printf("%d %d\n", x, y);
        if (p > 0) {
            y += yi;
            p -= 2*dx;
        }
        p += 2*dy;
    }
}

// menggambar garis dengan gradien > 1
void draw_line_high(frame_buffer fb, point start, point end, int scale, int color) {
    int dx, dy, xi, p, x, y;

    dx = end.x - start.x;
    dy = end.y - start.y;
    if (dx >= 0) {
        xi = 1;
    } else {
        xi = -1;
        dx = -dx;
    }

    p = 2*dx - dy;
    x = start.x;
    for (y = start.y; y <= end.y; y++) {
        setPixel(fb, x, y, color);
        // printf("%d %d\n", x, y);
        if (p > 0) {
            x += xi;
            p -= 2*dy;
        }
        p += 2*dx;
    }
}

// menggambar garis dari titik start ke titik end
void draw_line(frame_buffer fb, line a_line, int scale, int color) {
    point start = a_line.start;
    point end = a_line.end;

    if (abs(end.y - start.y) < abs(end.x - start.x)) {
        if (start.x < end.x) {
            draw_line_low(fb, start, end, scale, color);
        } else {
            draw_line_low(fb, end, start, scale, color);
        }
    } else {
        if (start.y < end.y) {
            draw_line_high(fb, start, end, scale, color);
        } else {
            draw_line_high(fb, end, start, scale, color);
        }
    }
}

// menggambar huruf pada titik acuan tertentu
void draw_letter(frame_buffer fb, letter ch, point origin, int color) {

    int idx_last = ch.count - 1;
    point start = create_point(ch.lines[idx_last].start.x + origin.x, ch.lines[idx_last].start.y + origin.y);
    point end = create_point(ch.lines[idx_last].end.x + origin.x, ch.lines[idx_last].end.y + origin.y);
    line drawn_line = create_line(start, end);

    float prev_grad, curr_grad;
    prev_grad = drawn_line.gradient;
    int prev_deltaY, curr_deltaY;
    prev_deltaY = drawn_line.deltaY;

    point exception[10000];
    for (int i = 0; i < 10000; i++) {
        exception[i].x = -1;
        exception[i].y = -1;
    }

    int count = 0;

    for (int i = 0; i < ch.count; i++) {
        start = create_point(ch.lines[i].start.x + origin.x, ch.lines[i].start.y + origin.y);
        end = create_point(ch.lines[i].end.x + origin.x, ch.lines[i].end.y + origin.y);
        drawn_line = create_line(start, end);
        
        draw_line(fb, drawn_line, 1, color);

        // get exception point
        curr_grad = drawn_line.gradient;
        curr_deltaY = drawn_line.deltaY;
        if (prev_grad < 0 && curr_grad > 0 && prev_deltaY < 0 && curr_deltaY > 0) {
            exception[count].x = start.x;
            exception[count].y = start.y;
            // printf("BAWAH\n");
            count++;
            exception[count].x = start.x-1;
            exception[count].y = start.y+1;
            count++;
            exception[count].x = start.x;
            exception[count].y = start.y+1;
            count++;
            exception[count].x = start.x+1;
            exception[count].y = start.y+1;
            count++;
            // printf("%d, %d\n", start.x, start.y);
        } else if (prev_grad > 0 && curr_grad < 0 && prev_deltaY > 0 && curr_deltaY < 0) {
            exception[count].x = start.x;
            exception[count].y = start.y;
            count++;
            // printf("ATAS\n");
            exception[count].x = start.x-1;
            exception[count].y = start.y-1;
            count++;
            exception[count].x = start.x;
            exception[count].y = start.y-1;
            count++;
            exception[count].x = start.x + 1;
            exception[count].y = start.y-1;
            count++;
            // printf("%d, %d\n", start.x, start.y);
        } else if (curr_deltaY == 0) {
            if (start.x >= end.x){
                for (int j = end.x; j <= start.x; j++) {
                    exception[count].x = j;
                    exception[count].y = start.y;
                    count++;
                }
            } else {
                for (int j = start.x; j <= end.x; j++) {
                    exception[count].x = j;
                    exception[count].y = start.y;
                    count++;
                }
            }
        }

        prev_grad = curr_grad;
        prev_deltaY = curr_deltaY;
    }

    // sorting exception
    for (int i = 0; i < count; i++) {
        for (int j = i+1; j < count; j++) {
            if (exception[i].y > exception[j].y) {
                int tempX = exception[i].x;
                int tempY = exception[i].y;
                exception[i].x = exception[j].x;
                exception[i].y = exception[j].y;
                exception[j].x = tempX;
                exception[j].y = tempY;
            } else if (exception[i].y == exception[j].y && exception[i].x > exception[j].x) {
                int tempX = exception[i].x;
                int tempY = exception[i].y;
                exception[i].x = exception[j].x;
                exception[i].y = exception[j].y;
                exception[j].x = tempX;
                exception[j].y = tempY;
            } else if (exception[i].y == exception[j].y && exception[i].x == exception[j].x) {
                exception[j].x = -1;
                exception[j].y = -1;
            }
        }
    }

    
    for (int i = 0; i < 100; i++) {
        // printf("%d %d\n", exception[i].x, exception[i].y);
    }

    // printf("%d\n", color);
    // setPixel(fb, 1, 1, color);
    // printf("%d\n", getPixel(fb, 1, 1));

    
    // memberi warna dalam huruf
    int idx = 0;
    bool isColor = false;
    int color_before = SCREEN_COLOR;
    int color_now;
    int found = 0;

    int copyMat[ch.length][ch.width];
    for (int j = 0; j < ch.length; j++) {
        for (int i = 0; i < ch.width; i++) {
            copyMat[j][i] = getPixel(fb, i + origin.x, j + origin.y);
            setPixel(fb, i + origin.x + 300, j + origin.y + 300, copyMat[j][i]); // buat debugging doang
        }
    }

    for (int j = 0; j < ch.length; j++) {
        for (int i = 0; i < ch.width; i++) {
            color_now = copyMat[j][i];
            if ((exception[idx].x == i + origin.x) && (exception[idx].y == j + origin.y)) {
                idx++;
                // printf("%d, %d\n", exception[idx].x, exception[idx].y);
            } else {
                if (color_now == color && color_before == SCREEN_COLOR) {
                    found++;
                    if (found % 2 == 0)
                        isColor = false;
                    else
                        isColor = true;
                }
                if (isColor == true) {
                    setPixel(fb, i + origin.x, j + origin.y, color);
                    // printf("%d %d\n", i + origin.x, j + origin.y);
                }
            }
            color_before = color_now;
        }
        color_before = SCREEN_COLOR;
        found = 0;
        isColor = false;
    }


}

void my_handler(int s) {
    printf("Caught signal %d", s);
    exit(1);
}


int main() {

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    /*********** Hardcode huruf A **********/
    point points[11];
    points[0] = create_point(0, 10);
    points[1] = create_point(2, 10);
    points[2] = create_point(3, 7);
    points[3] = create_point(7, 7);
    points[4] = create_point(8, 10);
    points[5] = create_point(10, 10);
    points[6] = create_point(6, 0);
    points[7] = create_point(4, 0);
    points[8] = create_point(4, 5);
    points[9] = create_point(5, 2);
    points[10] = create_point(6, 5);

    line lines[11];
    lines[0] = create_line(points[0], points[1]);
    lines[1] = create_line(points[1], points[2]);
    lines[2] = create_line(points[2], points[3]);
    lines[3] = create_line(points[3], points[4]);
    lines[4] = create_line(points[4], points[5]);
    lines[5] = create_line(points[5], points[6]);
    lines[6] = create_line(points[6], points[7]);
    lines[7] = create_line(points[7], points[0]);
    lines[8] = create_line(points[8], points[9]);
    lines[9] = create_line(points[9], points[10]);
    lines[10] = create_line(points[10], points[8]);

    letter letterA = create_letter('A', lines, sizeof(lines)/sizeof(lines[0]));
    /***************************************/
    
    /*********** Hardcode huruf B **********/
    point pointsB[17];
    pointsB[0] = create_point(3, 2);
    pointsB[1] = create_point(8, 2);
    pointsB[2] = create_point(9, 3);
    pointsB[3] = create_point(9, 5);
    pointsB[4] = create_point(8, 6);
    pointsB[5] = create_point(9, 7);
    pointsB[6] = create_point(9, 9);
    pointsB[7] = create_point(8, 10);
    pointsB[8] = create_point(3, 10);
    pointsB[9] = create_point(5, 3);
    pointsB[10] = create_point(6, 3);
    pointsB[11] = create_point(6, 4);
    pointsB[12] = create_point(5, 4);
    pointsB[13] = create_point(5, 7);
    pointsB[14] = create_point(6, 7);
    pointsB[15] = create_point(6, 8);
    pointsB[16] = create_point(5, 8);

    line linesB[17];
    linesB[0] = create_line(pointsB[0], pointsB[1]);
    linesB[1] = create_line(pointsB[1], pointsB[2]);
    linesB[2] = create_line(pointsB[2], pointsB[3]);
    linesB[3] = create_line(pointsB[3], pointsB[4]);
    linesB[4] = create_line(pointsB[4], pointsB[5]);
    linesB[5] = create_line(pointsB[5], pointsB[6]);
    linesB[6] = create_line(pointsB[6], pointsB[7]);
    linesB[7] = create_line(pointsB[7], pointsB[8]);
    linesB[8] = create_line(pointsB[8], pointsB[0]);
    linesB[9] = create_line(pointsB[9], pointsB[10]);
    linesB[10] = create_line(pointsB[10], pointsB[11]);
    linesB[11] = create_line(pointsB[11], pointsB[12]);
    linesB[12] = create_line(pointsB[12], pointsB[9]);
    linesB[13] = create_line(pointsB[13], pointsB[14]);
    linesB[14] = create_line(pointsB[14], pointsB[15]);
    linesB[15] = create_line(pointsB[15], pointsB[16]);
    linesB[16] = create_line(pointsB[16], pointsB[13]);
    

    letter letterB = create_letter('B', linesB, sizeof(linesB)/sizeof(linesB[0]));
    /***************************************/

    /*********** Hardcode huruf C **********/
    point pointsC[12];
    pointsC[0] = create_point(5, 1);
    pointsC[1] = create_point(9, 1);
    pointsC[2] = create_point(9, 3);
    pointsC[3] = create_point(6, 3);
    pointsC[4] = create_point(4, 5);
    pointsC[5] = create_point(4, 6);
    pointsC[6] = create_point(6, 8);
    pointsC[7] = create_point(9, 8);
    pointsC[8] = create_point(9, 10);
    pointsC[9] = create_point(5, 10);
    pointsC[10] = create_point(2, 8);
    pointsC[11] = create_point(2, 3);

    line linesC[12];
    linesC[0] = create_line(pointsC[0], pointsC[1]);
    linesC[1] = create_line(pointsC[1], pointsC[2]);
    linesC[2] = create_line(pointsC[2], pointsC[3]);
    linesC[3] = create_line(pointsC[3], pointsC[4]);
    linesC[4] = create_line(pointsC[4], pointsC[5]);
    linesC[5] = create_line(pointsC[5], pointsC[6]);
    linesC[6] = create_line(pointsC[6], pointsC[7]);
    linesC[7] = create_line(pointsC[7], pointsC[8]);
    linesC[8] = create_line(pointsC[8], pointsC[9]);
    linesC[9] = create_line(pointsC[9], pointsC[10]);
    linesC[10] = create_line(pointsC[10], pointsC[11]);
    linesC[11] = create_line(pointsC[11], pointsC[0]);

    letter letterC = create_letter('C', linesC, sizeof(linesC) / sizeof(linesC[0]));
    /***************************************/

    /*********** Hardcode huruf D **********/
    point pointsD[10];
    pointsD[0] = create_point(1, 0);
    pointsD[1] = create_point(4, 0);
    pointsD[2] = create_point(6, 2);
    pointsD[3] = create_point(6, 8);
    pointsD[4] = create_point(4, 10);
    pointsD[5] = create_point(1, 10);
    pointsD[6] = create_point(3, 3);
    pointsD[7] = create_point(4, 4);
    pointsD[8] = create_point(4, 6);
    pointsD[9] = create_point(3, 7);

    line linesD[10];
    linesD[0] = create_line(pointsD[0], pointsD[1]);
    linesD[1] = create_line(pointsD[1], pointsD[2]);
    linesD[2] = create_line(pointsD[2], pointsD[3]);
    linesD[3] = create_line(pointsD[3], pointsD[4]);
    linesD[4] = create_line(pointsD[4], pointsD[5]);
    linesD[5] = create_line(pointsD[5], pointsD[0]);
    linesD[6] = create_line(pointsD[6], pointsD[7]);
    linesD[7] = create_line(pointsD[7], pointsD[8]);
    linesD[8] = create_line(pointsD[8], pointsD[9]);
    linesD[9] = create_line(pointsD[9], pointsD[6]);

    letter letterD = create_letter('D', linesD, sizeof(linesD) / sizeof(linesD[0]));
    /***************************************/

    /*********** Hardcode huruf E **********/
    point pointsE[12];
    pointsE[0] = create_point(1, 0);
    pointsE[1] = create_point(6, 0);
    pointsE[2] = create_point(6, 2);
    pointsE[3] = create_point(3, 2);
    pointsE[4] = create_point(3, 4);
    pointsE[5] = create_point(6, 4);
    pointsE[6] = create_point(6, 6);
    pointsE[7] = create_point(3, 6);
    pointsE[8] = create_point(3, 8);
    pointsE[9] = create_point(6, 8);
    pointsE[10] = create_point(6, 10);
    pointsE[11] = create_point(1, 10);

    line linesE[12];
    linesE[0] = create_line(pointsE[0], pointsE[1]);
    linesE[1] = create_line(pointsE[1], pointsE[2]);
    linesE[2] = create_line(pointsE[2], pointsE[3]);
    linesE[3] = create_line(pointsE[3], pointsE[4]);
    linesE[4] = create_line(pointsE[4], pointsE[5]);
    linesE[5] = create_line(pointsE[5], pointsE[6]);
    linesE[6] = create_line(pointsE[6], pointsE[7]);
    linesE[7] = create_line(pointsE[7], pointsE[8]);
    linesE[8] = create_line(pointsE[8], pointsE[9]);
    linesE[9] = create_line(pointsE[9], pointsE[10]);
    linesE[10] = create_line(pointsE[10], pointsE[11]);
    linesE[11] = create_line(pointsE[11], pointsE[0]);

    letter letterE = create_letter('E', linesE, sizeof(linesE) / sizeof(linesE[0]));
    /***************************************/

    /*********** Hardcode huruf F **********/
    point pointsF[10];
    pointsF[0] = create_point(1, 0);
    pointsF[1] = create_point(6, 0);
    pointsF[2] = create_point(6, 2);
    pointsF[3] = create_point(3, 2);
    pointsF[4] = create_point(3, 4);
    pointsF[5] = create_point(6, 4);
    pointsF[6] = create_point(6, 6);
    pointsF[7] = create_point(3, 6);
    pointsF[8] = create_point(3, 10);
    pointsF[9] = create_point(1, 10);

    line linesF[10];
    linesF[0] = create_line(pointsF[0], pointsF[1]);
    linesF[1] = create_line(pointsF[1], pointsF[2]);
    linesF[2] = create_line(pointsF[2], pointsF[3]);
    linesF[3] = create_line(pointsF[3], pointsF[4]);
    linesF[4] = create_line(pointsF[4], pointsF[5]);
    linesF[5] = create_line(pointsF[5], pointsF[6]);
    linesF[6] = create_line(pointsF[6], pointsF[7]);
    linesF[7] = create_line(pointsF[7], pointsF[8]);
    linesF[8] = create_line(pointsF[8], pointsF[9]);
    linesF[9] = create_line(pointsF[9], pointsF[0]);

    letter letterF = create_letter('F', linesF, sizeof(linesF) / sizeof(linesF[0]));
    /***************************************/

    /*********** Hardcode huruf G **********/
    point pointsG[17];
    pointsG[0] = create_point(2, 0);
    pointsG[1] = create_point(6, 0);
    pointsG[2] = create_point(7, 1);
    pointsG[3] = create_point(7, 3);
    pointsG[4] = create_point(6, 3);
    pointsG[5] = create_point(6, 1);
    pointsG[6] = create_point(2, 1);
    pointsG[7] = create_point(2, 9);
    pointsG[8] = create_point(6, 9);
    pointsG[9] = create_point(6, 7);
    pointsG[10] = create_point(4, 7);
    pointsG[11] = create_point(4, 6);
    pointsG[12] = create_point(7, 6);
    pointsG[13] = create_point(7, 10);
    pointsG[14] = create_point(2, 10);
    pointsG[15] = create_point(1, 9);
    pointsG[16] = create_point(1, 1);

    line linesG[17];
    linesG[0] = create_line(pointsG[0], pointsG[1]);
    linesG[1] = create_line(pointsG[1], pointsG[2]);
    linesG[2] = create_line(pointsG[2], pointsG[3]);
    linesG[3] = create_line(pointsG[3], pointsG[4]);
    linesG[4] = create_line(pointsG[4], pointsG[5]);
    linesG[5] = create_line(pointsG[5], pointsG[6]);
    linesG[6] = create_line(pointsG[6], pointsG[7]);
    linesG[7] = create_line(pointsG[7], pointsG[8]);
    linesG[8] = create_line(pointsG[8], pointsG[9]);
    linesG[9] = create_line(pointsG[9], pointsG[10]);
    linesG[10] = create_line(pointsG[10], pointsG[11]);
    linesG[11] = create_line(pointsG[11], pointsG[12]);
    linesG[12] = create_line(pointsG[12], pointsG[13]);
    linesG[13] = create_line(pointsG[13], pointsG[14]);
    linesG[14] = create_line(pointsG[14], pointsG[15]);
    linesG[15] = create_line(pointsG[15], pointsG[16]);
    linesG[16] = create_line(pointsG[16], pointsG[0]);

    letter letterG = create_letter('G', linesG, sizeof(linesG) / sizeof(linesG[0]));
    /***************************************/

    /*********** Hardcode huruf I **********/
    point pointsI[4];
    pointsI[0] = create_point(2, 0);
    pointsI[1] = create_point(4, 0);
    pointsI[2] = create_point(4, 10);
    pointsI[3] = create_point(2, 10);

    line linesI[4];
    linesI[0] = create_line(pointsI[0], pointsI[1]);
    linesI[1] = create_line(pointsI[1], pointsI[2]);
    linesI[2] = create_line(pointsI[2], pointsI[3]);
    linesI[3] = create_line(pointsI[3], pointsI[0]);
    
    letter letterI = create_letter('I', linesI, sizeof(linesI)/sizeof(linesI[0]));
    /***************************************/

    /*********** Hardcode huruf J **********/
    point pointsJ[9];
    pointsJ[0] = create_point(8, 1);
    pointsJ[1] = create_point(9, 1);
    pointsJ[2] = create_point(9, 6);
    pointsJ[3] = create_point(7, 10);
    pointsJ[4] = create_point(5, 10);
    pointsJ[5] = create_point(3, 7);
    pointsJ[6] = create_point(4, 7);
    pointsJ[7] = create_point(6, 9);
    pointsJ[8] = create_point(8, 7);

    line linesJ[9];
    linesJ[0] = create_line(pointsJ[0], pointsJ[1]);
    linesJ[1] = create_line(pointsJ[1], pointsJ[2]);
    linesJ[2] = create_line(pointsJ[2], pointsJ[3]);
    linesJ[3] = create_line(pointsJ[3], pointsJ[4]);
    linesJ[4] = create_line(pointsJ[4], pointsJ[5]);
    linesJ[5] = create_line(pointsJ[5], pointsJ[6]);
    linesJ[6] = create_line(pointsJ[6], pointsJ[7]);
    linesJ[7] = create_line(pointsJ[7], pointsJ[8]);
    linesJ[8] = create_line(pointsJ[8], pointsJ[0]);
    
    letter letterJ = create_letter('J', linesJ, sizeof(linesJ)/sizeof(linesJ[0]));
    /***************************************/

    /*********** Hardcode huruf K **********/
    point pointsK[11];
    pointsK[0] = create_point(2, 0);
    pointsK[1] = create_point(4, 0);
    pointsK[2] = create_point(4, 3);
    pointsK[3] = create_point(7, 0);
    pointsK[4] = create_point(7, 2);
    pointsK[5] = create_point(4, 5);
    pointsK[6] = create_point(7, 8);
    pointsK[7] = create_point(7, 10);
    pointsK[8] = create_point(4, 7);
    pointsK[9] = create_point(4, 10);
    pointsK[10] = create_point(2, 10);

    line linesK[11];
    linesK[0] = create_line(pointsK[0], pointsK[1]);
    linesK[1] = create_line(pointsK[1], pointsK[2]);
    linesK[2] = create_line(pointsK[2], pointsK[3]);
    linesK[3] = create_line(pointsK[3], pointsK[4]);
    linesK[4] = create_line(pointsK[4], pointsK[5]);
    linesK[5] = create_line(pointsK[5], pointsK[6]);
    linesK[6] = create_line(pointsK[6], pointsK[7]);
    linesK[7] = create_line(pointsK[7], pointsK[8]);
    linesK[8] = create_line(pointsK[8], pointsK[9]);
    linesK[9] = create_line(pointsK[9], pointsK[10]);
    linesK[10] = create_line(pointsK[10], pointsK[0]);
    
    letter letterK = create_letter('K', linesK, sizeof(linesK)/sizeof(linesK[0]));
    /***************************************/

    /*********** Hardcode huruf M **********/
    point pointsM[13];
    pointsM[0] = create_point(2, 1);
    pointsM[1] = create_point(3, 1);
    pointsM[2] = create_point(6, 4);
    pointsM[3] = create_point(9, 1);
    pointsM[4] = create_point(10, 1);
    pointsM[5] = create_point(10, 10);
    pointsM[6] = create_point(9, 10);
    pointsM[7] = create_point(9, 2);
    pointsM[8] = create_point(7, 5);
    pointsM[9] = create_point(5, 5);
    pointsM[10] = create_point(3, 2);
    pointsM[11] = create_point(3, 10);
    pointsM[12] = create_point(2, 10);

    line linesM[13];
    linesM[0] = create_line(pointsM[0], pointsM[1]);
    linesM[1] = create_line(pointsM[1], pointsM[2]);
    linesM[2] = create_line(pointsM[2], pointsM[3]);
    linesM[3] = create_line(pointsM[3], pointsM[4]);
    linesM[4] = create_line(pointsM[4], pointsM[5]);
    linesM[5] = create_line(pointsM[5], pointsM[6]);
    linesM[6] = create_line(pointsM[6], pointsM[7]);
    linesM[7] = create_line(pointsM[7], pointsM[8]);
    linesM[8] = create_line(pointsM[8], pointsM[9]);
    linesM[9] = create_line(pointsM[9], pointsM[10]);
    linesM[10] = create_line(pointsM[10], pointsM[11]);
    linesM[11] = create_line(pointsM[11], pointsM[12]);
    linesM[12] = create_line(pointsM[12], pointsM[0]);

    letter letterM = create_letter('M', linesM, sizeof(linesM) / sizeof(linesM[0]));
    /***************************************/

    /*********** Hardcode huruf R **********/
    point pointsR[18];
    pointsR[0] = create_point(2, 0);
    pointsR[1] = create_point(6, 0);
    pointsR[2] = create_point(7, 1);
    pointsR[3] = create_point(7, 4);
    pointsR[4] = create_point(6, 5);
    pointsR[5] = create_point(6, 6);
    pointsR[6] = create_point(7, 7);
    pointsR[7] = create_point(7, 10);
    pointsR[8] = create_point(6, 10);
    pointsR[9] = create_point(6, 7);
    pointsR[10] = create_point(5, 6);
    pointsR[11] = create_point(3, 6);
    pointsR[12] = create_point(3, 10);
    pointsR[13] = create_point(2, 10);
    pointsR[14] = create_point(3, 1);
    pointsR[15] = create_point(5, 1);
    pointsR[16] = create_point(5, 4);
    pointsR[17] = create_point(3, 4);

    line linesR[18];
    linesR[0] = create_line(pointsR[0], pointsR[1]);
    linesR[1] = create_line(pointsR[1], pointsR[2]);
    linesR[2] = create_line(pointsR[2], pointsR[3]);
    linesR[3] = create_line(pointsR[3], pointsR[4]);
    linesR[4] = create_line(pointsR[4], pointsR[5]);
    linesR[5] = create_line(pointsR[5], pointsR[6]);
    linesR[6] = create_line(pointsR[6], pointsR[7]);
    linesR[7] = create_line(pointsR[7], pointsR[8]);
    linesR[8] = create_line(pointsR[8], pointsR[9]);
    linesR[9] = create_line(pointsR[9], pointsR[10]);
    linesR[10] = create_line(pointsR[10], pointsR[11]);
    linesR[11] = create_line(pointsR[11], pointsR[12]);
    linesR[12] = create_line(pointsR[12], pointsR[13]);
    linesR[13] = create_line(pointsR[13], pointsR[0]);
    linesR[14] = create_line(pointsR[14], pointsR[15]);
    linesR[15] = create_line(pointsR[15], pointsR[16]);
    linesR[16] = create_line(pointsR[16], pointsR[17]);
    linesR[17] = create_line(pointsR[17], pointsR[14]);
    
    letter letterR = create_letter('R', linesR, sizeof(linesR)/sizeof(linesR[0]));
    /***************************************/

    char *buffer;
    size_t bufsize = 32;
    size_t length;

    buffer = (char *)malloc(bufsize * sizeof(char));
    if (buffer == NULL) {
        perror("Unable to allocate buffer");
        exit(1);
    }

    printf("Type something: ");
    length = getline(&buffer, &bufsize, stdin);

    frame_buffer fb;

    //inisialisasi frame buffer
    if (initialFrameBuffer(&fb) == 1)
        return 1;

    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = X_START;
    pointer.y = Y_START;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    initialScreen(fb, SCREEN_COLOR);

    // test print huruf A dari pointer;
    // draw_letter(fb, letterA, pointer, FONT_COLOR);
    // pointer.x += letterA.width + FONT_SPACE*FONT_SCALE;
    // draw_letter(fb, letterA, pointer, FONT_COLOR);

    int i = 0;

  
    for (i = 0; i < length-1; ++i) {
        switch (buffer[i]) {
            case 'A':
                draw_letter(fb, letterA, pointer, FONT_COLOR);
                pointer.x += letterA.width + FONT_SPACE*FONT_SCALE;
                break;
            case 'B' :
                draw_letter(fb, letterB, pointer, FONT_COLOR);
                pointer.x += letterA.width + FONT_SPACE*FONT_SCALE;
                break;
            case 'C' :
                draw_letter(fb, letterC, pointer, FONT_COLOR);
                pointer.x += letterC.width + FONT_SPACE*FONT_SCALE;
                break;
            case 'D' :
                draw_letter(fb, letterD, pointer, FONT_COLOR);
                pointer.x += letterD.width + FONT_SPACE*FONT_SCALE;
                break;
            case 'E' :
                draw_letter(fb, letterE, pointer, FONT_COLOR);
                pointer.x += letterE.width + FONT_SPACE*FONT_SCALE;
                break;
            case 'F' :
                draw_letter(fb, letterF, pointer, FONT_COLOR);
                pointer.x += letterF.width + FONT_SPACE*FONT_SCALE;
                break;
            case 'G' :
                draw_letter(fb, letterG, pointer, FONT_COLOR);
                pointer.x += letterG.width + FONT_SPACE*FONT_SCALE;
                break;
            case 'I' :
                draw_letter(fb, letterI, pointer, FONT_COLOR);
                pointer.x += letterI.width + FONT_SPACE*FONT_SCALE;
                break;
	        case 'J' :
                draw_letter(fb, letterJ, pointer, FONT_COLOR);
                pointer.x += letterJ.width + FONT_SPACE*FONT_SCALE;
                break;
            case 'K' :
                draw_letter(fb, letterK, pointer, FONT_COLOR);
                pointer.x += letterK.width + FONT_SPACE*FONT_SCALE;
                break;
            case 'M' :
                draw_letter(fb, letterM, pointer, FONT_COLOR);
                pointer.x += letterM.width + FONT_SPACE*FONT_SCALE;
                break;
            case 'R' :
                draw_letter(fb, letterR, pointer, FONT_COLOR);
                pointer.x += letterR.width + FONT_SPACE*FONT_SCALE;
                break;
            default:
                draw_letter(fb, letterA, pointer, FONT_COLOR);
                pointer.x += letterA.width + FONT_SPACE*FONT_SCALE;
        }
    }

    return 0;
}