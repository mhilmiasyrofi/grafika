//
// Created by hilmi on 12/02/18.
//

#include "fbuffer.h"
#include "point.h"
#include "line.h"

#define SCREEN_COLOR 0x000000
#define LINE_COLOR 0x999999

void my_handler(int s) {
    printf("Caught signal %d", s);
    exit(1);
}

int main() {

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    frame_buffer fb;

    //inisialisasi frame buffer
    if (initializeFrameBuffer(&fb) == 1)
        return 1;

    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = X_START + 500;
    pointer.y = Y_START + 500;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    initializeScreen(fb, SCREEN_COLOR);

    point start, end;
    start = create_point(500, 500);
    end = create_point(300, 750);

    line l;
    l = create_line(start, end);

    draw_line(fb, l, 1, LINE_COLOR);

    printf("Gradient: %f\n", l.gradient);
    printf("Delta X: %d\n", l.deltaX);
    printf("Delta Y: %d\n", l.deltaY);

    // line l_new = rotate_line(l, l.start, 1);
    // draw_line(fb, l_new, 1, LINE_COLOR);
    
    // for (int i = 0; i < 90; i++) {
    //     delay(10);
    //     initializeScreen(fb, SCREEN_COLOR);
    //     line l_rotate = rotate_line(l, l.start, i);
    //     line l_translate = translate_line(l_rotate, i, i);
    //     draw_line(fb, l_translate, 1, LINE_COLOR);
    // }


    return 0;
}