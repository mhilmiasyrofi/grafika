//
// Created by iqbal on 19/02/18.
//

#include <stdlib.h>
#include "line.h"
#include "plane.h"

#ifndef GRAFIKA_SHAPE_H
#define GRAFIKA_SHAPE_H

#define MAX_NODES 100

typedef struct {
    point points[MAX_NODES];
    int count;
    int width; // jarak dari titik paling kiri ke paling kanan
    int height; // jarak dari titik paling atas ke paling bawah
} polygon;

void make_polygon(polygon * p, point * nodes, int count);

void print_polygon(polygon p);

void draw_empty_polygon(frame_buffer fb, point origin, polygon p, int scale, int border_color);

void draw_empty_rectangle(frame_buffer fb, point origin, int width, int height, int scale, int border_color);

void draw_filled_rectangle(frame_buffer fb, point origin, int width, int height, int fill_color);

void draw_empty_circle(frame_buffer fb, point center, int radius, int border_color);

void draw_filled_circle(frame_buffer fb, point center, int radius, int fill_color);

void draw_filled_polygon(frame_buffer fb, point origin, polygon p, int scale, int fill_color);

#endif //GRAFIKA_SHAPE_H
