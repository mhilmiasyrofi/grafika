//
// Created by iqbal on 19/02/18.
//

#include "fbuffer.h"
#include "global.h"
#include "shape.h"
#include "point.h"
#include "line.h"
#include "plane.h"
#include "clipper.h"

#define SCREEN_COLOR 0x000000
#define LINE_COLOR 0x999999
#define WINDOW_COLOR 0x0000ff

void my_handler(int s) {
    printf("Caught signal %d", s);
    exit(1);
}

// rectangle create_rectangle_c(int width, int height, int x, int y) {
//     point top_left = create_point(x, y);
//     point top_right = create_point(x+width, y);
//     point bottom_left = create_point(x, y+height);
//     point bottom_right = create_point(x+width, y+height);

//     line lines[4];
//     lines[0] = create_line(top_left, top_right);
//     lines[1] = create_line(top_right, bottom_right);
//     lines[2] = create_line(bottom_right, bottom_left);
//     lines[3] = create_line(bottom_left, top_left);

//     return create_plane(lines, 4);
// }

int main() {

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    frame_buffer fb;

    //inisialisasi frame buffer
    if (initializeFrameBuffer(&fb) == 1)
        return 1;

    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = X_START;
    pointer.y = Y_START;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    initializeScreen(fb, SCREEN_COLOR);

    /* Viewport */
        viewport v;
        int vpX = fb.width/4;
        int vpY = fb.height/4;
        int height = fb.height/2;
        int width = fb.width/2;

        
        // plane viewport = create_plane(vpLines, sizeof(vpLines)/sizeof(vpLines[0]));
        // rectangle viewport = create_rectangle_c(width, height, vpX, vpY);

    /* ITB Building */
    /* building 1 */
        point building1Points[4];
        building1Points[0] = create_point(200, 50);
        building1Points[1] = create_point(600, 50);
        building1Points[2] = create_point(600, 300);
        building1Points[3] = create_point(200, 300);

        line building1Lines[4];
        building1Lines[0] = create_line(building1Points[0], building1Points[1]);
        building1Lines[1] = create_line(building1Points[1], building1Points[2]);
        building1Lines[2] = create_line(building1Points[2], building1Points[3]);
        building1Lines[3] = create_line(building1Points[3], building1Points[0]);

        plane building1 = create_plane(building1Lines, sizeof(building1Lines)/sizeof(building1Lines[0]));
        polygon pbuilding1;
        make_polygon(&pbuilding1, building1Points, sizeof(building1Lines)/sizeof(building1Lines[0]));

    /* building 2 */
        point building2Points[4];
        building2Points[0] = create_point(700, 50);
        building2Points[1] = create_point(1100, 50);
        building2Points[2] = create_point(1100, 300);
        building2Points[3] = create_point(700, 300);

        line building2Lines[4];
        building2Lines[0] = create_line(building2Points[0], building2Points[1]);
        building2Lines[1] = create_line(building2Points[1], building2Points[2]);
        building2Lines[2] = create_line(building2Points[2], building2Points[3]);
        building2Lines[3] = create_line(building2Points[3], building2Points[0]);

        plane building2 = create_plane(building2Lines, sizeof(building2Lines)/sizeof(building2Lines[0]));
        polygon pbuilding2;
        make_polygon(&pbuilding2, building2Points, sizeof(building2Lines)/sizeof(building2Lines[0]));

    /* building 3 */
        point building3Points[4];
        building3Points[0] = create_point(200, 400);
        building3Points[1] = create_point(600, 400);
        building3Points[2] = create_point(600, 650);
        building3Points[3] = create_point(200, 650);

        line building3Lines[4];
        building3Lines[0] = create_line(building3Points[0], building3Points[1]);
        building3Lines[1] = create_line(building3Points[1], building3Points[2]);
        building3Lines[2] = create_line(building3Points[2], building3Points[3]);
        building3Lines[3] = create_line(building3Points[3], building3Points[0]);

        plane building3 = create_plane(building3Lines, sizeof(building3Lines)/sizeof(building3Lines[0]));
        polygon pbuilding3;
        make_polygon(&pbuilding3, building3Points, sizeof(building3Lines)/sizeof(building3Lines[0]));

    /* building 4 */
        point building4Points[4];
        building4Points[0] = create_point(700, 400);
        building4Points[1] = create_point(1100, 400);
        building4Points[2] = create_point(1100, 650);
        building4Points[3] = create_point(700, 650);

        line building4Lines[4];
        building4Lines[0] = create_line(building4Points[0], building4Points[1]);
        building4Lines[1] = create_line(building4Points[1], building4Points[2]);
        building4Lines[2] = create_line(building4Points[2], building4Points[3]);
        building4Lines[3] = create_line(building4Points[3], building4Points[0]);

        plane building4 = create_plane(building4Lines, sizeof(building4Lines)/sizeof(building4Lines[0]));
        polygon pbuilding4;
        make_polygon(&pbuilding4, building4Points, sizeof(building4Lines)/sizeof(building4Lines[0]));

    /* Circle */
        point center = create_point(650, 350);
        int radius = 25;

    /* Street line 1*/
        point street1Points[4];
        street1Points[0] = create_point(610, 50);
        street1Points[1] = create_point(620, 50);
        street1Points[2] = create_point(620, 660);
        street1Points[3] = create_point(610, 660);

        line street1Lines[4];
        street1Lines[0] = create_line(street1Points[0], street1Points[1]);
        street1Lines[1] = create_line(street1Points[1], street1Points[2]);
        street1Lines[2] = create_line(street1Points[2], street1Points[3]);
        street1Lines[3] = create_line(street1Points[3], street1Points[0]);

        plane street1 = create_plane(street1Lines, sizeof(street1Lines)/sizeof(street1Lines[0]));
        polygon pstreet1;
        make_polygon(&pstreet1, street1Points, sizeof(street1Lines)/sizeof(street1Lines[0]));

    /* Street line 2*/
        point street2Points[4];
        street2Points[0] = create_point(680, 50);
        street2Points[1] = create_point(690, 50);
        street2Points[2] = create_point(690, 660);
        street2Points[3] = create_point(680, 660);

        line street2Lines[4];
        street2Lines[0] = create_line(street2Points[0], street2Points[1]);
        street2Lines[1] = create_line(street2Points[1], street2Points[2]);
        street2Lines[2] = create_line(street2Points[2], street2Points[3]);
        street2Lines[3] = create_line(street2Points[3], street2Points[0]);

        plane street2 = create_plane(street2Lines, sizeof(street2Lines)/sizeof(street2Lines[0]));
        polygon pstreet2;
        make_polygon(&pstreet2, street2Points, sizeof(street2Lines)/sizeof(street2Lines[0]));

    /* Street line 3*/
        point street3Points[4];
        street3Points[0] = create_point(200, 660);
        street3Points[1] = create_point(1100, 660);
        street3Points[2] = create_point(1100, 710);
        street3Points[3] = create_point(200, 710);

        line street3Lines[4];
        street3Lines[0] = create_line(street3Points[0], street3Points[1]);
        street3Lines[1] = create_line(street3Points[1], street3Points[2]);
        street3Lines[2] = create_line(street3Points[2], street3Points[3]);
        street3Lines[3] = create_line(street3Points[3], street3Points[0]);

        plane street3 = create_plane(street3Lines, sizeof(street3Lines)/sizeof(street3Lines[0]));
        polygon pstreet3;
        make_polygon(&pstreet3, street3Points, sizeof(street3Lines)/sizeof(street3Lines[0]));


    // printPlane(viewport);
    // printPlane(building1);
    
    // LOOP TEMPAT NGERENDER GAMBAR
    char input = 0;
    for (int i = 0; i < 360; i++) {
        delay(50);
        initializeScreen(fb, SCREEN_COLOR);
        point vpPoints[4];
        polygon tempbuilding1 = pbuilding1;
        polygon tempbuilding2 = pbuilding2;
        polygon tempbuilding3 = pbuilding3;
        polygon tempbuilding4 = pbuilding4;
        polygon tempstreet1 = pstreet1;
        polygon tempstreet2 = pstreet2;
        polygon tempstreet3 = pstreet3;

        if(input=='w') {
            vpY = vpY - 10;
        } else if (input == 's') {
            vpY = vpY +10;
        } else if (input == 'a') {
            vpX =vpX -10;
        } else if (input == 'd') {
            vpX = vpX + 10;
        } else {
        }
        vpPoints[0] = create_point(vpX, vpY);
        vpPoints[1] = create_point(vpX, vpY+height);
        vpPoints[2] = create_point(vpX+width, vpY+height);
        vpPoints[3] = create_point(vpX+width, vpY);

        make_viewport(&v,vpPoints);

        // draw_plane(fb, viewport, pointer, LINE_COLOR);
        // make_viewport(&v, vpPoints);
        draw_empty_polygon(fb, pointer, v, 1, LINE_COLOR);

        clip_polygon_against_polygon(&tempbuilding1, v);
        clip_polygon_against_polygon(&tempbuilding2, v);
        clip_polygon_against_polygon(&tempbuilding3, v);
        clip_polygon_against_polygon(&tempbuilding4, v);
        clip_polygon_against_polygon(&tempstreet1, v);
        clip_polygon_against_polygon(&tempstreet2, v);
        clip_polygon_against_polygon(&tempstreet3, v);

        // clip_polygon_against_polygon(&building1, v);
        draw_filled_polygon(fb, pointer, tempbuilding1,1, 0xff0000);
        draw_filled_polygon(fb, pointer, tempbuilding2,1, 0x666fff);
        draw_filled_polygon(fb, pointer, tempbuilding3,1, 0x66ff66);
        draw_filled_polygon(fb, pointer, tempbuilding4,1, 0xcc66ff);
        // draw_plane(fb, building1, pointer, LINE_COLOR);
        // draw_plane(fb, building2, pointer, LINE_COLOR);
        // draw_plane(fb, building3, pointer, LINE_COLOR);
        // draw_plane(fb, building4, pointer, LINE_COLOR);

        draw_filled_circle(fb, center, radius, 0xffcccc);

        draw_filled_polygon(fb, pointer, tempstreet1,1, LINE_COLOR);
        draw_filled_polygon(fb, pointer, tempstreet2,1, LINE_COLOR);
        draw_filled_polygon(fb, pointer, tempstreet3,1, LINE_COLOR);

        tempbuilding1 = pbuilding1;
        tempbuilding2 = pbuilding2;
        tempbuilding3 = pbuilding3;
        tempbuilding4 = pbuilding4;
        tempstreet1 = pstreet1;
        tempstreet2 = pstreet2;
        tempstreet3 = pstreet3;
        printf("Masukkan input :\n");
        printf("w -> naik ke atas\n");
        printf("s -> turun ke bawah\n");
        printf("d -> geser ke kanan\n");
        printf("a -> geser ke kiri\n");
        input = getchar();
        // draw_plane(fb, street1, pointer, LINE_COLOR);
        // draw_plane(fb, street2, pointer, LINE_COLOR);
        // draw_plane(fb, street3, pointer, LINE_COLOR);
    }

    return 0;
}
