//
// Created by iqbal on 05/02/18.
//

#ifndef GRAFIKA_POINT_H
#define GRAFIKA_POINT_H

typedef struct {
    int x;
    int y;
} point;

point create_point(int x, int y);

point translate_point(point p, int dx, int dy);

point rotate_point(point p, point pivot, float angle);

point dilate_point(point p, point center, float sx, float sy);

point scale_point(point p, float sx, float sy);

void print_point(point p);

#endif //GRAFIKA_POINT_H
