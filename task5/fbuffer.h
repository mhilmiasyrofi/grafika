//
// Created by iqbal on 05/02/18.
//
#include <linux/fb.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <signal.h>
#include <time.h>
#include "point.h"

#ifndef GRAFIKA_FBUFFER_H
#define GRAFIKA_FBUFFER_H

// tipe data untuk merepresentasikan suatu frame buffer
typedef struct {
    char* pointer;
    int width;
    int height;
} frame_buffer;

int initializeFrameBuffer(frame_buffer *fb);

int setPixel(frame_buffer f, int x, int y, int color);

int getPixel(frame_buffer f, int x, int y);

void initializeScreen(frame_buffer fb, int color);

#endif //GRAFIKA_FBUFFER_H
