#include "global.h"
#include "shape.h"
#include "fbuffer.h"
#include "clipper.h"

#define COLOR_RED 0xff0000
#define COLOR_BLUE 0x0000ff
#define COLOR_GREEN 0x00ff00

void my_handler(int s) {
    printf("Caught signal %d", s);
    exit(1);
}

int main() {

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    frame_buffer fb;

    //inisialisasi frame buffer
    if (initializeFrameBuffer(&fb) == 1)
        return 1;

    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = X_START;
    pointer.y = Y_START;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    initializeScreen(fb, SCREEN_COLOR);

    /***************************************/

    // point center;
    // center.x = fb.width/2;
    // center.y = fb.height/2;

    // draw_filled_circle(fb, center, 100, 0xFFFFFF);

    // Viewport
    int fbq_w = fb.width/4;
    int fbq_h = fb.height/4;
    point points[4];
    points[0] = create_point(fbq_w,fbq_h);
    points[1] = create_point(fbq_w,3*fbq_h);
    points[2] = create_point(3*fbq_w,3*fbq_h);
    points[3] = create_point(3*fbq_w,fbq_h);
    
    point points2[3]; 
    points2[0] = create_point(400,400);
    points2[1] = create_point(200,200);
    points2[2] = create_point(fbq_w*2,fbq_h*2);
    
    polygon * p = malloc(sizeof(polygon));
    polygon * r = malloc(sizeof(polygon));
    make_polygon(p, points, 4);
    make_polygon(r, points2, 3);
    print_polygon(*p);
    print_polygon(*r);

    clip_polygon_against_polygon(r, *p);
    print_polygon(*p);
    print_polygon(*r);

    point origin = create_point(X_START, Y_START);
    draw_empty_polygon(fb, origin, *p, 2, COLOR_BLUE);
    draw_filled_polygon(fb, origin, *r, 2, COLOR_RED);

    return 0;
}