//
// Created by iqbal on 20/02/18.
//

#include "shape.h"
#include "global.h"

#ifndef GRAFIKA_CLIPPER_H
#define GRAFIKA_CLIPPER_H

#define INSIDE 0   // 0000
#define LEFT 1     // 0001
#define RIGHT 2    // 0010
#define BOTTOM 4   // 0100
#define TOP 8      // 1000

typedef int out_code;

typedef polygon viewport;

void make_viewport(viewport * v, point p[4]);

boolean clip_point_against_viewport(point p, viewport r);

line clip_line_against_viewport(line l, viewport r);

void clip_polygon_against_polygon(polygon * p, polygon r);
#endif //GRAFIKA_CLIPPER_H
