//
// Created by iqbal on 19/02/18.
//
#include "shape.h"
#include "point.h"

void make_polygon(polygon * p, point * nodes, int count) {

    int minx, maxx, miny, maxy, x, y;
    minx = nodes[0].x;
    maxx = nodes[0].x;
    miny = nodes[0].y;
    maxy = nodes[0].y;
    for (int i = 0; i < count; i++) {
        x = nodes[i].x;
        y = nodes[i].y;
        minx = (x < minx) ? x : minx;
        maxx = (x > maxx) ? x : maxx;
        miny = (y < miny) ? y : miny;
        maxy = (y > maxy) ? y : maxy;
        (*p).points[i].x = x;
        (*p).points[i].y = y;
    }
    (*p).width = maxx - minx;
    (*p).height = maxy - miny;
    (*p).count = count;
}

void print_polygon(polygon p) {
    for (int i = 0; i < p.count; i++) {
        print_point(p.points[i]);
        printf("\n");
    }
    printf("width: %d ", p.width);
    printf("height: %d\n", p.height);
}

void draw_empty_polygon(frame_buffer fb, point origin, polygon p, int scale, int border_color) {
    for (int i = 0; i < p.count; i++) {
        int x1, y1, x2, y2;
        point start, end;
        line l;
        if (i < p.count - 1) {
            x1 = p.points[i].x + origin.x;
            y1 = p.points[i].y + origin.y;

            x2 = p.points[i+1].x + origin.x;
            y2 = p.points[i+1].y + origin.y;
        } else {
            x1 = p.points[i].x + origin.x;
            y1 = p.points[i].y + origin.y;

            x2 = p.points[0].x + origin.x;
            y2 = p.points[0].y + origin.y;
        }
        start = create_point(x1, y1);
        end = create_point(x2, y2);
        l = create_line(start, end);
        draw_line(fb, l, scale, border_color);
    }
}

void draw_empty_rectangle(frame_buffer fb, point origin, int width, int height, int scale, int border_color) {
    point top_left = create_point(origin.x, origin.y);
    point top_right = create_point(origin.x+width, origin.y);
    point bottom_left = create_point(origin.x,origin.y+height);
    point bottom_right = create_point(origin.x+width, origin.y+height);

    line lines[4];
    lines[0] = create_line(top_left, top_right);
    lines[1] = create_line(top_right, bottom_right);
    lines[2] = create_line(bottom_right, bottom_left);
    lines[3] = create_line(bottom_left, top_left);

    for (int i = 0; i < 4; ++i) {
        draw_line(fb, lines[i], scale, border_color);
    }
}

void draw_filled_rectangle(frame_buffer fb, point origin, int width, int height, int fill_color) {
    point top_left = create_point(origin.x, origin.y);
    point top_right = create_point(origin.x+width, origin.y);
    point bottom_left = create_point(origin.x,origin.y+height);
    point bottom_right = create_point(origin.x+width, origin.y+height);

    line l = create_line(top_left, top_right);
    for (int i = origin.x; i < width; i++) {
        l.start.y++;
        l.end.y++;
        draw_line(fb, l, 1, fill_color);
    }
}

void draw_empty_circle(frame_buffer fb, point center, int radius, int border_color) {
    int x = radius - 1;
    int y = 0;
    int dx = 1;
    int dy = 1;
    int err = dx - (radius << 1);

    while (x >= y) {
        setPixel(fb, (center.x + x), (center.y + y), border_color);
        setPixel(fb, (center.x + y), (center.y + x), border_color);
        setPixel(fb, (center.x + x), (center.y - y), border_color);
        setPixel(fb, (center.x + y), (center.y - x), border_color);
        setPixel(fb, (center.x - x), (center.y + y), border_color);
        setPixel(fb, (center.x - y), (center.y + x), border_color);
        setPixel(fb, (center.x - x), (center.y - y), border_color);
        setPixel(fb, (center.x - y), (center.y - x), border_color);

        if (err <= 0) {
            y++;
            err += dy;
            dy += 2;
        }

        if (err > 0) {
            x--;
            dx += 2;
            err += dx - (radius << 1);
        }
    }
}

void draw_filled_circle(frame_buffer fb, point center, int radius, int fill_color) {
    int x = radius - 1;
    int y = 0;
    int dx = 1;
    int dy = 1;
    int err = dx - (radius << 1);

    while (x >= y) {
        setPixel(fb, (center.x + x), (center.y + y), fill_color);
        setPixel(fb, (center.x + y), (center.y + x), fill_color);
        setPixel(fb, (center.x + x), (center.y - y), fill_color);
        setPixel(fb, (center.x + y), (center.y - x), fill_color);
        setPixel(fb, (center.x - x), (center.y + y), fill_color);
        setPixel(fb, (center.x - y), (center.y + x), fill_color);
        setPixel(fb, (center.x - x), (center.y - y), fill_color);
        setPixel(fb, (center.x - y), (center.y - x), fill_color);

        if (err <= 0) {
            y++;
            err += dy;
            dy += 2;
        }

        if (err > 0) {
            x--;
            dx += 2;
            err += dx - (radius << 1);
        }
    }

    for(int j=-radius; j<=radius; j++) {
        for(int i=-radius; i<=radius; i++) {
            if(i*i+j*j <= radius*radius) {
                setPixel(fb, (center.x + i), (center.y + j), fill_color);
            }
        }
    }
}

void draw_filled_polygon(frame_buffer fb, point origin, polygon p, int scale, int fill_color) {
    float gradient[p.count];
    int maxy = 0, y, temp;
    int k;
    int xi[p.count];
    int dx, dy;
    point a[p.count + 1];

    for (int i = 0; i < p.count; i++) {
        a[i].x = p.points[i].x;
        a[i].y = p.points[i].y;
    }
    a[p.count].x = a[0].x;
    a[p.count].y = a[0].y;

    for (int i = 0; i < p.count; i++) {
        int x1, y1, x2, y2;
        point start, end;
        line l;
        if (i < p.count - 1) {
            x1 = p.points[i].x + origin.x;
            y1 = p.points[i].y + origin.y;

            x2 = p.points[i+1].x + origin.x;
            y2 = p.points[i+1].y + origin.y;
        } else {
            x1 = p.points[i].x + origin.x;
            y1 = p.points[i].y + origin.y;

            x2 = p.points[0].x + origin.x;
            y2 = p.points[0].y + origin.y;
        }
        start = create_point(x1, y1);
        end = create_point(x2, y2);
        l = create_line(start, end);
        draw_line(fb, l, scale, fill_color);
    }

    for (int i = 0; i < p.count; i++) {
        dx = a[i+1].x - a[i].x;
        dy = a[i+1].y - a[i].y;

        if (dy == 0) {
            gradient[i] = 1.0;
        }

        if (dx == 0) {
            gradient[i] = 0.0;
        }

        if ((dy != 0) && (dx != 0)) {
            gradient[i] = (float) dx /dy;
        }
    }

    for (int i = 0; i < p.count; i++) {
        y = p.points[i].y;
        maxy = (y > maxy) ? y : maxy;
    }

    for (y = 0; y <= maxy; y++) {
        k = 0;
        for (int i = 0; i < p.count; i++) {
            if (((a[i].y <= y) && (a[i+1].y > y)) || ((a[i].y > y) && a[i+1].y <= y)) {
                xi[k] = (int)(a[i].x + gradient[i] * (y - a[i].y));
                k++;
            }
        }

        for (int j = 0; j < k - 1; j++) {
            for (int i = 0; i < k -1; i++) {
                if (xi[i] > xi[i+1]) {
                    temp = xi[i];
                    xi[i] = xi[i+1];
                    xi[i+1] = temp;
                }
            }
            for (int i = 0; i < k; i+=2) {
                point start, end;
                line l;

                start = create_point(xi[i] + origin.x, y + origin.y);
                end = create_point(xi[i+1] + 1 + origin.x, y + origin.y);
                l = create_line(start, end);
                draw_line(fb, l, scale, fill_color);
            }
        }
    }

}

