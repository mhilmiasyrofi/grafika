#include "global.h"
#include "plane.h"
#include "fbuffer.h"

void my_handler(int s) {
    printf("Caught signal %d", s);
    exit(1);
}

int main() {

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    frame_buffer fb;

    //inisialisasi frame buffer
    if (initializeFrameBuffer(&fb) == 1)
        return 1;

    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = X_START;
    pointer.y = Y_START;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    initializeScreen(fb, SCREEN_COLOR);

    /*********** Hardcode huruf A **********/
    point points[11];
    points[0] = create_point(0, 10);
    points[1] = create_point(2, 10);
    points[2] = create_point(3, 7);
    points[3] = create_point(7, 7);
    points[4] = create_point(8, 10);
    points[5] = create_point(10, 10);
    points[6] = create_point(6, 0);
    points[7] = create_point(4, 0);
    points[8] = create_point(4, 5);
    points[9] = create_point(5, 2);
    points[10] = create_point(6, 5);

    line lines[11];
    lines[0] = create_line(points[0], points[1]);
    lines[1] = create_line(points[1], points[2]);
    lines[2] = create_line(points[2], points[3]);
    lines[3] = create_line(points[3], points[4]);
    lines[4] = create_line(points[4], points[5]);
    lines[5] = create_line(points[5], points[6]);
    lines[6] = create_line(points[6], points[7]);
    lines[7] = create_line(points[7], points[0]);
    lines[8] = create_line(points[8], points[9]);
    lines[9] = create_line(points[9], points[10]);
    lines[10] = create_line(points[10], points[8]);

    plane letterA = create_plane(lines, sizeof(lines) / sizeof(lines[0]));
    /***************************************/


    draw_plane(fb, letterA, pointer, FONT_COLOR);
    // printf("SEBELUm\n");
    // printPlane(letterA);

    // draw_plane(fb, letterA, pointer, SCREEN_COLOR);
    // plane new_A = rotate_plane(letterA, letterA.lines[0].start, 10);
    // draw_plane(fb, new_A, pointer, FONT_COLOR);
    for (int i = 0; i < 90/4; i++) {
        delay(50);
        // draw_plane(fb, letterA, pointer, SCREEN_COLOR);
        initializeScreen(fb, SCREEN_COLOR);
        plane rotate_A = rotate_plane(letterA, letterA.lines[0].start, 4);
        plane translate_A = translate_plane(rotate_A, 3, 3);
        draw_plane(fb, translate_A, pointer, FONT_COLOR);
        letterA = translate_A;
    }

    // printf("LUAR\n");
    // printPlane(new_A);

    return 0;
}