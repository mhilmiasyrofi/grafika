//
// Created by iqbal on 20/02/18.
//

#include "clipper.h"
#include "plane.h"
#include "line.h"
#include "point.h"

void make_viewport(viewport * v, point p[4]) {
    make_polygon(v, p, 4);
}

out_code get_point_out_code(point p, viewport view_port) {
    out_code code = INSIDE;

    int xmin = view_port.points[0].x;
    int ymin = view_port.points[0].y;
    int xmax = view_port.points[1].x;
    int ymax = view_port.points[1].y;

    if (p.x < xmin) {
        code |= LEFT;
    } else if (p.x > xmax) {
        code |= RIGHT;
    }

    if (p.y < ymin) {
        code |= BOTTOM;
    } else if (p.y > ymax) {
        code |= TOP;
    }

    return code;
}

int x_intersect (line line1, line line2) {
    int x1 = line1.start.x;
    int y1 = line1.start.y;
    int x2 = line1.end.x;
    int y2 = line1.end.y;
    int x3 = line2.start.x;
    int y3 = line2.start.y;
    int x4 = line2.end.x;
    int y4 = line2.end.y;

    int numerator = (x1*y2 - y1*x2) * (x3-x4) - (x1-x2) * (x3*y4 - y3*x4);
    int denominator = (x1-x2) * (y3-y4) - (y1-y2) * (x3-x4);

    return numerator/denominator;
}

int y_intersect (line line1, line line2) {
    int x1 = line1.start.x;
    int y1 = line1.start.y;
    int x2 = line1.end.x;
    int y2 = line1.end.y;
    int x3 = line2.start.x;
    int y3 = line2.start.y;
    int x4 = line2.end.x;
    int y4 = line2.end.y;

    int numerator = (x1*y2 - y1*x2) * (y3-y4) - (y1-y2) * (x3*y4 - y3*x4);
    int denominator = (x1-x2) * (y3-y4) - (y1-y2) * (x3-x4);

    return numerator/denominator;
}

boolean clip_point_against_viewport(point p, viewport r) {
    return get_point_out_code(p, r) != 0;
}

line clip_line_against_viewport(line l, viewport r) {

}

void clip_polygon_against_line(polygon * p, line l) {
    int count = (*p).count;
    int new_count = 0;
    point new_nodes[MAX_NODES];

    int x1, x2, y1, y2;
    x1 = l.start.x;
    y1 = l.start.y;
    x2 = l.end.x;
    y2 = l.end.y;
    for (int i = 0; i < count; i++) {

        // i dan k membentuk garis pada polygon p
        int k = (i+1) % count;
        int ix = (*p).points[i].x, iy = (*p).points[i].y;
        int kx = (*p).points[k].x, ky = (*p).points[k].y;

        point start = create_point(ix, iy);
        point end = create_point(kx, ky);
        line p_line = create_line(start, end);
        
        // posisi titik i dan k terhadap poligon
        int i_pos = (x2-x1) * (iy-y1) - (y2-y1) * (ix-x1);
        int k_pos = (x2-x1) * (ky-y1) - (y2-y1) * (kx-x1);

        // Kasus ketika kedua poin berada di dalam
        if (i_pos < 0  && k_pos < 0)
        {
            new_nodes[new_count].x = kx;
            new_nodes[new_count].y = ky;
            new_count++;
        }
 
        // Kasus ketika titik i berada di luar
        else if (i_pos >= 0  && k_pos < 0)
        {
            new_nodes[new_count].x = x_intersect(l, p_line);
            new_nodes[new_count].y = y_intersect(l, p_line);
            new_count++;
 
            new_nodes[new_count].x = kx;
            new_nodes[new_count].y = ky;
            new_count++;
        }
 
        // Kasus ketika titik k berada di luar
        else if (i_pos < 0  && k_pos >= 0)
        {
            new_nodes[new_count].x = x_intersect(l, p_line);
            new_nodes[new_count].y = y_intersect(l, p_line);
            new_count++;
        }
 
        // Kasus ketika kedua titik berada di luar
        else
        {
            // tidak melakukan apa-apa
        }
    }
 
    make_polygon(p, new_nodes, new_count);
}

void clip_polygon_against_polygon(polygon * p, polygon r) {
    for (int i = 0; i < r.count; i++) {
        int k = (i+1) % r.count;
        line r_line = create_line(r.points[i], r.points[k]);
        clip_polygon_against_line(p, r_line);
    }
}


