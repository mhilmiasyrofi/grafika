#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef GLOBAL_H
#define GLOBAL_H

#define FONT_COLOR 0x999990
#define FONT_SCALE 1
#define FONT_SPACE 2
#define SCREEN_COLOR 0x000000
#define X_START 0
#define Y_START 0

typedef int boolean;
#define true 1
#define false 0

void delay(int number_of_seconds);

#endif //GLOBAL_H