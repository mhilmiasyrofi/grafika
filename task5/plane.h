#include "line.h"
#include "fbuffer.h"

#ifndef PLANE_LINE_H
#define PLANE_LINE_H

typedef struct {
    int count;
    line *lines;
    int min_width;
    int min_height;
    int width;
    int height;
} plane;

plane create_plane(line *lines, int count);

void draw_plane(frame_buffer fb, plane p, point origin, int color);

plane rotate_plane(plane p, point pivot, float angle);

plane scale_plane(plane p, float sx, float sy);

plane translate_plane(plane p, int dx, int dy);

plane resize_plane(plane p);

void printPlane(plane p);

#endif //PLANE_LINE_H
