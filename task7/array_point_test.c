//
// Created by iqbal on 09/03/18.
//

#include "array_point.h"

int main() {
    arr_point arr;
    initArray(&arr, 1);

    for (int i = 0; i < 10; i++) {
        point p = create_point(i, i);
        insertArray(&arr, p);
    }

    printArray(arr);
    return 0;
}