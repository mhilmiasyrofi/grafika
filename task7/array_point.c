//
// Created by iqbal on 09/03/18.
//

#include "array_point.h"

void initArray(arr_point *a, size_t initialSize) {
  a->array = (point *)malloc(initialSize * sizeof(point));
  a->used = 0;
  a->size = initialSize;
}

void insertArray(arr_point *a, point element) {
  if (a->used == a->size) {
    a->size *= 2;
    a->array = (point *)realloc(a->array, a->size * sizeof(point));
  }
  a->array[a->used++] = element;
}

void printArray(arr_point a) {
    for (int i = 0; i < a.used; i++) {
        print_point(a.array[i]);
    }
}

void freeArray(arr_point *a) {
  free(a->array);
  a->array = NULL;
  a->used = a->size = 0;
}