//
// Created by iqbal on 09/03/18.
//

#include <stdlib.h>
#include "point.h"

#ifndef GRAFIKA_ARRAY_POINT_H
#define GRAFIKA_ARRAY_POINT_H

typedef struct {
  point *array;
  size_t used;
  size_t size;
} arr_point;

void initArray(arr_point *a, size_t initialSize);

void insertArray(arr_point *a, point element);

void printArray(arr_point a);

void freeArray(arr_point *a);

#endif 