//
// Created by iqbal on 03/03/18.
//

#include "cursor.h"

#define DEV_PATH "/dev/input/mice"

static const size_t        mousedev_seq_len    = 6;
static const unsigned char mousedev_imps_seq[] = { 0xf3, 200, 0xf3, 100, 0xf3, 80 };

static int bytetoint(const unsigned char c)
{
    if (c < 128)
        return c;
    else
        return c - 256;
}

int init_cursor(cursor * c) {
    c->center = create_point(100,100);

    // membuka mouse event listener
    do {
        c->devfd = open(DEV_PATH, O_RDWR | O_NOCTTY);
    } while (c->devfd == -1 && errno == EINTR);
    if (c->devfd == -1) {
        fprintf(stderr, "Cannot open %s: %s.\n", DEV_PATH, strerror(errno));
        return EXIT_FAILURE;
    }

    // mengubah mouse menjadi ImPS/2 protocol.
    if (write(c->devfd, mousedev_imps_seq, mousedev_seq_len) != (ssize_t)mousedev_seq_len) {
        fprintf(stderr, "Cannot switch to ImPS/2 protocol.\n");
        close(c->devfd);
        return EXIT_FAILURE;
    }
    if (read(c->devfd, c->buffer, sizeof c->buffer) != 1 || c->buffer[0] != 0xFA) {
        fprintf(stderr, "Failed to switch to ImPS/2 protocol.\n");
        close(c->devfd);
        return EXIT_FAILURE;
    }

    fprintf(stderr, "Mouse device %s opened successfully.\n", DEV_PATH);
    fprintf(stderr, "Press CTRL+C to exit.\n");
    fflush(stderr);

    c->wasleft = 0;
}

int draw_cursor(frame_buffer fb, cursor * c) {
    int ch_x = c->center.x - CROSSHAIR_SIZE/2;
    int ch_y = c->center.y - CROSSHAIR_SIZE/2;
    int center = CROSSHAIR_SIZE/2;
    for (int i = 0; i < CROSSHAIR_SIZE; i++) {
        setPixel(fb, ch_x++, c->center.y, CURSOR_COLOR);
        setPixel(fb, c->center.x, ch_y++, CURSOR_COLOR);
    }

    ssize_t len;
    int x, y;

    len = read(c->devfd, c->buffer, 4);
    if (len == -1) {
        if (errno == EINTR)
            return 1;
        fprintf(stderr, "%s.\n", strerror(errno));
        return SIGINT;
    } 

    // melakukan parse terhadap event
    c->left = c->buffer[0] & 1;
    x = bytetoint(c->buffer[1]);
    y = bytetoint(c->buffer[2]);

    // event handling
    if (x) {
        c->center.x += x;
        //printf(" x%+d", c->center.x);
    }

    if (y) {
        c->center.y -= y;
        //printf(" y%+d", c->center.y);

    }
}
