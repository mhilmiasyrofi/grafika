//
// Created by iqbal on 05/02/18.
//

#include "line.h"

line create_line(point start, point end) {
    line new_line;
    new_line.start = start;
    new_line.end = end;
    new_line.gradient = (end.x - start.x) != 0 ? (float)((float)(end.y - start.y) / (float)(end.x - start.x)) : INF;
    new_line.deltaX = end.x - start.x;
    new_line.deltaY = end.y - start.y;
    return new_line;
}

// menggambar garis dengan 0 < gradien < 1
void draw_line_low(frame_buffer fb, point start, point end, int scale, int color) {
    int dx, dy, yi, p, x, y;

    dx = end.x - start.x;
    dy = end.y - start.y;
    if (dy >= 0) {
        yi = 1;
    } else {
        yi = -1;
        dy = -dy;
    }

    p = 2*dy - dx;
    y = start.y;
    for (x = start.x; x < end.x; x++) {
        setPixel(fb, x, y, color);
        // printf("%d %d\n", x, y);
        if (p > 0) {
            y += yi;
            p -= 2*dx;
        }
        p += 2*dy;
    }
}

// menggambar garis dengan gradien > 1
void draw_line_high(frame_buffer fb, point start, point end, int scale, int color) {
    int dx, dy, xi, p, x, y;

    dx = end.x - start.x;
    dy = end.y - start.y;
    if (dx >= 0) {
        xi = 1;
    } else {
        xi = -1;
        dx = -dx;
    }

    p = 2*dx - dy;
    x = start.x;
    for (y = start.y; y < end.y; y++) {
        setPixel(fb, x, y, color);
        // printf("%d %d\n", x, y);
        if (p > 0) {
            x += xi;
            p -= 2*dy;
        }
        p += 2*dx;
    }
}

// menggambar garis dari titik start ke titik end
void draw_line(frame_buffer fb, line a_line, int scale, int color) {
    point start = a_line.start;
    point end = a_line.end;

    if (abs(end.y - start.y) < abs(end.x - start.x)) {
        if (start.x < end.x) {
            draw_line_low(fb, start, end, scale, color);
        } else {
            draw_line_low(fb, end, start, scale, color);
        }
    } else {
        if (start.y < end.y) {
            draw_line_high(fb, start, end, scale, color);
        } else {
            draw_line_high(fb, end, start, scale, color);
        }
    }
}


line rotate_line(line l, point pivot, float angle) {

    point start = l.start;
    point end = l.end;

    start = rotate_point(start, pivot, angle);
    end = rotate_point(end, pivot, angle);

    line dst;

    dst = create_line(start, end);

    return dst;
}

line translate_line(line l, int dx, int dy) {

    point start = l.start;
    point end = l.end;

    start = translate_point(start, dx, dy);
    end = translate_point(end, dx, dy);

    line dst;

    dst = create_line(start, end);

    return dst;
}