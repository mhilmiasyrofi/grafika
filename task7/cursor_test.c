#define  _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include "fbuffer.h"
#include "global.h"
#include "cursor.h"

static volatile sig_atomic_t  done = 0;

static void my_handler(int s) {
    printf("Caught signal %d\n", s);
    done = 1;
}

void on_left_button_press(cursor * c) {
    if (c->left && !c->wasleft) {
        // onpress left button
        printf(" LeftDown");
    } else if (c->left && c->wasleft) {
        // onhold left button
        printf(" Left");
    } else if (!c->left && c->wasleft) {
        // onrelease left button
        printf(" LeftUp");
    }

    printf("\n");
    fflush(stdout);

    c->wasleft = c->left;
}

int main()
{

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    frame_buffer fb;

    //inisialisasi frame buffer
    if (initializeFrameBuffer(&fb) == 1)
        return 1;

    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = 0;
    pointer.y = 0;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    initializeScreen(fb, SCREEN_COLOR);

    // inisialisasi cursor
    cursor crosshair;
    init_cursor(&crosshair);

    while (!done) {
        initializeScreen(fb, SCREEN_COLOR);

        // menggambar cursor
        draw_cursor(fb, &crosshair);
        on_left_button_press(&crosshair);

    }

    /* Done. */
    close(crosshair.devfd);
    printf("________\n");
    return EXIT_SUCCESS;
}