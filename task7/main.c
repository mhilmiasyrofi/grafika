#define  _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include "fbuffer.h"
#include "global.h"
#include "cursor.h"
#include "array_point.h"
#include "shape.h"

#define DOT_SIZE 5
#define YELLOW 0xffff00

static volatile sig_atomic_t  done = 0;

static void my_handler(int s) {
    printf("Caught signal %d\n", s);
    done = 1;
}

void on_left_button_press(cursor * c, arr_point * a) {
    point p = create_point(c->center.x, c->center.y);

    if (c->left && !c->wasleft) {
        // onpress left button
        //printf(" LeftDown");
        insertArray(a, p);
    } else if (c->left && c->wasleft) {
        // onhold left button
        //printf(" Left");
    } else if (!c->left && c->wasleft) {
        // onrelease left button
        //printf(" LeftUp");
    }

    printf("\n");
    fflush(stdout);

    c->wasleft = c->left;
}

void draw_fb_state(frame_buffer f, arr_point a) {
    for (int i = 0; i < a.used; i++) {
        draw_filled_circle(f, a.array[i], DOT_SIZE, YELLOW);
    }
}

int main()
{

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    frame_buffer fb;

    //inisialisasi frame buffer
    if (initializeFrameBuffer(&fb) == 1)
        return 1;

    arr_point fb_state;
    initArray(&fb_state, 1);

    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = 0;
    pointer.y = 0;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    initializeScreen(fb, SCREEN_COLOR);

    // inisialisasi cursor
    cursor crosshair;
    init_cursor(&crosshair);

    while (!done) {

        draw_fb_state(fb, fb_state);

        // menggambar cursor
        draw_cursor(fb, &crosshair);
        on_left_button_press(&crosshair, &fb_state);

        initializeScreen(fb, SCREEN_COLOR);
    }

    /* Done. */
    close(crosshair.devfd);
    printf("________\n");
    return EXIT_SUCCESS;
}