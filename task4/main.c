//
// Created by iqbal on 05/02/18.
//

#include "fbuffer.h"
#include "global.h"
#include "shape.h"
#include "point.h"
#include "line.h"
#include "plane.h"
#include "clipper.h"

#define SCREEN_COLOR 0x000000
#define LINE_COLOR 0x999999
#define WINDOW_COLOR 0x0000ff

void my_handler(int s) {
    printf("Caught signal %d", s);
    exit(1);
}

int main() {

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/

    frame_buffer fb;

    //inisialisasi frame buffer
    if (initializeFrameBuffer(&fb) == 1)
        return 1;

    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = X_START;
    pointer.y = Y_START;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    initializeScreen(fb, SCREEN_COLOR);

    point start, end;
    start = create_point(0, 0);
    end = create_point(300, 500);

    line l;
    l = create_line(start, end);

    // draw_line(fb, l, 1, LINE_COLOR);

// PESAWAT
    point pointsPlane[53];
    // point badan pesawat
    pointsPlane[0] = create_point(16, 9);
    pointsPlane[1] = create_point(17, 8);
    pointsPlane[2] = create_point(17, 7);
    pointsPlane[3] = create_point(18, 6);
    pointsPlane[4] = create_point(22, 6);
    pointsPlane[5] = create_point(23, 7);
    pointsPlane[6] = create_point(23, 8);
    pointsPlane[7] = create_point(24, 9);
    pointsPlane[8] = create_point(24, 13);
    pointsPlane[9] = create_point(23, 14);
    pointsPlane[10] = create_point(23, 15);
    pointsPlane[11] = create_point(22, 16);
    pointsPlane[12] = create_point(18, 16);
    pointsPlane[13] = create_point(17, 15);
    pointsPlane[14] = create_point(17, 14);
    pointsPlane[15] = create_point(16, 13);

    // point sayap kiri pesawat
    pointsPlane[16] = create_point(16, 9);
    pointsPlane[17] = create_point(2, 9);
    pointsPlane[18] = create_point(0, 10);
    pointsPlane[19] = create_point(4, 11);
    pointsPlane[20] = create_point(5, 11);
    pointsPlane[21] = create_point(8, 12);
    pointsPlane[22] = create_point(11, 12);
    pointsPlane[23] = create_point(14, 13);
    pointsPlane[24] = create_point(16, 13);

    // point sayap kanan pesawat
    pointsPlane[25] = create_point(24, 9);
    pointsPlane[26] = create_point(38, 9);
    pointsPlane[27] = create_point(40, 10);
    pointsPlane[28] = create_point(36, 11);
    pointsPlane[29] = create_point(35, 11);
    pointsPlane[30] = create_point(32, 12);
    pointsPlane[31] = create_point(29, 12);
    pointsPlane[32] = create_point(26, 13);
    pointsPlane[33] = create_point(24, 13);

    // point ekor pesawat
    pointsPlane[34] = create_point(19, 6);
    pointsPlane[35] = create_point(19, 4);
    pointsPlane[36] = create_point(20, 0);
    pointsPlane[37] = create_point(21, 4);
    pointsPlane[38] = create_point(21, 6);

    // point aksesoris pesawat
    pointsPlane[39] = create_point(18, 8);
    pointsPlane[40] = create_point(19, 8);
    pointsPlane[41] = create_point(19, 9);
    pointsPlane[42] = create_point(18, 9);
    
    pointsPlane[43] = create_point(21, 8);
    pointsPlane[44] = create_point(22, 8);
    pointsPlane[45] = create_point(22, 9);
    pointsPlane[46] = create_point(21, 9);

    pointsPlane[47] = create_point(20, 10);
    pointsPlane[48] = create_point(21, 11);
    pointsPlane[49] = create_point(21, 12);
    pointsPlane[50] = create_point(20, 13);
    pointsPlane[51] = create_point(19, 12);
    pointsPlane[52] = create_point(19, 11);
    
    line linesPlane[53];
    // line badan pesawat
    linesPlane[0] = create_line(pointsPlane[0], pointsPlane[1]);
    linesPlane[1] = create_line(pointsPlane[1], pointsPlane[2]);
    linesPlane[2] = create_line(pointsPlane[2], pointsPlane[3]);
    linesPlane[3] = create_line(pointsPlane[3], pointsPlane[4]);
    linesPlane[4] = create_line(pointsPlane[4], pointsPlane[5]);
    linesPlane[5] = create_line(pointsPlane[5], pointsPlane[6]);
    linesPlane[6] = create_line(pointsPlane[6], pointsPlane[7]);
    linesPlane[7] = create_line(pointsPlane[7], pointsPlane[8]);
    linesPlane[8] = create_line(pointsPlane[8], pointsPlane[9]);
    linesPlane[9] = create_line(pointsPlane[9], pointsPlane[10]);
    linesPlane[10] = create_line(pointsPlane[10], pointsPlane[11]);
    linesPlane[11] = create_line(pointsPlane[11], pointsPlane[12]);
    linesPlane[12] = create_line(pointsPlane[12], pointsPlane[13]);
    linesPlane[13] = create_line(pointsPlane[13], pointsPlane[14]);
    linesPlane[14] = create_line(pointsPlane[14], pointsPlane[15]);
    linesPlane[15] = create_line(pointsPlane[15], pointsPlane[0]);

    // line sayap kiri
    linesPlane[16] = create_line(pointsPlane[16], pointsPlane[17]);
    linesPlane[17] = create_line(pointsPlane[17], pointsPlane[18]);
    linesPlane[18] = create_line(pointsPlane[18], pointsPlane[19]);
    linesPlane[19] = create_line(pointsPlane[19], pointsPlane[20]);
    linesPlane[20] = create_line(pointsPlane[20], pointsPlane[21]);
    linesPlane[21] = create_line(pointsPlane[21], pointsPlane[22]);
    linesPlane[22] = create_line(pointsPlane[22], pointsPlane[23]);
    linesPlane[23] = create_line(pointsPlane[23], pointsPlane[24]);
    linesPlane[24] = create_line(pointsPlane[24], pointsPlane[16]);

    // line sayap kanan
    linesPlane[25] = create_line(pointsPlane[25], pointsPlane[26]);
    linesPlane[26] = create_line(pointsPlane[26], pointsPlane[27]);
    linesPlane[27] = create_line(pointsPlane[27], pointsPlane[28]);
    linesPlane[28] = create_line(pointsPlane[28], pointsPlane[29]);
    linesPlane[29] = create_line(pointsPlane[29], pointsPlane[30]);
    linesPlane[30] = create_line(pointsPlane[30], pointsPlane[31]);
    linesPlane[31] = create_line(pointsPlane[31], pointsPlane[32]);
    linesPlane[32] = create_line(pointsPlane[32], pointsPlane[33]);
    linesPlane[33] = create_line(pointsPlane[33], pointsPlane[25]);

    // line ekor pesawat
    linesPlane[34] = create_line(pointsPlane[34], pointsPlane[35]);
    linesPlane[35] = create_line(pointsPlane[35], pointsPlane[36]);
    linesPlane[36] = create_line(pointsPlane[36], pointsPlane[37]);
    linesPlane[37] = create_line(pointsPlane[37], pointsPlane[38]);
    linesPlane[38] = create_line(pointsPlane[38], pointsPlane[34]);

    // line aksesoris pesawat
    linesPlane[39] = create_line(pointsPlane[39], pointsPlane[40]);
    linesPlane[40] = create_line(pointsPlane[40], pointsPlane[41]);
    linesPlane[41] = create_line(pointsPlane[41], pointsPlane[42]);
    linesPlane[42] = create_line(pointsPlane[42], pointsPlane[39]);

    linesPlane[43] = create_line(pointsPlane[43], pointsPlane[44]);
    linesPlane[44] = create_line(pointsPlane[44], pointsPlane[45]);
    linesPlane[45] = create_line(pointsPlane[45], pointsPlane[46]);
    linesPlane[46] = create_line(pointsPlane[46], pointsPlane[43]);

    linesPlane[47] = create_line(pointsPlane[47], pointsPlane[48]);
    linesPlane[48] = create_line(pointsPlane[48], pointsPlane[49]);
    linesPlane[49] = create_line(pointsPlane[49], pointsPlane[50]);
    linesPlane[50] = create_line(pointsPlane[50], pointsPlane[51]);
    linesPlane[51] = create_line(pointsPlane[51], pointsPlane[52]);
    linesPlane[52] = create_line(pointsPlane[52], pointsPlane[47]);
// *******

// WIINDOW
    int widthUnit = fb.width/4;
    int heightUnit = fb.height/4;

    point windowPoints[4];
    windowPoints[0] = create_point(widthUnit, heightUnit);
    windowPoints[1] = create_point(widthUnit*3, heightUnit);
    windowPoints[2] = create_point(widthUnit*3, heightUnit*3);
    windowPoints[3] = create_point(widthUnit, heightUnit*3);

    line windowLines[4];
    windowLines[0] = create_line(windowPoints[0], windowPoints[1]);
    windowLines[1] = create_line(windowPoints[1], windowPoints[2]);
    windowLines[2] = create_line(windowPoints[2], windowPoints[3]);
    windowLines[3] = create_line(windowPoints[3], windowPoints[0]);

    // viewport window;
    plane window = create_plane(windowLines, sizeof(windowLines) / sizeof(windowLines[0]));

// *******
    plane pesawat = create_plane(linesPlane, sizeof(linesPlane) / sizeof(linesPlane[0]));
    // polygon pesawat;
    // make_polygon(&pesawat, pointsPlane, sizeof(pointsPlane)/sizeof(pointsPlane[0]));

    /*********** Hardcode huruf Baling-baling **********/
    point points[3];
    points[0] = create_point(0, 0);
    points[1] = create_point(7, 0);
    points[2] = create_point(3, 30);

    line lines[3];
    lines[0] = create_line(points[0], points[1]);
    lines[1] = create_line(points[1], points[2]);
    lines[2] = create_line(points[2], points[0]);
    
    plane baling = create_plane(lines, sizeof(lines) / sizeof(lines[0]));
    /***************************************/

    // draw_plane(fb, baling, pointer, FONT_COLOR);

    plane baling_before = baling;

    // LOOP TEMPAT NGERENDER GAMBAR
    for (int i = 0; i < 360; i++) {
        delay(50);
        initializeScreen(fb, SCREEN_COLOR);
        point vpPoints[4];
        // pesawat = scale_polygon(pesawat, 1.03, 1.03);
        pesawat = scale_plane(pesawat, 1.03, 1.03);
        pesawat = rotate_plane(pesawat, pesawat.lines[pesawat.count-1].start, 1);

        plane tempPesawat = pesawat;

        // make_viewport(&window,windowPoints);
        // draw_empty_polygon(fb, pointer, window, 1, LINE_COLOR);
        // draw_plane(fb, window, pointer, WINDOW_COLOR);

        // clip_polygon_against_polygon(&tempPesawat, window);

        plane baling_translate = translate_plane(pesawat, 3, 3);
        draw_plane(fb, baling_translate, pointer, FONT_COLOR);
        // polygon baling_translate = translate_polygon(pesawat, 3, 3);
        // draw_filled_polygon(fb, pointer, tempPesawat, 1, FONT_COLOR);

        // baling = scale_plane(baling, 1.1*i/i-1, 1.1*i/i-1);
    }

    return 0;
}