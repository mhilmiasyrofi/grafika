//
// Created by iqbal on 09/02/18.
//

#include "point.h"

int main() {
    point a = create_point(2, 3);
    point piv = create_point(5, 5);

    point b = dilate_point(a, piv, 3, 3);
    
    print_point(b);

    return 0;
}