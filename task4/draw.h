// 
// Nama file : draw.h
// 

#include <linux/fb.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <signal.h>
#include <time.h>
#include "line.h"

// tipe data untuk merepresentasikan gambar
// ekivalen dengan "letter" pada task3
typedef struct {
    int count;
    line * lines;
    int width;
    int length;
} image;

image create_plane();