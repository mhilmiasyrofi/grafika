#include <linux/fb.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <signal.h>
#include <time.h>

typedef int bool;
#define true 1
#define false 0

#define FONT_WIDTH 3
#define FONT_HEIGHT 5
#define PESAWAT_COLOR 0x999990
#define FONT_SCALE 15
#define SCREEN_COLOR 0x000000
#define LINE_COLOR 0x999999

#define X_START 0
#define Y_START 0

#define LINE_X_START 100
#define LINE_Y_START 100
#define LINE_X_END 70
#define LINE_Y_END 20


#define SIZE 105

#define MAX 2000

// tipe data untuk merepresentasikan suatu frame buffer
typedef struct {
    char* pointer;
    int width;
    int height;
} frame_buffer;

// tipe data untuk merepresantikan titik
typedef struct {
    int x;
    int y;
} point;

typedef struct {
    point p[2000];
    int max;
} line;

typedef struct {
    int mat[SIZE][SIZE];
    int size;
} pesawat;

const char *FB_NAME = "/dev/fb0";
void*   m_FrameBuffer;
struct  fb_fix_screeninfo m_FixInfo;
struct  fb_var_screeninfo m_VarInfo;
int 	m_FBFD;

void delay(int number_of_seconds) {

    // Converting time into milli_seconds
    int milli_seconds = 1000 * number_of_seconds;
 
    // Stroing start time
    clock_t start_time = clock();
 
    // looping till required time is not acheived
    while (clock() < start_time + milli_seconds);
}

// menginisialisasi frame_buffer
int initialFrameBuffer(frame_buffer *fb) {

    int iFrameBufferSize;
    /* Open the framebuffer device in read write */
    m_FBFD = open(FB_NAME, O_RDWR);
    if (m_FBFD < 0) {
    	printf("Unable to open %s.\n", FB_NAME);
    	return 1;
    }
    /* Do Ioctl. Retrieve fixed screen info. */
    if (ioctl(m_FBFD, FBIOGET_FSCREENINFO, &m_FixInfo) < 0) {
    	printf("get fixed screen info failed: %s\n",
    		  strerror(errno));
    	close(m_FBFD);
    	return 1;
    }
    /* Do Ioctl. Get the variable screen info. */
    if (ioctl(m_FBFD, FBIOGET_VSCREENINFO, &m_VarInfo) < 0) {
    	printf("Unable to retrieve variable screen info: %s\n",
    		  strerror(errno));
    	close(m_FBFD);
    	return 1;
    }

    /* Calculate the size to mmap */
    iFrameBufferSize = m_FixInfo.line_length * m_VarInfo.yres;
    printf("Line length %d\n", m_FixInfo.line_length);
    /* Now mmap the framebuffer. */
    m_FrameBuffer = mmap(NULL, iFrameBufferSize, PROT_READ | PROT_WRITE,
    				 MAP_SHARED, m_FBFD,0);
    if (m_FrameBuffer == NULL) {
    	printf("mmap failed:\n");
    	close(m_FBFD);
    	return 1;
    }

    fb->pointer = (char *) m_FrameBuffer;
    fb->width = m_VarInfo.xres;
    fb->height = m_VarInfo.yres;

    return 0;
}

// menyetting warna pixel pada suatu frame_buffer
void setPixel(frame_buffer f, int x, int y, int color) {
    const long int loc_x = (x + m_VarInfo.xoffset) * m_VarInfo.bits_per_pixel / 8;
    const long int loc_y = (y + m_VarInfo.yoffset) * m_FixInfo.line_length;
    *(int*) (f.pointer + loc_x + loc_y) = color;
}

// menginsialisi screen frame_buffer dengan suatu warna
void initialScreen(frame_buffer fb, int color) {
    for (int y = 0; y < fb.height; y++) {
        for (int x = 0; x < fb.width; x++) {
            setPixel(fb, x, y, color);
        }
    }
}


void drawPesawat(frame_buffer fb, pesawat p, point start, int color) {
    // for (int i = 0; i < p.size; i++) {
    //     for (int j = 0; j < p.size; j++) {
    //         if (p.mat[i][j] == true) {
    //             setPixel(fb, start.x + j, start.y + i, color);
    //         }
    //     }
    // }

    for (int i=35; i<40; i++) {
        for (int j=-25; j<0; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }

        for (int j=40; j<50; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=30; i<35; i++) {
        for (int j=-20; j<0; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }

        for (int j=45; j<50; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=25; i<30; i++) {
        for (int j=-15; j<0; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }

        for (int j=47; j<50; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=20; i<25; i++) {
        for (int j=-10; j<5; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=15; i<20; i++) {
        for (int j=-5; j<10; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=10; i<15; i++) {
        for (int j=0; j<15; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=5; i<10; i++) {
        for (int j=5; j<20; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=0; i<5; i++) {
        for (int j=10; j<25; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    //gambar body
    for (int i=40; i<45; i++) {
        for (int j=-87; j<50; j++) {
            setPixel(fb, start.x +j, start.y + i, color);
        }
    }

    for (int i=45; i<50; i++) {
        for (int j=-90; j<50; j++) {
            setPixel(fb, start.x +j, start.y + i, color);
        }
    }

    for (int i=50; i<55; i++) {
        for (int j=-93; j<50; j++) {
            setPixel(fb, start.x +j, start.y + i, color);
        }
    }

    for (int i=55; i<60; i++) {
        for (int j=-96; j<50; j++) {
            setPixel(fb, start.x +j, start.y + i, color);
        }
    }

    for (int i=60; i<65; i++) {
        for (int j=-99; j<50; j++) {
            setPixel(fb, start.x +j, start.y + i, color);
        }
    }


    //gambar sayap bawah
    for (int i=65; i<70; i++) {
        for (int j=-25; j<0; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=70; i<75; i++) {
        for (int j=-20; j<0; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=75; i<80; i++) {
        for (int j=-15; j<0; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=80; i<85; i++) {
        for (int j=-10; j<5; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=85; i<90; i++) {
        for (int j=-5; j<10; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=90; i<95; i++) {
        for (int j=0; j<15; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=95; i<100; i++) {
        for (int j=5; j<20; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

    for (int i=100; i<105; i++) {
        for (int j=10; j<25; j++) {
            setPixel(fb, start.x + j, start.y + i, color);
        }
    }

}

void clearPesawat(frame_buffer fb, pesawat p, point start) {
    for (int i = 0; i < p.size; i++) {
        for (int j = 0; j < p.size; j++) {
            setPixel(fb, start.x + j, start.y + i, SCREEN_COLOR);
        }
    }
}

void draw_line_low(frame_buffer fb, point start, point end, int scale, int color) {
    int dx, dy, yi, p, x, y;

    dx = end.x - start.x;
    dy = end.y - start.y;
    if (dy >= 0) {
        yi = 1;
    } else {
        yi = -1;
        dy = -dy;
    }

    p = 2*dy - dx;
    y = start.y;
    for (x = start.x; x < end.x; x++) {
        setPixel(fb, x, y, color);
        if (p > 0) {
            y += yi;
            p -= 2*dx;
        }
        p += 2*dy;
    }
}

void draw_line_high(frame_buffer fb, point start, point end, int scale, int color) {
    int dx, dy, xi, p, x, y;

    dx = end.x - start.x;
    dy = end.y - start.y;
    if (dx >= 0) {
        xi = 1;
    } else {
        xi = -1;
        dx = -dx;
    }

    p = 2*dx - dy;
    x = start.x;
    for (y = start.y; y < end.y; y++) {
        setPixel(fb, x, y, color);
        if (p > 0) {
            x += xi;
            p -= 2*dy;
        }
        p += 2*dx;
    }
}

void draw_line(frame_buffer fb, point start, point end, int scale, int color) {
    if (abs(end.y - start.y) < abs(end.x - start.x)) {
        if (start.x < end.x) {
            draw_line_low(fb, start, end, scale, color);
        } else {
            draw_line_low(fb, end, start, scale, color);
        }
    } else {
        if (start.y < end.y) {
            draw_line_high(fb, start, end, scale, color);
        } else {
            draw_line_high(fb, end, start, scale, color);
        }
    }
}

void get_line_low(frame_buffer fb, point start, point end, int scale, int color, line *l) {
    int dx, dy, yi, p, x, y;

    dx = end.x - start.x;
    dy = end.y - start.y;
    if (dy >= 0) {
        yi = 1;
    } else {
        yi = -1;
        dy = -dy;
    }

    p = 2*dy - dx;
    y = start.y;

    int idx= 0;
    for (x = start.x; x < end.x; x++) {
        // setPixel(fb, x, y, color);
        l->p[idx].x = x; 
        l->p[idx].y = y;
        // printf("data : %d %d\n", x, y); 
        // printf("hasil : %d %d\n", l->p[idx].x, l->p[idx].y); 
        idx++; 
        if (p > 0) {
            y += yi;
            p -= 2*dx;
        }
        p += 2*dy;
    }
}

void get_line_high(frame_buffer fb, point start, point end, int scale, int color, line *l) {
    int dx, dy, xi, p, x, y;

    dx = end.x - start.x;
    dy = end.y - start.y;
    if (dx >= 0) {
        xi = 1;
    } else {
        xi = -1;
        dx = -dx;
    }

    p = 2*dx - dy;
    x = start.x;

    int idx= 0;
    for (y = start.y; y < end.y; y++) {
        // setPixel(fb, x, y, color);
        l->p[idx].x = x; 
        l->p[idx].y = y;
        // printf("data : %d %d\n", x, y); 
        // printf("hasil : %d %d\n", l->p[idx].x, l->p[idx].y); 
        idx++; 
        if (p > 0) {
            x += xi;
            p -= 2*dy;
        }
        p += 2*dx;
    }
}

void get_line(frame_buffer fb, point start, point end, int scale, int color, line *l) {
    if (abs(end.y - start.y) < abs(end.x - start.x)) {
        // printf("%d %d\n", l->p[i].x, l->p[i].y); 
        // printf("line low\n"); 
        if (start.x < end.x) {
            get_line_low(fb, start, end, scale, color, l);
        } else {
            get_line_low(fb, end, start, scale, color, l);
        }
    } else {
        // printf("line high\n"); 
        if (start.y < end.y) {
            get_line_high(fb, start, end, scale, color, l);
        } else {
            get_line_high(fb, end, start, scale, color, l);
        }
    }
}

void my_handler(int s) {
    printf("Caught signal %d", s);
    exit(1);
}

int main() {

    /*********** CTRL+C Handling ***********/
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    /***************************************/
    
    frame_buffer fb;

    //inisialisasi frame buffer
    if (initialFrameBuffer(&fb) == 1)
        return 1;
    
    // pointer sebagai penunjuk posisi active pixel
    point pointer;
    pointer.x = X_START;
    pointer.y = Y_START;

    // inisialisasi screen
    // mengubah screen menjadi satu warna sesuai SCREEN_COLOR
    initialScreen(fb, SCREEN_COLOR);

    pesawat garuda;

    // gambar pesawat
    garuda.size = SIZE;
    for (int i = 0; i < garuda.size; i++) {
        for (int j = 0; j < garuda.size; j++) {
            garuda.mat[i][j] = true;
        }
    }


    // titik awal pesawat
    point p_start;
    p_start.x = fb.width - garuda.size - 1;
    p_start.y = garuda.size;

    point l_start, l_end, l_start1, l_end1, l_start2, l_end2;
    l_start.x = fb.width/2 + 20;
    l_start.y = fb.height - 10;
    l_start1.x = fb.width/2 - 20;
    l_start1.y = l_start.y;
    l_end.x = l_start.x + 500;
    l_end.y = l_start.y - 500;
    l_end1.x = l_start1.x + 430;
    l_end1.y = l_start1.y - 530;
    l_start2.x = fb.width/2 - 20;
    l_start2.y = l_start.y;
    l_end2.x = l_start2.x - 450;
    l_end2.y = l_start.y - 550; 

    line l;
    line l1;
    line l2;
    l.max = MAX;
    l1.max = MAX;
    l2.max = MAX;
    int line_count = 0;
    int line_count1 = 0;
    int line_count2 = 0;

    for (int i = 0; i < l.max; i++) {
        l.p[i].x = -1;
        l.p[i].y = -1;
    }

    for (int i = 0; i < l1.max; i++) {
        l1.p[i].x = -1;
        l1.p[i].y = -1;
    }

    for (int i = 0; i < l2.max; i++) {
        l2.p[i].x = -1;
        l2.p[i].y = -1;
    }
    

    get_line(fb, l_start, l_end, 1, SCREEN_COLOR, &l);

    get_line(fb, l_start1, l_end1, 1, SCREEN_COLOR, &l1);

    get_line(fb, l_start2, l_end2, 1, SCREEN_COLOR, &l2);

    int line_length;
    for (int i = 0; (i < l.max && l.p[i].x != -1 ); i++) {
        line_length = i;
    }
    int line_idx = line_length;

    // for (int i = 0; (i < 2000 && l.p[i].x != -1 ); i++) {
    //     printf("%d %d\n", l.p[i].x, l.p[i].y); 
    //     setPixel(fb, l.p[i].x, l.p[i].y, PESAWAT_COLOR);
    // }

    while(1) {
        delay(10);
        clearPesawat(fb, garuda, p_start);
        p_start.x -= 1;
        if (p_start.x < 0) {
            p_start.x = fb.width - garuda.size - 1;
        }
        drawPesawat(fb, garuda, p_start, PESAWAT_COLOR);

        // draw_line(fb, l_start, l_end, 1, SCREEN_COLOR);
        // if (l_end.x < fb.width && l_end.y > 0) {
        //     l_start.x++;
        //     l_start.y--;
        //     l_end.x++;
        //     l_end.y--;
        // }   
        // draw_line(fb, l_start, l_end, 1, PESAWAT_COLOR);
        line_count = 0;
        line_count1 = 0;
        line_count2 = 0;
        if (line_idx != line_length) {
            for (int i = line_idx; i > 0 && l.p[i].x != -1 && line_count < 50; i++) {
            	for (int j = -3; j < 4; j++) {
                    setPixel(fb, l.p[i].x + j, l.p[i].y + j, SCREEN_COLOR);
                }
            	
                line_count++;
            }
            for (int i = line_idx; i > 0 && l1.p[i].x != -1 && line_count1 < 50; i++) {
                for (int j = -2; j < 3; j++){
                    setPixel(fb, l1.p[i].x + j, l1.p[i].y + j, SCREEN_COLOR);
                }
                // setPixel(fb, l1.p[i].x+1, l1.p[i].y+1, SCREEN_COLOR);
                line_count1++;
            }
            for (int i = line_idx; i > 0 && l2.p[i].x != -1 && line_count2 < 50; i++) {
                for (int j = -5; j < 5; j++){
                    setPixel(fb, l2.p[i].x + j, l2.p[i].y, SCREEN_COLOR);
                }
                line_count2++;
            }
        }
        line_idx--;
        line_count = 0;
        line_count1 = 0;
        line_count2 = 0;
        for (int i = line_idx; i > 0 && l.p[i].x != -1 && line_count < 50; i++) {
            for (int j = - 3; j < 4; j++) {
                setPixel(fb, l.p[i].x + j, l.p[i].y + j, LINE_COLOR);
            }
            line_count++;
        }
        for (int i = line_idx; i > 0 && l1.p[i].x != -1 && line_count1 < 50; i++) {
            for (int j = -2; j < 3; j++) {
                setPixel(fb, l1.p[i].x + j, l1.p[i].y + j, 0xcccccc);
            }   // setPixel(fb, l1.p[i].x+1, l1.p[i].y+1, 0xcccccc);
            line_count1++;
        }
        for (int i = line_idx; i > 0 && l2.p[i].x != -1 && line_count2 < 50; i++) {
            for (int j = -5; j < 5; j++){
                setPixel(fb, l2.p[i].x + j, l2.p[i].y, 0x777777);
            }
            line_count2++;
        }
        if (line_idx < 0)
            line_idx = line_length;

    }
    
    return 0;
}
