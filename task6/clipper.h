//
// Created by iqbal on 20/02/18.
//

#include "shape.h"
#include "global.h"

#ifndef GRAFIKA_CLIPPER_H
#define GRAFIKA_CLIPPER_H

typedef int out_code;

#define INSIDE 0;   // 0000
#define LEFT 1;     // 0001
#define RIGHT 2;    // 0010
#define BOTTOM 4;   // 0100
#define TOP 8;      // 1000

boolean clip_point_against_rectangle(point p, rectangle r);

line clip_line_against_rectangle(line l, rectangle r);

plane clip_polygon_against_rectangle(plane p, rectangle r);

#endif //GRAFIKA_CLIPPER_H
