#include "plane.h"
#include "line.h"
#include "point.h"

/* Basically just create a rectangle at a certain offset */
plane create_viewport(int width, int height, int vpX, int vpY, point * vpPoints, line * vpLines) {
    // vpX = 10;
    // vpY = 10;
    // height = 10;
    // width = 10;

    // point vpPoints[4];
    vpPoints[0] = create_point(vpX, vpY);
    vpPoints[1] = create_point(vpX+width, vpY);
    vpPoints[2] = create_point(vpX+width, vpY+height);
    vpPoints[3] = create_point(vpX, vpY+height);

    // line vpLines[4];
    vpLines[0] = create_line(vpPoints[0], vpPoints[1]);
    vpLines[1] = create_line(vpPoints[1], vpPoints[2]);
    vpLines[2] = create_line(vpPoints[2], vpPoints[3]);
    vpLines[3] = create_line(vpPoints[3], vpPoints[0]);

    plane viewport = create_plane(vpLines, sizeof(vpLines)/sizeof(vpLines[0]));
    return viewport;
}