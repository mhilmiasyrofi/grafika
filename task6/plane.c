#include "global.h"
#include "plane.h"
#include "fbuffer.h"

plane create_plane(line *lines, int count) {
    plane result;
    result.lines = lines;
    result.count = count;

    for (int i = 0; i < count; i++){
        lines[i].start.x *= FONT_SCALE;
        lines[i].start.y *= FONT_SCALE;
        lines[i].end.x *= FONT_SCALE;
        lines[i].end.y *= FONT_SCALE;
    }

    int max_width = lines[0].start.x;
    int min_width = lines[0].start.x;
    for (int i = 0; i < count; i++){
        if (lines[i].start.x < min_width)
            min_width = lines[i].start.x;
        if (lines[i].start.x > max_width)
            max_width = lines[i].start.x;
    }
    // result.width = max_width - min_width;
    result.min_width = min_width;
    result.width = (max_width - min_width + 1);
    
    
    int max_height = lines[0].start.y;
    int min_height = lines[0].start.y;
    for (int i = 0; i < count; i++){
        if (lines[i].start.y < min_height)
            min_height = lines[i].start.y;
        if (lines[i].start.y > max_height)
            max_height = lines[i].start.y;
    }
    // result.height = max_height - min_height;
    result.min_height = min_height;
    result.height = (max_height - min_height + 1);

    // printf("%d\n", result.width);
    return result;
}

void draw_plane(frame_buffer fb, plane p, point origin, int color) {

    int idx_last = p.count - 1;
    point start = create_point(p.lines[idx_last].start.x + origin.x, p.lines[idx_last].start.y + origin.y);
    point end = create_point(p.lines[idx_last].end.x + origin.x, p.lines[idx_last].end.y + origin.y);
    line drawn_line = create_line(start, end);

    float prev_grad, curr_grad;
    prev_grad = drawn_line.gradient;
    int prev_deltaY, curr_deltaY;
    prev_deltaY = drawn_line.deltaY;

    point exception[1000];
    for (int i = 0; i < 1000; i++) {
        exception[i].x = -1;
        exception[i].y = -1;
    }

    int count = 0;

    for (int i = 0; i < p.count; i++) {
        start = create_point(p.lines[i].start.x + origin.x, p.lines[i].start.y + origin.y);
        end = create_point(p.lines[i].end.x + origin.x, p.lines[i].end.y + origin.y);
        drawn_line = create_line(start, end);
        
        draw_line(fb, drawn_line, 1, color);

        // get exception point
        curr_grad = drawn_line.gradient;
        curr_deltaY = drawn_line.deltaY;
        if (prev_grad < 0 && curr_grad > 0 && prev_deltaY < 0 && curr_deltaY > 0) {
            exception[count].x = start.x;
            exception[count].y = start.y;
            // printf("BAWAH\n");
            count++;
            exception[count].x = start.x-1;
            exception[count].y = start.y+1;
            count++;
            exception[count].x = start.x;
            exception[count].y = start.y+1;
            count++;
            exception[count].x = start.x+1;
            exception[count].y = start.y+1;
            count++;
            // printf("%d, %d\n", start.x, start.y);
        } else if (prev_grad > 0 && curr_grad < 0 && prev_deltaY > 0 && curr_deltaY < 0) {
            exception[count].x = start.x;
            exception[count].y = start.y;
            count++;
            // printf("ATAS\n");
            exception[count].x = start.x-1;
            exception[count].y = start.y-1;
            count++;
            exception[count].x = start.x;
            exception[count].y = start.y-1;
            count++;
            exception[count].x = start.x + 1;
            exception[count].y = start.y-1;
            count++;
            // printf("%d, %d\n", start.x, start.y);
        } else if (curr_deltaY == 0) {
            if (start.x >= end.x){
                for (int j = end.x; j <= start.x; j++) {
                    exception[count].x = j;
                    exception[count].y = start.y;
                    count++;
                }
            } else {
                for (int j = start.x; j <= end.x; j++) {
                    exception[count].x = j;
                    exception[count].y = start.y;
                    count++;
                }
            }
        }

        prev_grad = curr_grad;
        prev_deltaY = curr_deltaY;
    }

    // sorting exception
    for (int i = 0; i < count; i++) {
        for (int j = i+1; j < count; j++) {
            if (exception[i].y > exception[j].y) {
                int tempX = exception[i].x;
                int tempY = exception[i].y;
                exception[i].x = exception[j].x;
                exception[i].y = exception[j].y;
                exception[j].x = tempX;
                exception[j].y = tempY;
            } else if (exception[i].y == exception[j].y && exception[i].x > exception[j].x) {
                int tempX = exception[i].x;
                int tempY = exception[i].y;
                exception[i].x = exception[j].x;
                exception[i].y = exception[j].y;
                exception[j].x = tempX;
                exception[j].y = tempY;
            } else if (exception[i].y == exception[j].y && exception[i].x == exception[j].x) {
                exception[j].x = -1;
                exception[j].y = -1;
            }
        }
    }

    
    for (int i = 0; i < 100; i++) {
        // printf("%d %d\n", exception[i].x, exception[i].y);
    }

    // printf("%d\n", color);
    // setPixel(fb, 1, 1, color);
    // printf("%d\n", getPixel(fb, 1, 1));

    
    // memberi warna dalam huruf
    int idx = 0;
    boolean isColor = false;
    int color_before = SCREEN_COLOR;
    int color_now;
    int found = 0;

    int copyMat[p.height][p.width];
    for (int j = p.min_height; j < p.min_height + p.height; j++) {
        for (int i = p.min_width; i < p.min_width + p.width; i++) {
            copyMat[j - p.min_height][i - p.min_width] = getPixel(fb, i + origin.x, j + origin.y);
            // setPixel(fb, i + origin.x + 300, j + origin.y + 300, copyMat[j][i]); // buat debugging doang
        }
    }

    for (int j = p.min_height; j < p.min_height + p.height; j++) {
        for (int i = p.min_width; i < p.min_width + p.width; i++) {
            color_now = copyMat[j - p.min_height][i - p.min_width];
            if ((exception[idx].x == i + origin.x) && (exception[idx].y == j + origin.y)) {
                idx++;
                // printf("%d, %d\n", exception[idx].x, exception[idx].y);
            } else {
                if (color_now == color && color_before == SCREEN_COLOR) {
                    found++;
                    if (found % 2 == 0)
                        isColor = false;
                    else
                        isColor = true;
                }
                if (isColor == true) {
                    setPixel(fb, i + origin.x, j + origin.y, color);
                    // printf("%d %d\n", i + origin.x, j + origin.y);
                }
            }
            color_before = color_now;
            // setPixel(fb, i + origin.x, j + origin.y, 0xFFFF);
        }
        color_before = SCREEN_COLOR;
        found = 0;
        isColor = false;
    }



}

plane rotate_plane(plane p, point pivot, float angle) {
    for (int i = 0; i < p.count; i++) {
        line l = p.lines[i];
        p.lines[i] = rotate_line(l, pivot, angle);
    }

    plane dst = resize_plane(p);

    return dst;
}

plane scale_plane(plane p, float sx, float sy) {
    for (int i = 0; i < p.count; i++) {
        point start = p.lines[i].start;
        point end = p.lines[i].end;

        p.lines[i].start = scale_point(start, sx, sy);
        p.lines[i].end = scale_point(end, sx, sy);
    }

    plane dst = resize_plane(p);

    return dst;
}

plane translate_plane(plane p, int dx, int dy) {

    for (int i = 0; i < p.count; i++) {
        line l = p.lines[i];
        p.lines[i] = translate_line(l, dx, dy);
    }

    plane dst = resize_plane(p);
    
    return dst;
}

plane resize_plane(plane p) {
    int max_width = p.lines[0].start.x;
    int min_width = p.lines[0].start.x;
    for (int i = 0; i < p.count; i++)
    {
        if (p.lines[i].start.x < min_width)
            min_width = p.lines[i].start.x;
        if (p.lines[i].start.x > max_width)
            max_width = p.lines[i].start.x;
    }
    // result.width = max_width - min_width;
    p.min_width = min_width;
    p.width = (max_width - min_width + 1);

    int max_height = p.lines[0].start.y;
    int min_height = p.lines[0].start.y;
    for (int i = 0; i < p.count; i++)
    {
        if (p.lines[i].start.y < min_height)
            min_height = p.lines[i].start.y;
        if (p.lines[i].start.y > max_height)
            max_height = p.lines[i].start.y;
    }
    // result.height = max_height - min_height;
    p.min_height = min_height;
    p.height = (max_height - min_height + 1);

    // printf("DALAM\n");
    // printPlane(p);

    return p;
}

void printPlane(plane p) {
    printf("count: %d\n", p.count);
    printf("min_width: %d\n", p.min_width);
    printf("min_height: %d\n", p.min_height);
    printf("width: %d\n", p.width);
    printf("height: %d\n", p.height);

    printf("\n");

    printf("lines :\n");
    for (int i = 0; i < p.count; ++i)
    {
        printf("line[%d] : \n", i);
        printf("\t(x1, y1) = (%d, %d)\n", p.lines[i].start.x, p.lines[i].start.y);
        printf("\t(x2, y2) = (%d, %d)\n", p.lines[i].end.x, p.lines[i].end.y);
    }
}