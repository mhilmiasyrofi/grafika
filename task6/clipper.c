//
// Created by iqbal on 20/02/18.
//

#include "clipper.h"
#include "plane.h"
#include "line.h"
#include "point.h"

out_code get_point_out_code(point p, rectangle view_port) {
    out_code code = INSIDE;

    int xmin = view_port.lines[0].start.x;
    int ymin = view_port.lines[0].start.y;
    int xmax = view_port.lines[1].end.x;
    int ymax = view_port.lines[1].end.y;

    if (p.x < xmin) {
        code |= LEFT;
    } else if (p.x > xmax) {
        code |= RIGHT;
    }

    if (p.y < ymin) {
        code |= BOTTOM;
    } else if (p.y > ymax) {
        code |= TOP;
    }

    return code;
}

int x_intersect (line line1, line line2) {
    int x1 = line1.start.x;
    int y1 = line1.start.y;
    int x2 = line1.end.x;
    int y2 = line1.end.y;
    int x3 = line2.start.x;
    int y3 = line2.start.y;
    int x4 = line2.end.x;
    int y4 = line2.end.y;

    int numerator = (x1*y2 - y1*x2) * (x3-x4) - (x1-x2) * (x3*y4 - y3*x4);
    int denominator = (x1-x2) * (y3-y4) - (y1-y2) * (x3-x4);

    return numerator/denominator;
}

int y_intersect (line line1, line line2) {
    int x1 = line1.start.x;
    int y1 = line1.start.y;
    int x2 = line1.end.x;
    int y2 = line1.end.y;
    int x3 = line2.start.x;
    int y3 = line2.start.y;
    int x4 = line2.end.x;
    int y4 = line2.end.y;

    int numerator = (x1*y2 - y1*x2) * (y3-y4) - (y1-y2) * (x3*y4 - y3*x4);
    int denominator = (x1-x2) * (y3-y4) - (y1-y2) * (x3-x4);

    return numerator/denominator;
}

boolean clip_point_against_rectangle(point p, rectangle r) {
    return get_point_out_code(p, r) != 0;
}

line clip_line_against_rectangle(line l, rectangle r) {

}

plane clip_polygon_against_line(plane p, line l) {
    line result_lines[p.count];
    int result_count = 0;
    plane result;


    boolean is_new_start_set = false;
    boolean is_new_end_set = false;
    for (int i = 0; i < p.count; ++i) {
        point start = p.lines[i].start;
        point end = p.lines[i].end;
        line new_edge;

        int x1 = l.start.x;
        int y1 = l.start.y;
        int x2 = l.end.x;
        int y2 = l.end.y;

        // posisi point terhadap garis acu.
        // pos < 0, titik berada di kanan garis.
        // pos = 0, titik berada pada garis.
        // pos > 0, titik berada di kiri garis.
        // titik berada di kanan berarti titik berada di dalam view_port
        int start_pos = (y2-y1) * (start.x - x1) - (x2-x1) * (start.y-y1); //- (y2-y1) * (start.x - x1);
        int end_pos = (y2-y1) * (end.x - x1) - (x2-x1) * (end.y-y1); //- (y2-y1) * (end.x - x1);
        // printf("x1: %d\n", x1);
        // printf("y1: %d\n", y1);
        // printf("x2: %d\n", x2);
        // printf("y2: %d\n", y2);
        // printf("start.x: %d\n", start.x);
        // printf("start.y: %d\n", start.y);
        // printf("end.x: %d\n", end.x);
        // printf("end.y: %d\n", end.y);
        // printf("start_pos: %d\n", start_pos);
        // printf("end_pos: %d\n", end_pos);

        point new_start, new_end;
        if (start_pos < 0 && end_pos < 0) {
            // ketika kedua titik berada di dalam
            new_edge = create_line(start, end);
            result_lines[result_count] = new_edge;
            result_count++;
            printf("in ch 1\n");
        } else if (start_pos >= 0 && end_pos < 0) {
            // ketika hanya titik awal yang berada di dalam
            int x = x_intersect(p.lines[i], l);
            int y = y_intersect(p.lines[i], l);
            new_end = create_point(x, y);
            new_edge = create_line(start, new_end);
            result_lines[result_count] = new_edge;
            result_count++;
            printf("in ch 2\n");
            is_new_end_set = true;
        } else if (start_pos < 0 && end_pos >= 0) {
            // ketika hanya titik akhir yang berada di dalam
            int x = x_intersect(p.lines[i], l);
            int y = y_intersect(p.lines[i], l);
            new_start = create_point(x, y);
            new_edge = create_line(new_start, end);
            result_lines[result_count] = new_edge;
            result_count++;
            printf("in ch 3\n");
            is_new_start_set = true;
        } else if (is_new_end_set && is_new_start_set){
            new_edge = create_line(new_end, new_start);
            result_lines[result_count] = new_edge;
            result_count++;
            printf("in ch 4\n");
        }
    }

    result = create_plane(result_lines, result_count);
    printPlane(result);
    return result;
}

plane clip_polygon_against_rectangle(plane p, rectangle r) {
    plane result = r;

    for (int i = 0; i < r.count; ++i) {
        result = clip_polygon_against_line(p, r.lines[i]);
    }

    return result;
}


