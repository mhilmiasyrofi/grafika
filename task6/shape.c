//
// Created by iqbal on 19/02/18.
//
#include "shape.h"
#include "point.h"

rectangle create_rectangle(int width, int height) {
    point top_left = create_point(0, 0);
    point top_right = create_point(width, 0);
    point bottom_left = create_point(0, height);
    point bottom_right = create_point(width, height);

    line lines[4];
    lines[0] = create_line(top_left, top_right);
    lines[1] = create_line(top_right, bottom_right);
    lines[2] = create_line(bottom_right, bottom_left);
    lines[3] = create_line(bottom_left, top_left);

    return create_plane(lines, 4);
}

void draw_empty_rectangle(frame_buffer fb, point origin, int width, int height, int scale, int border_color) {
    plane rectangle = create_rectangle(width, height);
    for (int i = 0; i < rectangle.count; ++i) {
        draw_line(fb, rectangle.lines[i], scale, border_color);
    }
}

void draw_filled_rectangle(frame_buffer fb, point origin, int width, int height, int fill_color) {
    plane rectangle = create_rectangle(width, height);
    draw_plane(fb, rectangle, origin, fill_color);
}

void draw_empty_circle(frame_buffer fb, point center, int radius, int border_color) {
    int x = radius - 1;
    int y = 0;
    int dx = 1;
    int dy = 1;
    int err = dx - (radius << 1);

    while (x >= y) {
        setPixel(fb, (center.x + x), (center.y + y), border_color);
        setPixel(fb, (center.x + y), (center.y + x), border_color);
        setPixel(fb, (center.x + x), (center.y - y), border_color);
        setPixel(fb, (center.x + y), (center.y - x), border_color);
        setPixel(fb, (center.x - x), (center.y + y), border_color);
        setPixel(fb, (center.x - y), (center.y + x), border_color);
        setPixel(fb, (center.x - x), (center.y - y), border_color);
        setPixel(fb, (center.x - y), (center.y - x), border_color);

        if (err <= 0) {
            y++;
            err += dy;
            dy += 2;
        }

        if (err > 0) {
            x--;
            dx += 2;
            err += dx - (radius << 1);
        }
    }
}

