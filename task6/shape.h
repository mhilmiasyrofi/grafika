//
// Created by iqbal on 19/02/18.
//

#include "line.h"
#include "plane.h"

#ifndef GRAFIKA_SHAPE_H
#define GRAFIKA_SHAPE_H

typedef plane rectangle;

void draw_empty_rectangle(frame_buffer fb, point origin, int width, int height, int scale, int border_color);

void draw_filled_rectangle(frame_buffer fb, point origin, int width, int height, int fill_color);

void draw_empty_circle(frame_buffer fb, point center, int radius, int border_color);

#endif //GRAFIKA_SHAPE_H
